package com.disco.sal.biblioappbicocca.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.disco.sal.biblioappbicocca.R;
import com.disco.sal.biblioappbicocca.classes.HttpRequest;


public class NewsActivity extends ActionBarActivity
{
    String url;
    int position;
    TextView textNews;
    TextView title;
    TextView date;
    TextView tag;
    String description;
    Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        position = intent.getIntExtra("position", 0);
        url = intent.getStringExtra("url");
        if (MainActivity.news != null)
        {
            if (MainActivity.news.get((position)).getDescription() != null)
            {
                description = MainActivity.news.get(position).getDescription();
                setContentView(R.layout.activity_news);
                toolbar = (Toolbar) findViewById(R.id.toolbargeneric);
                title = (TextView) findViewById(R.id.newsTitle);
                date = (TextView) findViewById(R.id.newsData);
                textNews = (TextView) findViewById(R.id.newsText);
                tag = (TextView) findViewById(R.id.tagNews);
                setSupportActionBar(toolbar);
                title.setText(MainActivity.news.get(position).getTitle());
                date.setText(MainActivity.news.get(position).getDate());
                tag.setText(MainActivity.news.get(position).getTag().toUpperCase());
                textNews.setText(Html.fromHtml(description));
                textNews.setMovementMethod(LinkMovementMethod.getInstance());
            }
            else
            {
                LoadNews loadNews = new LoadNews(this);
                loadNews.execute();
            }
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        return super.onOptionsItemSelected(item);
    }

    public class LoadNews extends AsyncTask<String, String, String>
    {

        Activity context;
        private ProgressDialog progressDialog = null;

        public LoadNews(Activity context)
        {
            this.context = context;
        }

        @Override
        protected void onPostExecute(String result)
        {
            progressDialog.dismiss();
            setContentView(R.layout.activity_news);
            toolbar = (Toolbar) findViewById(R.id.toolbargeneric);
            setSupportActionBar(toolbar);
            if (!MainActivity.serverDown && !MainActivity.errorGeneric)
            {
                title = (TextView) findViewById(R.id.newsTitle);
                date = (TextView) findViewById(R.id.newsData);
                textNews = (TextView) findViewById(R.id.newsText);
                tag = (TextView) findViewById(R.id.tagNews);
                title.setText(MainActivity.news.get(position).getTitle());
                date.setText(MainActivity.news.get(position).getDate());
                tag.setText(MainActivity.news.get(position).getTag().toUpperCase());
                description = MainActivity.news.get(position).getDescription();
                textNews.setText(Html.fromHtml(description));
                textNews.setMovementMethod(LinkMovementMethod.getInstance());
            }
            else if (MainActivity.serverDown)
            {
                TextView txt;
                View lay = findViewById(R.id.empty);
                lay.setVisibility(View.VISIBLE);
                txt = (TextView) lay.findViewById(R.id.emptySet);
                txt.setText(getString(R.string.connection_error));
            }
            else if (MainActivity.errorGeneric)
            {
                TextView txt;
                View lay = findViewById(R.id.empty);
                lay.setVisibility(View.VISIBLE);
                txt = (TextView) lay.findViewById(R.id.emptySet);
                txt.setText(getString(R.string.generic_error));
            }

        }

        @Override
        protected String doInBackground(String... params)
        {
            if (isOnline())
                HttpRequest.getSingleNews(url, position);
            else
                MainActivity.serverDown = true;
            return "Finish";

        }

        @Override
        protected void onPreExecute()
        {
            progressDialog = ProgressDialog.show(context, getString(R.string.download_alert_title), getString(R.string.news_download_alert_text), true);
            progressDialog.setCancelable(false);
        }

        @Override
        protected void onProgressUpdate(String... values)
        {
        }


    }

    public boolean isOnline()
    {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

}
