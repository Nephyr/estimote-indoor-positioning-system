package com.disco.sal.biblioappbicocca.activities;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import com.disco.sal.biblioappbicocca.R;
import com.disco.sal.biblioappbicocca.adapters.AdapterCopies;
import com.disco.sal.biblioappbicocca.models.Copy;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

public class CopiesDetailsActivity extends ActionBarActivity
{
    Toolbar toolbar;
    String library;
    String urlDocument;
    String subLibrary;
    GoogleMap supportMap;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_copies);
        Intent intent = getIntent();
        library = intent.getStringExtra("library");

        View header = getLayoutInflater().inflate(R.layout.list_copies_header, null, false);
        ListView listViewCopies = (ListView) findViewById(R.id.listViewCopies);
        listViewCopies.addHeaderView(header, null, false);
        ArrayList<Copy> copie = new ArrayList<>();
        for (Copy copy : MainActivity.document.getCopieses())
        {
            if (copy.getSubLibrary().equalsIgnoreCase(library))
                copie.add(copy);
        }
        AdapterCopies adapterCopies = new AdapterCopies(this, copie);
        listViewCopies.setAdapter(adapterCopies);

        supportMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
        toolbar = (Toolbar) findViewById(R.id.toolbargeneric);
        setSupportActionBar(toolbar);

        urlDocument = intent.getStringExtra("urlDocument");
        library = intent.getStringExtra("library");
        subLibrary = library;
        if (GooglePlayServicesUtil.isGooglePlayServicesAvailable(this) == ConnectionResult.SUCCESS)
        {
            setMap();
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();
        switch (item.getItemId())
        {
            case android.R.id.home:
                finish();

        }
        return super.onOptionsItemSelected(item);
    }


    public void setMap()
    {
        String indirizzoBiblioteca = "";
        LatLng coordinate = null;
        if (subLibrary.equalsIgnoreCase("Bicocca Sede Centrale"))
        {
            coordinate = new LatLng(45.518447, 9.213905);
            indirizzoBiblioteca = "Piazza dell'Ateneo Nuovo, 1 - 20126 Milano\n(building U6, floor II)";
        }
        else if (subLibrary.equalsIgnoreCase("Bicocca Scienze"))
        {
            coordinate = new LatLng(45.513734, 9.211929);
            indirizzoBiblioteca = "Piazza della Scienza, 3 - 20126 Milano\nbuilding U2, floor I (books section)\nand floor -I (journals section)";
        }
        else if (subLibrary.equalsIgnoreCase("Bicocca Medicina"))
        {
            coordinate = new LatLng(45.604118, 9.261077);
            indirizzoBiblioteca = "Via Cadore, 48 - 20900 Monza\n(building U8, ground floor)";
        }
        else if (subLibrary.equalsIgnoreCase("Insubria Medicina e Scienze(VA"))
        {
            coordinate = new LatLng(45.796774, 8.848428);
            indirizzoBiblioteca = "Via J. H. Dunant, 3 - 21100 Varese\ntel.: 0332/421.426\ne-mail: bibliobiomedica@uninsubria.it";
        }
        else if (subLibrary.equalsIgnoreCase("Insubria Economia"))
        {
            coordinate = new LatLng(45.798159, 8.852641);
            indirizzoBiblioteca = "Via Monte Generoso, 71 - 21100 Varese\ntel. sala lettura/prestito: 0332/39.5103-4\ne-mail: biblioeconomia@uninsubria.it";
        }
        else if (subLibrary.equalsIgnoreCase("Insubria Giurisprudenza"))
        {
            coordinate = new LatLng(45.811193, 9.075758);
            indirizzoBiblioteca = "Via M.E. Bossi, n. 5 - 22100 Como\ntel. +39 031-238 4175\ne-mail: bibliogiurisprudenza@uninsubria.it";
        }
        else if (subLibrary.equalsIgnoreCase("Insubria Scienze (Como)"))
        {
            coordinate = new LatLng(45.802087, 9.094623);
            indirizzoBiblioteca = "Via Valleggio, 11 – 22100 Como\ntel. Sala 2: 031/238.9566\ntel. Sala 3: 031/238.9565\ne-mail: biblioscienze@uninsubria.it";
        }
        else
            coordinate = new LatLng(0, 0);

        supportMap.setMyLocationEnabled(true);
        supportMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener()
        {

            @Override
            public void onMyLocationChange(Location arg0)
            {
                supportMap.addMarker(new MarkerOptions().position(new LatLng(arg0.getLatitude(), arg0.getLongitude())).title(getString(R.string.user_position)));
            }
        });

        MarkerOptions mark = new MarkerOptions();
        mark.position(coordinate);
        mark.title(subLibrary);
        mark.snippet(indirizzoBiblioteca);

        supportMap.addMarker(mark);
        CameraUpdate center = CameraUpdateFactory.newLatLng(coordinate);
        CameraUpdate zoom = CameraUpdateFactory.zoomTo(15);
        supportMap.moveCamera(center);
        supportMap.animateCamera(zoom);

    }

}

