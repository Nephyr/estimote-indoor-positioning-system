package com.disco.sal.biblioappbicocca.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Spinner;

import com.disco.sal.biblioappbicocca.R;
import com.disco.sal.biblioappbicocca.activities.MainActivity;

import java.util.ArrayList;
import java.util.Calendar;


public class FilteredSearchFragment extends DialogFragment
{

    private EditText title;
    private EditText author;
    private EditText publisher;
    private EditText place;
    private EditText code;
    private Spinner material;
    private Spinner spinYear;
    private Spinner language;

    public static FilteredSearchFragment newInstance()
    {
        FilteredSearchFragment fragment = new FilteredSearchFragment();
        return fragment;
    }

    public FilteredSearchFragment()
    {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View view = inflater.inflate(R.layout.fragment_filtered_search, null);
        final ScrollView scrollViewFileredSearch = (ScrollView) view.findViewById(R.id.scrollViewFileredSearch);
        material = (Spinner) view.findViewById(R.id.spinnerType);
        spinYear = (Spinner) view.findViewById(R.id.spinnerYear);
        language = (Spinner) view.findViewById(R.id.spinnerLanguage);
        title = (EditText) view.findViewById(R.id.editTextTitle);
        title.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {

            }

            @Override
            public void afterTextChanged(Editable s)
            {
                if (s.toString().contains("\n"))
                {
                    title.setText(title.getText().toString().replace("\n", ""));
                    author.requestFocus();
                }

            }
        });
        author = (EditText) view.findViewById(R.id.editTextAutore);
        author.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {

            }

            @Override
            public void afterTextChanged(Editable s)
            {
                if (s.toString().contains("\n"))
                {
                    author.setText(author.getText().toString().replace("\n", ""));
                    publisher.requestFocus();

                }

            }
        });
        publisher = (EditText) view.findViewById(R.id.editTextEditore);
        publisher.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {

            }

            @Override
            public void afterTextChanged(Editable s)
            {
                if (s.toString().contains("\n"))
                {
                    publisher.setText(publisher.getText().toString().replace("\n", ""));
                    place.requestFocus();

                }

            }
        });
        place = (EditText) view.findViewById(R.id.editTextPlace);
        place.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {

            }

            @Override
            public void afterTextChanged(Editable s)
            {
                if (s.toString().contains("\n"))
                {
                    place.setText(place.getText().toString().replace("\n", ""));
                    code.requestFocus();

                }

            }
        });
        code = (EditText) view.findViewById(R.id.editTextCode);
        code.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {

            }

            @Override
            public void afterTextChanged(Editable s)
            {
                if (s.toString().contains("\n"))
                {
                    code.setText(code.getText().toString().replace("\n", ""));
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(
                            Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(code.getWindowToken(), 0);
                    scrollViewFileredSearch.post(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            scrollViewFileredSearch.fullScroll(ScrollView.FOCUS_DOWN);
                        }
                    });

                }

            }
        });

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(), R.array.type_spinner_values, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        material.setAdapter(adapter);


        ArrayAdapter<CharSequence> lingAdap = ArrayAdapter.createFromResource(getActivity(), R.array.language_spinner_values, android.R.layout.simple_spinner_item);
        lingAdap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        language.setAdapter(lingAdap);


        ArrayList<String> year = new ArrayList<>();
        year.add(getString(R.string.year_spinner_all));
        Calendar calendar = Calendar.getInstance();
        int yearNow = calendar.get(Calendar.YEAR);
        for (int i = yearNow; i > 0; i--)
            year.add(String.valueOf(i));
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, year);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinYear.setAdapter(dataAdapter);

        builder.setView(inflater.inflate(R.layout.fragment_filtered_search, null));
        builder.setTitle(getString(R.string.filter_search_toolbar_title))
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int id)
                    {
                        MainActivity.titolo = title.getText().toString();
                        MainActivity.autore = author.getText().toString();
                        MainActivity.editore = publisher.getText().toString();
                        MainActivity.luogo = place.getText().toString();
                        MainActivity.codice = code.getText().toString();
                        setMaterial(material.getSelectedItem().toString());
                        MainActivity.anno = spinYear.getSelectedItem().toString();
                        setLanguage(language.getSelectedItem().toString());
                        MainActivity.documentsPreviews = null;
                        MainActivity.offset = 1;
                        MainActivity.query = "";
                        MainActivity.serverDown = false;
                        MainActivity.offsetBound = false;
                        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                        fragmentManager.beginTransaction()
                                .replace(R.id.container, ListDocumentsFromAlpehFragment.newInstance(MainActivity.query))
                                .commit();
                    }
                })
                .setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int id)
                    {

                    }
                });

        builder.setView(view);
        return builder.create();
    }

    private void setMaterial(String s)
    {

        if (s.equals(getResources().getStringArray(R.array.type_spinner_values)[0]))
        {
            MainActivity.materiale = "Tutti";
        }
        else if (s.equals(getResources().getStringArray(R.array.type_spinner_values)[1]))
        {
            MainActivity.materiale = "BK";
        }
        else if (s.equals(getResources().getStringArray(R.array.type_spinner_values)[2]))
        {
            MainActivity.materiale = "SE";
        }
        else if (s.equals(getResources().getStringArray(R.array.type_spinner_values)[3]))
        {
            MainActivity.materiale = "AQ";
        }
        else if (s.equals(getResources().getStringArray(R.array.type_spinner_values)[4]))
        {
            MainActivity.materiale = "CM";
        }
        else if (s.equals(getResources().getStringArray(R.array.type_spinner_values)[5]))
        {
            MainActivity.materiale = "NB";
        }
        else if (s.equals(getResources().getStringArray(R.array.type_spinner_values)[6]))
        {
            MainActivity.materiale = "VM";
        }
    }

    private void setLanguage(String s)
    {

        if (s.equals(getResources().getStringArray(R.array.language_spinner_values)[0]))
        {
            MainActivity.lingua = "Tutte";
        }
        else if (s.equals(getResources().getStringArray(R.array.language_spinner_values)[1]))
        {
            MainActivity.lingua = "ITA";
        }
        else if (s.equals(getResources().getStringArray(R.array.language_spinner_values)[2]))
        {
            MainActivity.lingua = "ENG";
        }
        else if (s.equals(getResources().getStringArray(R.array.language_spinner_values)[3]))
        {
            MainActivity.lingua = "FRE";
        }
        else if (s.equals(getResources().getStringArray(R.array.language_spinner_values)[4]))
        {
            MainActivity.lingua = "SPA";
        }
        else if (s.equals(getResources().getStringArray(R.array.language_spinner_values)[5]))
        {
            MainActivity.lingua = "GER";
        }
        else if (s.equals(getResources().getStringArray(R.array.language_spinner_values)[6]))
        {
            MainActivity.lingua = "RUS";
        }
        else if (s.equals(getResources().getStringArray(R.array.language_spinner_values)[7]))
        {
            MainActivity.lingua = "CHI";
        }
        else if (s.equals(getResources().getStringArray(R.array.language_spinner_values)[8]))
        {
            MainActivity.lingua = "JPN";
        }
    }


    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
    }

}
