package com.disco.sal.biblioappbicocca.classes;

import com.estimote.sdk.Beacon;
import com.estimote.sdk.Utils;

import java.util.ArrayList;

/**
 * Created by Marco on 31/01/2016.
 * Codice preso da tesi di terzi.
 */
public class AVGDistanceThreeNearestBeacons implements Heuristic
{
    private int nPositions = 0;
    private double[] media = new double[2];
    ArrayList<double[]> results = new ArrayList<>();

    @Override
    public void heuristic(ArrayList<Beacon> beacons, double[][] centers)
    {
        double[] distances = new double[3];

        distances[0] = Utils.computeAccuracy((Beacon) beacons.get(0));
        distances[1] = Utils.computeAccuracy((Beacon) beacons.get(1));
        distances[2] = Utils.computeAccuracy((Beacon) beacons.get(2));

        Trilateration tr = new Trilateration();
        results.add(tr.positioning(centers, distances));
        nPositions++;

        if (nPositions == 4)
        {
            nPositions = 0;
            media[0] = (results.get(0)[0] + results.get(1)[0] + results.get(2)[0] +
                        results.get(3)[0]) / 4;
            media[1] = (results.get(0)[1] + results.get(1)[1] + results.get(2)[1] +
                        results.get(3)[1]) / 4;
        }
    }

    public double[] getMedia()
    {
        return media;
    }
}
