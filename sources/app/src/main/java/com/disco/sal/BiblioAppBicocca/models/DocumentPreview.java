package com.disco.sal.biblioappbicocca.models;

import java.util.ArrayList;

/**
 * Created by lorenzo on 17/03/15.
 */
public class DocumentPreview {

    private String title;
    private String id;
    private ArrayList<String> autore;
    private ArrayList<String> corporateBody;
    private String type;
    private ArrayList<String> publication;

    DocumentPreview(String title, ArrayList<String> autore, String id) {
        this.title = title;
        this.id = id;
        this.autore = autore;

    }

    public DocumentPreview(String title, ArrayList<String> autore, ArrayList<String> corporateBody, String id, String type, ArrayList<String> publication) {
        this.title = title;
        this.id = id;
        this.autore = autore;
        this.corporateBody = corporateBody;
        this.type = type;
        this.publication = publication;

    }


    public DocumentPreview() {
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<String> getAutore() {
        return autore;
    }

    public void setAutore(ArrayList<String> autore) {
        this.autore = autore;
    }

    public ArrayList<String> getCorporateBody() {
        return corporateBody;
    }

    public void setCorporateBody(ArrayList<String> corporateBody) {
        this.corporateBody = corporateBody;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public ArrayList<String> getPublication() {
        return publication;
    }

    public void setPublication(ArrayList<String> publication) {
        this.publication = publication;
    }
}
