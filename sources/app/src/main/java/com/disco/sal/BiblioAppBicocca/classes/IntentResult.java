package com.disco.sal.biblioappbicocca.classes;


/*
 * Copyright 2009 ZXing authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * <p>Encapsulates the result of a barcode scan invoked through {@link IntentIntegrator}.</p>
 *
 * @author Sean Owen
 */
public final class IntentResult
{

    private String contents;
    private String formatName;
    private byte[] rawBytes2 = null;
    private Integer orientation2 = null;
    private String errore = null;

    IntentResult(String contents, String formatName)
    {
        this.contents = contents;
        this.formatName = formatName;
    }

    public IntentResult(String contents2, String formatName2, byte[] rawBytes, Integer orientation, String errorCorrectionLevel)
    {
        contents = contents2;
        formatName = formatName2;
        rawBytes2 = rawBytes;
        orientation2 = orientation;
        errore = errorCorrectionLevel;


    }

    public IntentResult()
    {

    }

    /**
     * @return raw content of barcode
     */
    public String getContents()
    {
        return contents;
    }

    /**
     * @return name of format, like "QR_CODE", "UPC_A". See <code>BarcodeFormat</code> for more format names.
     */
    public String getFormatName()
    {
        return formatName;
    }

}