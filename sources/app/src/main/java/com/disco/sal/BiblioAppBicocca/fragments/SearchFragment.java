package com.disco.sal.biblioappbicocca.fragments;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.SearchView;

import com.disco.sal.biblioappbicocca.R;
import com.disco.sal.biblioappbicocca.activities.AdvanceSearchActivity;
import com.disco.sal.biblioappbicocca.activities.MainActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class SearchFragment extends Fragment
{

    private SearchView searchView;
    private Button btnAdvanceSearch;

    public SearchFragment()
    {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_search, container, false);
        ((MainActivity) getActivity()).getSupportActionBar().setTitle(getString(R.string.search_title_actionbar));
        searchView = (SearchView) view.findViewById(R.id.searchView);
        btnAdvanceSearch = (Button) view.findViewById(R.id.btnAdvanceSearch);
        btnAdvanceSearch = (Button) view.findViewById(R.id.btnAdvanceSearch);
        btnAdvanceSearch.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                startActivity(new Intent(getActivity(), AdvanceSearchActivity.class));
            }
        });
        searchView.setIconified(false);
        searchView.clearFocus();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener()
        {
            @Override
            public boolean onQueryTextSubmit(String query)
            {
                // Hide Soft Keyboard
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(searchView.getWindowToken(), 0);
                MainActivity.reset();
                MainActivity.query = query;
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.container, ListDocumentsFromAlpehFragment.newInstance(MainActivity.query), "tag_fragment_listDocuments")
                        .commit();

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText)
            {
                if (newText.length() > 0)
                {
                    btnAdvanceSearch.setVisibility(View.INVISIBLE);
                }
                else
                {
                    btnAdvanceSearch.setVisibility(View.VISIBLE);
                }
                return false;
            }
        });

        return view;
    }


    public static Fragment newInstance()
    {
        SearchFragment fragment = new SearchFragment();
        return fragment;
    }


}
