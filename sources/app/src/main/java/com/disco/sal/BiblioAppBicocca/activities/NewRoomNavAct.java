package com.disco.sal.biblioappbicocca.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.disco.sal.biblioappbicocca.R;
import com.disco.sal.biblioappbicocca.adapters.AdapterSIS;
import com.disco.sal.biblioappbicocca.classes.AVGDistanceThreeNearestBeacons;
import com.disco.sal.biblioappbicocca.classes.Room;
import com.disco.sal.biblioappbicocca.classes.SISManager;
import com.estimote.sdk.Beacon;
import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.Region;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.GridLabelRenderer;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.PointsGraphSeries;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class NewRoomNavAct extends ActionBarActivity
{
    private static final Region ALL_ESTIMOTE_BEACONS_REGION = new Region("rid", null, null, null);   // creo una regione attorno a me dicendo di non filtrare alcun beacon

    private BeaconManager beaconManager = null;        // gestisce la ricerca dei beacons
    private String macNearestBeacon;                   // MAC del beacon più vicino a me
    private ProgressDialog progressDialog;             // associata al messaggio del tentativo di localizzazione fatto appena accendo il BT
    private ActionBarActivity context;                 // passo un context (l'activity stessa)
    private int n_rechecks;                            // indica il numero di volte che ho controllato di avere beacons attorno
    private int nManagedBeaconsAtTime;                 //indica il numero di beacons di cui l'euristica ha bisogno
    private boolean popupWrongRoom;                    //flag che indica se deve aprirsi il popup che indica all'utente di essere nella stanza sbagliata (falso = non deve aprirsi)
    private AVGDistanceThreeNearestBeacons euristica;  // tiene in memoria l'euristica
    private int countEuristica;                        // conta quante volte ho richiamato il codice della euristica, e dopo 4 volte prende la media
    private boolean wrongLibrary;                      // se è vera sono nella biblioteca sbagliata
    //private boolean isRangingStarted;
    private SisRoomFromName sisRoomFromName;           // costruisce una stanza prendendo in ingresso il nome (poi usa altre info ovviamente)
    private int seconds;                               // se arriva a 6 e non ho trovato beacons, si apre il popup della mancata localizzazione
    private Room currentRoom;                          // ha al suo interno la stanza corrente
    private boolean showBeaconNotFound;                // vera se posso aprire il popup in cui dico di non aver trovato beacons
    private String rightLibrary;                       // contiene il nome della biblio con il libro (preso dall'intent)
    private String rightRoom;                          // contiene il nome del piano con il libro (preso dall'intent)
    private ArrayList<String> shelvesbook;             // contiene i nomi degli scaffali con il libro (presi dall'intent)
    private String likelyCurrentRoom;                  // viene settata da myTimerTask e contiene il nome del piano in cui sono
    private Timer myTimer;                             // gestisce l'esecuzione di myTimerTask
    private MyTimerTask myTimerTask;                   // ogni 6 secondi prende il MAC del beacon più vicino e, tramite SIS, controlla in che piano sono
    private boolean defaultView;                       // vera se devo aprire la schermata col giusto piano perché non mi sono localizzato
    private int timesEuristica;                        // parametro che indica che dopo 4 volte sono pronto a prendere il valore settato nella euristica
    private Intent intent;

    // usate nella view per la parte grafica
    private AdapterSIS adapter;
    private ListView listsis;
    private ScrollView scrollView;
    private String sublibrary;
    private Toolbar toolbar;
    private GraphView graph;
    private TextView title, room, shelves;
    private PointsGraphSeries<DataPoint> userPosition;

    // MONITOR BTH STATUS
    private final BroadcastReceiver mBthReceiver = new BroadcastReceiver()
    {
        public void onReceive(Context context, Intent intent)
        {
            String action = intent.getAction();
            //controllo che sia stato chiamato per un cambiamento di stato del bt
            if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(action))
            {
                //torna -1 se non trova il risultato di extra_state. La prima parte mi restituisce il power state
                if (intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, -1) ==
                    BluetoothAdapter.STATE_TURNING_OFF)
                {
                    // Pulisco la posizione rimuovendo il segnalino dalla mappa.
                    if (userPosition != null)
                        graph.removeSeries(userPosition);

                    // Spengo Estimote
                    if(beaconManager != null)
                        beaconManager.stopRanging(ALL_ESTIMOTE_BEACONS_REGION);

                    //controllo che non sia null, se no qui sotto mi darebbe null pointer exception
                    if (sisRoomFromName != null)
                    {
                        //se sta andando lo cancello
                        if (sisRoomFromName.getStatus() == AsyncTask.Status.RUNNING)
                        {
                            sisRoomFromName.cancel(true);
                        }
                        //altrimenti cancello loro due (se sisRoomFromName sta andando, questi due
                        // qui sotto non saranno attivi)
                        else
                        {
                            myTimerTask.cancel();
                            myTimer.cancel();
                        }
                    }
                }
                else if (intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, -1) ==
                         BluetoothAdapter.STATE_TURNING_ON)
                {
                    //proseguo solo quando il bluetooth è pronto per essere usato
                    progressToScan();
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        context = this;
        intent = getIntent();
        rightLibrary = intent.getStringExtra("library");
        rightRoom = intent.getStringExtra("room");
        shelvesbook = intent.getStringArrayListExtra("shelvesbook");
        sublibrary = intent.getStringExtra("library").replace(" ", "_");

        nManagedBeaconsAtTime = 3; //settata così perché l'euristica accetta triplette
        timesEuristica = 4; //settata così, prendo la posizione dalla euristica dopo 4 volte che
        // quest'ultima è stata utilizzata
    }

    @Override
    public void onStop()
    {
        super.onStop();

        //stessi controlli fatti quando si spegne il BT
        if (sisRoomFromName != null)
        {
            if (sisRoomFromName.getStatus() == AsyncTask.Status.RUNNING)
            {
                sisRoomFromName.cancel(true);
            }
            else
            {
                myTimerTask.cancel();
                myTimer.cancel();
            }
        }

        // Spengo Estimote
        if(beaconManager != null)
            beaconManager.stopRanging(ALL_ESTIMOTE_BEACONS_REGION);

        //deassocio il broadcast receiver del BTH
        unregisterReceiver(mBthReceiver);
    }

    @Override
    protected void onStart()
    {
        super.onStart();

        // Register for broadcasts on BluetoothAdapter state change
        IntentFilter filter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);

        //il broadcast receiver viene associato all'intent filter di prima e verrà invocato con
        // qualsiasi broadcast intent che corrisponde al filtro
        registerReceiver(mBthReceiver, filter);

        // mi occupo del bluetooth
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        if (mBluetoothAdapter == null)
        {
            // Device does not support Bluetooth.
            AlertDialog.Builder alert = new AlertDialog.Builder(NewRoomNavAct.this);
            alert.setMessage(R.string.bt_not_supported);
            alert.setPositiveButton("OK", null);
            alert.setCancelable(true);
            alert.create().show();
        }
        else if (!mBluetoothAdapter.isEnabled())
        {
            //creo un intent per lanciare l'activity che permette all'utente di accendere il bluetooth
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);

            //faccio partire l'activity tramite l'intent
            startActivity(enableBtIntent);
        }
        else
        {
            //proseguo solo quando il bluetooth è già pronto per essere usato (in questo caso era già acceso)
            progressToScan();
        }
    }

    private void setVariables()
    {
        //setto tutti gli attributi della classe
        macNearestBeacon = null;
        showBeaconNotFound = true;
        popupWrongRoom = true;
        countEuristica = 0;
        n_rechecks = 0;
        wrongLibrary = false;
        //isRangingStarted = false;
        seconds = 0;
        likelyCurrentRoom = null;
        progressDialog = new ProgressDialog(context);
        euristica = new AVGDistanceThreeNearestBeacons();
        beaconManager = new BeaconManager(this);
        currentRoom = null;
        sisRoomFromName = new SisRoomFromName();
        defaultView = false;
        wrongLibrary = false;
    }

    // creo una classe privata che rappresenta un task da eseguire
    private class MyTimerTask extends TimerTask
    {
        @Override
        public void run()
        {
            //mi faccio restituire da SIS il piano a cui è associato il beacon più vicino (ovviamente
            // lo faccio se trovo beacons)
            if (macNearestBeacon != null)
            {
                ArrayList<String> tmpR = SISManager.getSISManager().getRoomFromBeacon(macNearestBeacon);
                likelyCurrentRoom = tmpR.get(0);
                System.out.println("ma que ci arrivo?");
            }
        }
    }

    //dopo che il BT è stato impostato vado avanti alla scansione
    private void progressToScan()
    {
        //setto gli attributi della classe principale
        setVariables();

        progressDialog.setCancelable(false);
        progressDialog.setTitle(R.string.wait_title);
        progressDialog.setMessage(context.getResources().getString(R.string.find_nearest_beacon));
        progressDialog.show();

        //setto il necessario per l'individuazione della stanza in cui sono (6 secondi tra una
        // individuazione e la successiva)
        myTimer = new Timer();
        myTimerTask = new MyTimerTask();
        myTimer.scheduleAtFixedRate(myTimerTask, 0, 5000);

        //setto il listener dei beacon (modalità ranging)
        beaconManager.setRangingListener(new BeaconManager.RangingListener()
        {
            // evento chiamato ogni secondo (si può modificare ogni quanto viene invocato)
            @Override
            public void onBeaconsDiscovered(Region region, final List beacons)
            {
                //se non trovo beacons attorno a me, parte la funzione che mi fa apparire un messaggio
                // spiegando il problema
                if (beacons.isEmpty())
                {
                    showAlertDlg();
                    return;
                }

                //altrimenti setto il valore di macNearestBeacon (i beacons sono ordinati in ordine
                // di vicinanza)
                macNearestBeacon = ((Beacon) beacons.get(0)).getMacAddress().toString();
                macNearestBeacon = macNearestBeacon.substring(1, macNearestBeacon.length() - 1);
                //System.out.println("BEACON_VICINO: " + macNearestBeacon);

                // da qui in avanti faccio roba solo se non si sta caricando la stanza in cui sono
                if (sisRoomFromName.getStatus() != AsyncTask.Status.RUNNING)
                {
                    //ovviamente non posso proseguire se il timer task non capisce in che piano sono
                    if (likelyCurrentRoom != null)
                    {
                        //se currentRoom è null, vuol dire che non ho mai caricato ancora alcun piano
                        //dunque mi segno il nome del piano e faccio partire la AsyncTask che prenderà
                        //da SIS i dati necessari ovviamente poi non faccio più nulla in attesa che
                        // la asyncTask finisca
                        if (currentRoom == null)
                        {
                            //System.out.println("ma qui ci arrivo?");
                            String tmpNameRoom = likelyCurrentRoom;
                            sisRoomFromName = new SisRoomFromName();
                            sisRoomFromName.execute(tmpNameRoom);
                            return;
                        }

                        //altrimenti prendo sempre il nome del piano rilevato e lo confronto con
                        // quello del piano in cui dovrei essere. Se è dverso faccio partire la asyncTask
                        // che cambierà piano prendendo i dati da SIS
                        else
                        {
                            String tmpNameRoom = likelyCurrentRoom;
                            if (!tmpNameRoom.equalsIgnoreCase(currentRoom.getName()))
                            {
                                sisRoomFromName = new SisRoomFromName();
                                sisRoomFromName.execute(tmpNameRoom);

                                //ho cambiato piano, dunque cancello il segnalino dell'utente
                                if (userPosition != null)
                                    graph.removeSeries(userPosition);

                                //setto da capo l'euristica
                                euristica = new AVGDistanceThreeNearestBeacons();
                                countEuristica = 0;
                                return;
                            }
                        }

                        //adesso preparo il necessario per euristica e posizionamento:
                        //alla nostra euristica servono 3 beacons alla volta, presi dalla stanza in
                        //cui sono, dunque recupero i primi tre oggetti beacons che trovo nella
                        //stanza (quelli più vicini) e i loro mac
                        ArrayList<Beacon> listaB = new ArrayList<Beacon>();
                        ArrayList<String> macs = new ArrayList<String>();
                        int i = 0;

                        while (i < nManagedBeaconsAtTime && i < beacons.size())
                        {
                            Beacon b = (Beacon) beacons.get(i);
                            String tmpMacBeac = b.getMacAddress().toString();
                            tmpMacBeac = tmpMacBeac.substring(1, tmpMacBeac.length() - 1);
                            if (currentRoom.getMacbeacons().contains(tmpMacBeac))
                            {
                                listaB.add(b);
                                macs.add(tmpMacBeac);
                            }
                            i++;
                        }

                        // Se non trovo beacon nella stanza??? ritorno!
                        if (listaB.size() <= 0)
                            return;

                        // se trovo un solo beacon nella stanza, faccio apparire un puntino dove c'è
                        // il beacon (l'utente sarà lì attorno)!!!
                        if (listaB.size() <= 2)
                        {
                            double[] c = currentRoom.getBeaconFromMac(macs.get(0)).getXYs();
                            updatePosition(c[0], c[1]);
                        }
                        //se i beacons trovati sono davvero 3 e non meno, recupero dal piano corrente
                        // la loro posizione passo i tre beacons trovati e la loro posizione alla
                        // euristica
                        //else if (listaB.size() == nManagedBeaconsAtTime)
                        else
                        {
                            double[][] centers = new double[3][2];
                            for (int j = 0; j < macs.size(); j++)
                            {
                                double[] c = currentRoom.getBeaconFromMac(macs.get(j)).getXYs();
                                centers[j][0] = c[0];
                                centers[j][1] = c[1];
                            }
                            euristica.heuristic(listaB, centers);
                            countEuristica++;

                            //l'euristica prepara il valore della media dopo 4 volte (dovuto proprio
                            // alla euristica usata)
                            if (countEuristica == timesEuristica)
                            {
                                countEuristica = 0;
                                double[] posizione = euristica.getMedia();

                                // a questo punto visualizzo la posizione
                                updatePosition(posizione[0], posizione[1]);
                            }
                        }
                    }
                }
            }
        });

        //faccio partire la modalità ranging
        beaconManager.connect(new BeaconManager.ServiceReadyCallback()
        {
            @Override
            public void onServiceReady()
            {
                //isRangingStarted = true;
                beaconManager.startRanging(ALL_ESTIMOTE_BEACONS_REGION);
            }
        });
    }

    //invocata ogni volta che onBeaconsDiscovered non mi passa beacons. So che onBeaconsDiscovered
    // viene invocata ogni secondo dunque attendo 6 secondi prima di far apparire un messaggio di errore.
    // Se l'utente preme "no" al popup che si apre, parte la asyncTask che recupera la Room col libro.
    // Altrimenti viene fatto un nuovo tentativo di ricerca beacons.
    // Vengono fatti al massimo 4 tentativi, dopodiché appare quanto è stato già detto al click su "no"
    private void showAlertDlg()
    {
        if (showBeaconNotFound)
        {
            seconds++;
            if (seconds == 6)
            {
                seconds = 0;
                n_rechecks++;

                if (progressDialog != null && progressDialog.isShowing())
                {
                    progressDialog.dismiss();
                }

                AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
                builder1.setMessage(R.string.beacons_not_found);
                builder1.setPositiveButton(R.string.affirmative_answer, new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int id)
                    {
                        // User clicked OK button
                        if (n_rechecks >= 4)
                        {
                            defaultView = true;
                            sisRoomFromName = new SisRoomFromName();
                            sisRoomFromName.execute(rightRoom);
                        }
                        showBeaconNotFound = true;
                    }
                });
                builder1.setNegativeButton(R.string.negative_answer, new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int id)
                    {
                        // User cancelled the dialog
                        // è andata male, faccio apparire le info della altra activity con un
                        // messaggio di errore
                        defaultView = true;
                        sisRoomFromName = new SisRoomFromName();
                        sisRoomFromName.execute(rightRoom);
                    }
                });
                AlertDialog dialog1 = builder1.create();
                dialog1.setCancelable(false);
                dialog1.setCanceledOnTouchOutside(false);
                showBeaconNotFound = false;
                dialog1.show();
            }
        }

    }

    //popup richiamato nell'asyncTask che prende i dati da sis del piano corrente e li visualizza
    //controllo di essere nel piano col libro selezionato, se no e se questo stesso popup è stato
    //chiuso in precedenza, allora lo faccio apparire
    private void checkRightRoom()
    {
        if (!currentRoom.getName().equalsIgnoreCase(rightRoom))
        {
            if (popupWrongRoom)
            {
                popupWrongRoom = false;
                AlertDialog.Builder builder2 = new AlertDialog.Builder(context);
                String msg = getResources().getString(R.string.wrong_room);
                msg = msg + " " + rightRoom;
                msg = msg.replace("_", " ");
                builder2.setMessage(msg);
                builder2.setPositiveButton("OK", new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int id)
                    {
                        // User clicked OK button
                        popupWrongRoom = true;
                    }
                });
                AlertDialog dialog2 = builder2.create();
                dialog2.setCancelable(false);
                dialog2.setCanceledOnTouchOutside(false);
                dialog2.show();
            }
        }
    }

    // simile a sopra, ma ora controllo se sono nella biblio giusta, se no appare il popup in cui
    // dico dove recarmi poi spengo la ricerca dei beacons
    private void checkRightLibrary()
    {
        if (!rightLibrary.equalsIgnoreCase(currentRoom.getParentLibrary().get(0)))
        {
            wrongLibrary = true;
            AlertDialog.Builder builder3 = new AlertDialog.Builder(context);
            String msg = currentRoom.getParentLibrary().get(0);
            msg = msg + " " + getResources().getString(R.string.wrong_library);
            msg = msg + " " + rightLibrary + " " + rightRoom;
            msg = msg.replace("_", " ");
            builder3.setMessage(msg);
            builder3.setPositiveButton("OK", null);
            AlertDialog dialog3 = builder3.create();
            dialog3.setCancelable(false);
            dialog3.setCanceledOnTouchOutside(false);
            dialog3.show();

            // Spengo Estimote
            if(beaconManager != null)
                beaconManager.stopRanging(ALL_ESTIMOTE_BEACONS_REGION);
        }
    }

    //la view viene sempre richiamata nell'unica asyncTask presente. Si occupa di caricare il necessario
    //per far apparire il piano corrente. Tolto il controllo se sono nella sede centrale della bicocca
    private void view()
    {
        runOnUiThread(new Runnable()
        {
            public void run()
            {
                setContentView(R.layout.activity_room);
                scrollView = (ScrollView) findViewById(R.id.scrollViewSIS);
                scrollView.smoothScrollTo(0, 0);
                //intent = getIntent();
                listsis = (ListView) findViewById(R.id.listViewSIS);
                graph = (GraphView) findViewById(R.id.SISview);
                graph.removeAllSeries();
                toolbar = (Toolbar) findViewById(R.id.toolbargeneric);
                toolbar.setTitle("Locazione");
                setSupportActionBar(toolbar);
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setDisplayShowHomeEnabled(true);
                context.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                context.getSupportActionBar().setDisplayShowHomeEnabled(true);
                toolbar.setNavigationOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        onBackPressed();
                    }
                });
                room = (TextView) findViewById(R.id.textViewRoom);
                sublibrary = currentRoom.getParentLibrary().get(0);
                title = (TextView) findViewById(R.id.textViewSisTitle);
                title.setText(sublibrary);
                shelves = (TextView) findViewById(R.id.textNumbershelves);
                shelves.setText(String.valueOf(currentRoom.getSizeShelves()));
                graph.removeAllSeries();

                // MODIFICATO PER RIMOZIONE CHECK MAPPE
                // Solo adesso è possibile utilizzare liberamente l'app
                // con mappature di biblioteche differenti su SIS (solo se presenti su db)!!
                //if (sublibrary.contains("Bicocca_Sede_Centrale"))
                //{
                    room.setText(currentRoom.getName());
                    drawGraph();
                    currentRoom.drawRoom(graph);

                    adapter = new AdapterSIS(context, currentRoom);
                    listsis = (ListView) findViewById(R.id.listViewSIS);
                    listsis.setAdapter(adapter);
                    adapter.replaceWith(currentRoom.getShelves());

                    listsis.setOnItemClickListener(new AdapterView.OnItemClickListener()
                    {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id)
                        {
                            adapter.changeStatus(position, graph);
                            adapter.replaceWith(currentRoom.getShelves());
                        }
                    });

                    listsis.setOnTouchListener(new View.OnTouchListener()
                    {
                        // Setting on Touch Listener for handling the touch inside ScrollView
                        @Override
                        public boolean onTouch(View v, MotionEvent event)
                        {
                            // Disallow the touch request for parent scroll on touch of child view
                            v.getParent().requestDisallowInterceptTouchEvent(true);
                            return false;
                        }
                    });
                //}
            }
        });
    }

    //usato dalla view. Sistemo il necessario per far apparire il grafico del piano da mostrare
    private void drawGraph()
    {
        graph.getGridLabelRenderer().setGridStyle(GridLabelRenderer.GridStyle.NONE);
        graph.getGridLabelRenderer().setHorizontalLabelsVisible(false);
        graph.getGridLabelRenderer().setVerticalLabelsVisible(false);
        graph.getViewport().setXAxisBoundsManual(true);
        graph.getViewport().setYAxisBoundsManual(true);

        //calcolo dimensione grafico in base alla dimensione della stanza selezionata
        // (il grafico deve essere quadrato per le proporzioni grafiche)
        double dimGraph = 0;
        for (double[] xy : currentRoom.getXYs())
        {
            if (xy[0] > dimGraph)
            {
                dimGraph = xy[0];
            }
            if (xy[1] > dimGraph)
            {
                dimGraph = xy[1];
            }
        }
        graph.getViewport().setMaxX(dimGraph);
        graph.getViewport().setMaxY(dimGraph);
    }

    //usata dopo che ho in mano la media di 4 posizioni. Fa apparire la posizione dell'utente ma prima
    //rimuove dalla mappa la precedente posizione
    public void updatePosition(double x, double y)
    {
        if (userPosition != null)
            graph.removeSeries(userPosition);

        userPosition = new PointsGraphSeries<DataPoint>(new DataPoint[]{
                new DataPoint(x, y),
        });
        userPosition.setShape(PointsGraphSeries.Shape.POINT);
        userPosition.setColor(Color.parseColor("#FFFF4444"));
        userPosition.setSize(8);
        graph.addSeries(userPosition);
    }

    // classe necessaria per recuperare da SIS le info del piano corrente.
    // in onPreExecute cancello il timerTask e l'oggetto che lo gestisce e poi faccio apparire un
    // popup che indica il reperimento delle informazioni della Room.
    // In doInBackground faccio la richiesta a SIS per le info invocando un metodo che recupera
    // tutte le info di una Room e la monta, a partire dal nome della Room stessa e da eventuali
    // scaffali da "illuminare".
    // Da notare che le richieste al server posso farle SOLO in una AsyncTask.
    // In onPostExecute faccio sparire il popup apparso prima, poi controllo in che biblioteca sono.
    // Se sono nella giusta biblioteca, faccio apparire la stanza in cui sono usando la view, poi
    // controllo di essere nella giusta Room e poi...
    // 1) se ho beacons attorno a me, faccio ripartire il timer e il suo gestore
    // 2) se la asyncTask è stata richiamata perché non ho trovato beacons, allora non li faccio
    // ripartire e appare invece un messaggio di errore
    private class SisRoomFromName extends AsyncTask<String, Void, Void>
    {
        ProgressDialog p1;

        @Override
        protected void onPreExecute()
        {
            myTimerTask.cancel();
            myTimer.cancel();

            if (progressDialog != null && progressDialog.isShowing())
            {
                progressDialog.dismiss();
            }

            p1 = new ProgressDialog(context);
            p1.setMessage(context.getResources().getString(R.string.room_loading));
            p1.setCancelable(false);
            p1.show();
        }

        protected Void doInBackground(String... name)
        {
            Room tmpRoom;

            if (name[0].equalsIgnoreCase(rightRoom))
                tmpRoom = SISManager.getSISManager().getObjectRoom(name[0], shelvesbook);
            else
                tmpRoom = SISManager.getSISManager().getObjectRoom(name[0], new ArrayList<String>());

            currentRoom = tmpRoom;
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid)
        {
            p1.dismiss();
            checkRightLibrary();
            if (!wrongLibrary)
            {
                view();
                checkRightRoom();
                if (!defaultView)
                {
                    myTimer = new Timer();
                    myTimerTask = new MyTimerTask();
                    myTimer.scheduleAtFixedRate(myTimerTask, 0, 5000);
                }
                else
                {
                    AlertDialog.Builder alertFinal = new AlertDialog.Builder(NewRoomNavAct.this);
                    alertFinal.setMessage(R.string.localization_ko);
                    alertFinal.setPositiveButton("OK", null);
                    alertFinal.setCancelable(true);
                    alertFinal.create().show();

                    // Spengo Estimote
                    if(beaconManager != null)
                        beaconManager.stopRanging(ALL_ESTIMOTE_BEACONS_REGION);
                }
            }
        }
    }
}
