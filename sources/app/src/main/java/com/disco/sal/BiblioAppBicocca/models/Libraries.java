package com.disco.sal.biblioappbicocca.models;

import com.disco.sal.biblioappbicocca.models.libraryitemdettails.Library;

import java.util.ArrayList;

/**
 * Created by lorenzo on 17/09/15.
 */
public class Libraries {
    private ArrayList<Library> libraries;

    public Libraries(ArrayList<Library> libraries) {
        this.libraries = libraries;
    }

    public Libraries(){
        libraries = new ArrayList<>();
    }

    public ArrayList<Library> getLibraries() {

        return libraries;
    }

    public void setLibraries(ArrayList<Library> libraries) {
        this.libraries = libraries;
    }
}
