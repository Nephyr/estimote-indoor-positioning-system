package com.disco.sal.biblioappbicocca.models;

/**
 * Created by lorenzo on 05/05/15.
 */
public class SubFilter {
    private String id;
    private String name;
    private String numberOfNew;
    private String url;
    private int idImage;

    public SubFilter(String id, String name, String numberOfNew, String url, int image) {
        this.id = id;
        this.name = name;
        this.numberOfNew = numberOfNew;
        this.url = url;
        this.idImage = image;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumberOfNew() {
        return numberOfNew;
    }

    public void setNumberOfNew(String numberOfNew) {
        this.numberOfNew = numberOfNew;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getIdImage() {
        return idImage;
    }

    public void setIdImage(int idImage) {
        this.idImage = idImage;
    }
}
