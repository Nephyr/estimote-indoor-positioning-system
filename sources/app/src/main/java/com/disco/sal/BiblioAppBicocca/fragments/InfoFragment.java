package com.disco.sal.biblioappbicocca.fragments;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.disco.sal.biblioappbicocca.R;

public class InfoFragment extends Fragment
{

    View myInflatedView;
    RelativeLayout relativeLayoutFirstContact;
    RelativeLayout relativeLayoutSecondContact;

    public static InfoFragment newInstance()
    {
        InfoFragment fragment = new InfoFragment();
        return fragment;
    }


    public InfoFragment()
    {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        myInflatedView = inflater.inflate(R.layout.fragment_info, container, false);
        relativeLayoutFirstContact = (RelativeLayout) myInflatedView.findViewById(R.id.email_first_contact_relative_layout);
        relativeLayoutFirstContact.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent emailIntent = new Intent(Intent.ACTION_SEND);
                emailIntent.setType("text/html");
                emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{
                        getActivity().getResources().getString(R.string.email_first_contact)});
                getActivity().startActivity(emailIntent);
            }
        });
        relativeLayoutSecondContact = (RelativeLayout) myInflatedView.findViewById(R.id.email_second_contact_relative_layout);
        relativeLayoutSecondContact.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent emailIntent = new Intent(Intent.ACTION_SEND);
                emailIntent.setType("text/html");
                emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{
                        getActivity().getResources().getString(R.string.email_second_contact)});
                getActivity().startActivity(emailIntent);
            }
        });
        ImageView logoSal = (ImageView) myInflatedView.findViewById(R.id.logo_sal);
        logoSal.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.sal.disco.unimib.it/"));
                startActivity(browserIntent);
            }
        });
        ImageView logoBicocca = (ImageView) myInflatedView.findViewById(R.id.logo_bicocca);
        logoBicocca.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.unimib.it/"));
                startActivity(browserIntent);
            }
        });
        ImageView logoLibraryBicocca = (ImageView) myInflatedView.findViewById(R.id.logo_library_bicocca);
        logoLibraryBicocca.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.biblio.unimib.it/"));
                startActivity(browserIntent);

            }
        });
        TextView icon8 = (TextView) myInflatedView.findViewById(R.id.icons8_credits_website);
        icon8.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://icons8.com/"));
                startActivity(browserIntent);
            }
        });

        return myInflatedView;
    }


}
