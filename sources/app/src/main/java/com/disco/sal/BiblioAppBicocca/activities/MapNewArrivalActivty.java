package com.disco.sal.biblioappbicocca.activities;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.disco.sal.biblioappbicocca.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapNewArrivalActivty extends FragmentActivity
{
    GoogleMap supportMap;
    String subLibrary;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_new_arrival);
        supportMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapNewArrive)).getMap();
        Intent intent = getIntent();
        subLibrary = intent.getStringExtra("library");
        if (GooglePlayServicesUtil.isGooglePlayServicesAvailable(this) == ConnectionResult.SUCCESS)
        {
            setMap();
        }
        // TODO: 30/06/15 crare apikey appostita per app
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        return super.onOptionsItemSelected(item);
    }

    public void setMap()
    {
        String libraryAddress = "";
        LatLng coordinate = null;
        if (subLibrary.equalsIgnoreCase("Sede Centrale"))
        {
            coordinate = new LatLng(45.518447, 9.213905);
            libraryAddress = "Piazza dell'Ateneo Nuovo, 1 - 20126 Milano\n(building U6, floor II)";
        }
        else if (subLibrary.equalsIgnoreCase("Sede di Scienze"))
        {
            coordinate = new LatLng(45.513734, 9.211929);
            libraryAddress = "Piazza della Scienza, 3 - 20126 Milano\nbuilding U2, floor I (books section)\nand floor -I (journals section)";
        }
        else if (subLibrary.equalsIgnoreCase("Sede di Medicina"))
        {
            coordinate = new LatLng(45.604118, 9.261077);
            libraryAddress = "Via Cadore, 48 - 20900 Monza\n(building U8, ground floor)";
        }
        else if (subLibrary.equalsIgnoreCase("Biblioteca di Medicina e di Scienze"))
        {
            coordinate = new LatLng(45.796774, 8.848428);
            libraryAddress = "Via J. H. Dunant, 3 - 21100 Varese\ntel.: 0332/421.426\ne-mail: bibliobiomedica@uninsubria.it";
        }
        else if (subLibrary.equalsIgnoreCase("Biblioteca di Economia"))
        {
            coordinate = new LatLng(45.798159, 8.852641);
            libraryAddress = "Via Monte Generoso, 71 - 21100 Varese\ntel. sala lettura/prestito: 0332/39.5103-4\ne-mail: biblioeconomia@uninsubria.it";
        }
        else if (subLibrary.equalsIgnoreCase("Biblioteca di Giurisprudenza"))
        {
            coordinate = new LatLng(45.811193, 9.075758);
            libraryAddress = "Via M.E. Bossi, n. 5 - 22100 Como\ntel. +39 031-238 4175\ne-mail: bibliogiurisprudenza@uninsubria.it";
        }
        else if (subLibrary.equalsIgnoreCase("Biblioteca di Scienze Como"))
        {
            coordinate = new LatLng(45.802087, 9.094623);
            libraryAddress = "Via Valleggio, 11 – 22100 Como\ntel. Sala 2: 031/238.9566\ntel. Sala 3: 031/238.9565\ne-mail: biblioscienze@uninsubria.it";
        }
        else
            coordinate = new LatLng(0, 0);

        supportMap.setMyLocationEnabled(true);
        supportMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener()
        {
            @Override
            public void onMyLocationChange(Location arg0)
            {
                supportMap.addMarker(new MarkerOptions().position(new LatLng(arg0.getLatitude(), arg0.getLongitude())).title(getString(R.string.user_position)));
            }
        });

        MarkerOptions mark = new MarkerOptions();
        mark.position(coordinate);
        mark.title(subLibrary);
        mark.snippet(libraryAddress);

        supportMap.addMarker(mark);
        CameraUpdate center = CameraUpdateFactory.newLatLng(coordinate);
        CameraUpdate zoom = CameraUpdateFactory.zoomTo(15);
        supportMap.moveCamera(center);
        supportMap.animateCamera(zoom);

    }
}
