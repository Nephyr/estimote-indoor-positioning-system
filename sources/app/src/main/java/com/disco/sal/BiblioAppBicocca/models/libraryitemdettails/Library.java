package com.disco.sal.biblioappbicocca.models.libraryitemdettails;

/**
 * Created by lorenzo on 17/09/15.
 */
public class Library {
    private String nameLibrary;
    private String urlLibrary;
    private ItemLibrary itemLibrary;

    public Library(String nameLibrary, String urlLibrary,ItemLibrary itemLibrary){
        this.itemLibrary = new ItemLibrary();
        this.itemLibrary = itemLibrary;
        this.nameLibrary = nameLibrary;
        this.urlLibrary = urlLibrary;

    }

    public String getNameLibrary() {
        return nameLibrary;
    }

    public void setNameLibrary(String nameLibrary) {
        this.nameLibrary = nameLibrary;
    }

    public String getUrlLibrary() {
        return urlLibrary;
    }

    public void setUrlLibrary(String urlLibrary) {
        this.urlLibrary = urlLibrary;
    }

    public ItemLibrary getItemLibrary() {
        return itemLibrary;
    }

    public void setItemLibrary(ItemLibrary itemLibrary) {
        this.itemLibrary = itemLibrary;
    }
}
