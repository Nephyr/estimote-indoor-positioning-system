package com.disco.sal.biblioappbicocca.classes;

import android.graphics.Color;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Matteo Vegini on 08/11/2015.
 * Modified by Marco Sgura on 05/02/2016
 */
public class Room implements Serializable
{
    String name;
    ArrayList<String> parentLibrary;
    ArrayList<double[]> XYs; //lista spigloli in metri
    ArrayList<Shelf> shelves;
    ArrayList<Shelf> shelvesbook;
    ArrayList<Shelf> gardens;
    ArrayList<String> nameShelvesbook;
    ArrayList<Estimote> beacons;
    ArrayList<String> macbeacons;

    // VECCHIA VERSIONE - NON PIU' UTILI
    //SISManager sis_manager;
    //ArrayList<String> listshe;
    //ArrayList<String> macbeacons;
    //ArrayList<Estimote> beacons;

    public void setParentLibrary(ArrayList<String> _libs) { this.parentLibrary = _libs; }

    public void setShelves(ArrayList<Shelf> _shelves) { this.shelves = _shelves; }

    public void setShelvesbook(ArrayList<Shelf> _shelvesbook) { this.shelvesbook = _shelvesbook; }

    public void setGardens(ArrayList<Shelf> _gardens) { this.gardens = _gardens; }

    public void setMacbeacons(ArrayList<String> macbeacons)
    {
        this.macbeacons = macbeacons;
    }

    public void setNameShelvesbook(ArrayList<String> nameShelvesbook)
    {
        this.nameShelvesbook = nameShelvesbook;
    }

    public void setBeacons(ArrayList<Estimote> beacons)
    {
        this.beacons = beacons;
    }


    public Room(String name)
    {
        // NUOVA VERSIONE
        this.name = name;
        this.shelves = new ArrayList<Shelf>(); // Meglio vuote che NullPointerEx.
        this.shelvesbook = new ArrayList<Shelf>(); // Meglio vuote che NullPointerEx.
        this.gardens = new ArrayList<Shelf>(); // Meglio vuote che NullPointerEx.
        this.parentLibrary = new ArrayList<String>();
        this.nameShelvesbook = new ArrayList<String>();
        this.beacons = new ArrayList<Estimote>();

        // VECCHIA VERSIONE
        //this.name = name;
        //this.sis_manager = SISManager.getSISManager();
        //this.setXYsFromCells(sis_manager.getRoom(name));
        //this.shelves = new ArrayList<Shelf>();
        //this.gardens = new ArrayList<Shelf>();
        //this.shelvesbook = new ArrayList<Shelf>();
        //this.beacons = new ArrayList<Estimote>();
        //this.listshe = sis_manager.getListShelves(name);
        //this.macbeacons = sis_manager.getListBeacs(name);

        //con sto pezzo stampo il nome dei beacons (non ricevo altro oltre al nome che è il MAC)
        /*for(String s : beacons)
        {
            System.out.println("nada da fare con i beacons?");
            System.out.println(s);
        }*/

        /*for(String shelf : shelfbook)
        {
            Shelf s = new Shelf(shelf,sis_manager);
            this.shelvesbook.add(s);
            listshe.remove(s);
        }

        sis_manager.getListShelves(name).remove(shelfbook);

        //per ogni scaffale, lo creo e lo aggiungo alla lista degli scaffali di questa stanza
        for(String shelf : listshe){
            Shelf s = new Shelf(shelf,sis_manager);
            if(shelf.split("_")[0].compareTo("Garden") == 0)
                this.gardens.add(s);
            else
                shelves.add(s);
        }*/

        //per ogni beacon, lo creo (da solo si setta la posizione) e lo aggiungo alla lista dei beacon di questa stanza
        /*for(String macbeacon : macbeacons)
        {
            Estimote b = new Estimote(macbeacon, sis_manager);
            beacons.add(b);
        }*/
    }

    public void drawRoom(GraphView g)
    {
        //disegno stanza
        double x1, y1, x2, y2;
        int size = this.XYs.size();
        LineGraphSeries<DataPoint> roomWall;
        for (int i = 0; i < size - 1; i++)
        {
            x1 = (this.XYs.get(i))[0];
            y1 = (this.XYs.get(i))[1];
            x2 = (this.XYs.get(i + 1))[0];
            y2 = (this.XYs.get(i + 1))[1];
            roomWall = new LineGraphSeries<DataPoint>(new DataPoint[]{
                    new DataPoint(x1, y1),
                    new DataPoint(x2, y2),
            });
            roomWall.setColor(Color.parseColor("#333333"));
            g.addSeries(roomWall);
        }
        x1 = (this.XYs.get(0))[0];
        y1 = (this.XYs.get(0))[1];
        x2 = (this.XYs.get(size - 1))[0];
        y2 = (this.XYs.get(size - 1))[1];
        roomWall = new LineGraphSeries<DataPoint>(new DataPoint[]{
                new DataPoint(x1, y1),
                new DataPoint(x2, y2),
        });
        roomWall.setColor(Color.parseColor("#333333"));
        g.addSeries(roomWall);

        //disegno scaffali
        for (Shelf shelf : this.shelves)
        {
            shelf.drawShelf(g);
        }

        for (Shelf shelf : this.shelvesbook)
        {
            shelf.colorShelf(g);
        }

        for (Shelf grd : this.gardens)
        {
            grd.colorGarden(g);
        }

    }

    public void setXYsFromCells(ArrayList<String> cellslist)
    {
        ArrayList<double[]> list = new ArrayList<>();
        double[] tmp;
        for (int i = 0; i < cellslist.size(); i++)
        {
            tmp = new double[2];
            tmp[0] = Double.parseDouble(cellslist.get(i).split(",")[0]);
            tmp[1] = Double.parseDouble(cellslist.get(i).split(",")[1]);
            list.add(tmp);
        }

        double x1 = list.get(0)[0] / 10;
        double y1 = list.get(0)[1] / 10;
        double x2 = 0, y2 = 0;

        for (int i = 0; i < list.size(); i++)
        {
            if ((list.get(i)[0] / 10) < x1)
                x1 = (list.get(i)[0] / 10);
            if ((list.get(i)[1] / 10) < y1)
                y1 = (list.get(i)[1] / 10);

            if (((list.get(i)[0] / 10) + 0.1) > x2)
                x2 = (list.get(i)[0] / 10) + 0.1;
            if (((list.get(i)[1] / 10) + 0.1) > y2)
                y2 = (list.get(i)[1] / 10) + 0.1;
        }

        ArrayList<double[]> XYs = new ArrayList<>();
        XYs.add(new double[]{x1, y1});
        XYs.add(new double[]{x2, y1});
        XYs.add(new double[]{x2, y2});
        XYs.add(new double[]{x1, y2});

        this.XYs = XYs;
    }

    //ritorna la shelf dato il nome se presente nella room corrente
    public Shelf getShelfFromName(String shelf)
    {
        for (Shelf s : this.shelves)
        {
            if (s.getName().equals(shelf))
            {
                return s;
            }
        }
        return null;
    }

    public Estimote getBeaconFromMac(String mac)
    {
        for (Estimote e : this.beacons)
        {
            if (e.getMacaddress().equalsIgnoreCase(mac))
            {
                return e;
            }
        }
        return null;
    }

    public String getName()
    {
        return this.name;
    }

    public ArrayList<double[]> getXYs()
    {
        return this.XYs;
    }

    public ArrayList<Shelf> getShelves()
    {
        return this.shelvesbook;
    }

    public int getSizeShelves()
    {
        return this.shelvesbook.size();
    }

    public ArrayList<String> getParentLibrary() { return this.parentLibrary; }

    public ArrayList<Shelf> getShelvesbook()
    {
        return shelvesbook;
    }

    public ArrayList<String> getNameShelvesbook()
    {
        return nameShelvesbook;
    }

    public ArrayList<Estimote> getBeacons() { return this.beacons; }

    public ArrayList<String> getMacbeacons() {return this.macbeacons; }
}

