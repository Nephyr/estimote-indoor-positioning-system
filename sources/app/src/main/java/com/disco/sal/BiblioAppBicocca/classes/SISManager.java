package com.disco.sal.biblioappbicocca.classes;

import java.util.ArrayList;
import java.util.List;

import sal.sis.SIS;
import sal.sis.SpatialModels;
import sal.sis.datatypes.EnumerativeContext;
import sal.sis.datatypes.Grid2DSpaceParameters;
import sal.sis.datatypes.Location;
import sal.sis.datatypes.MappingDescription;
import sal.sis.datatypes.Name;
import sal.sis.datatypes.NameSpaceParameters;
import sal.sis.datatypes.Node;
import sal.sis.datatypes.OrientedWeightedGraphParameters;
import sal.sis.datatypes.SpaceInformation;
import sal.sis.datatypes.WeightedEdge;
import sal.sis.exceptions.SISServiceException;
import sal.sis.interfaces.IMappingsInspector;
import sal.sis.interfaces.IMappingsManager;
import sal.sis.interfaces.ISpaceInspector;
import sal.sis.interfaces.ISpaceManager;
import sal.sis.services.SISServiceFactory;

/**
 * Created by Matteo Vegini on 08/11/2015.
 * Modified by Marco Sgura on 05/02/2016
 */
public class SISManager
{

    SIS _sis;
    ISpaceManager spaceManager;
    ISpaceInspector spaceInspector;
    IMappingsManager mappingsManager;
    IMappingsInspector mappingsInspector;

    private static SISManager sisManager;


    public static SISManager getSISManager()
    {
        if (sisManager == null)
        {
            sisManager = new SISManager();
        }
        return sisManager;
    }

    private SISManager()
    {
        _sis = SISServiceFactory.build("http://149.132.178.195:8080/SIS2ws-dev/", "141db2486934d803e1fcb5baa3dccdec");
        spaceManager = _sis.spaceManager();
        spaceInspector = _sis.spaceInspector();
        mappingsManager = _sis.mappingsManager();
        mappingsInspector = _sis.mappingsInspector();
        setAllNamespaces();
    }

    //CREA SPAZI DI NOMI
    public void setAllNamespaces()
    {
        setNameSpaceLibraries();
        setNameSpacesRooms();
        setNameSpacesBeacons();
        setNameSpacesShelves();
    }

    //Crea lo spazio dei nomi delle librarie se non esiste
    public void setNameSpaceLibraries()
    {
        try
        {
            ArrayList<String> spaces = (ArrayList<String>) spaceInspector.getSpaceNames();
            if (!spaces.contains("bambook.Libraries"))
            {
                NameSpaceParameters params = new NameSpaceParameters(new ArrayList<String>()); //spazio dei nomi delle stanze

                spaceManager.defSpace("bambook.Libraries", SpatialModels.NAME, params);

            }
        } catch (SISServiceException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    //Crea lo spazio dei nomi delle stanze se non esiste
    public void setNameSpacesRooms()
    {
        try
        {
            List<String> spaces = spaceInspector.getSpaceNames();
            if (!spaces.contains("bambook.Rooms"))
            {
                NameSpaceParameters params = new NameSpaceParameters(new ArrayList<String>()); //spazio dei nomi delle stanze

                spaceManager.defSpace("bambook.Rooms", SpatialModels.NAME, params);

            }
        } catch (SISServiceException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    //Crea lo spazio dei nomi  degli scaffali se non esiste
    public void setNameSpacesShelves()
    {
        try
        {
            List<String> spaces = spaceInspector.getSpaceNames();
            if (!spaces.contains("bambook.Shelves"))
            {
                NameSpaceParameters params = new NameSpaceParameters(new ArrayList<String>()); //spazio dei nomi degli scaffali
                spaceManager.defSpace("bambook.Shelves", SpatialModels.NAME, params);
            }
        } catch (SISServiceException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    //Crea lo spazio dei nomi dei beacon se non esiste
    public void setNameSpacesBeacons()
    {
        try
        {
            List<String> spaces = spaceInspector.getSpaceNames();
            if (!spaces.contains("bambook.Beacons"))
            {
                NameSpaceParameters params = new NameSpaceParameters(new ArrayList<String>()); //spazio dei nomi dei Beacons

                spaceManager.defSpace("bambook.Beacons", SpatialModels.NAME, params);
            }
        } catch (SISServiceException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    //crea una biblioteca
    public void createLibrary(String library)
    {
        try
        {
            if (!spaceInspector.locationExists("bambook.Libraries", new Name(library)))
            {
                spaceManager.addLocation("bambook.Libraries", new Name(library));
                OrientedWeightedGraphParameters params = new OrientedWeightedGraphParameters();
                params.getNodeList().add("root");
                spaceManager.defSpace("bambook." + library +
                                      ".graph", SpatialModels.ORIENTEDWEIGHTEDGRAPH, params);
            }
            else
                System.out.println("La biblioteca con nome :" + library + " esiste gi?.");
        } catch (SISServiceException e2)
        {
            // TODO Auto-generated catch block
            e2.printStackTrace();
        }
    }

    //crea una stanza
    public void createRoom(String library, String name, double x, double y)
    {
        int x_i, y_i;
        x = x * 10;
        y = y * 10;
        x_i = (int) x;
        y_i = (int) y;
        try
        {
            List<String> spaces = spaceInspector.getSpaceNames();
            //spaces = spaceInspector.getSpaceNames();

            if ((!spaces.contains("bambook.Grid." + name)) &&
                spaceInspector.locationExists("bambook.Libraries", new Name(library)))
            {
                ArrayList<String> cell_list = new ArrayList<String>();
                Grid2DSpaceParameters params = new Grid2DSpaceParameters();
                EnumerativeContext grid, name_room, lib;
                params.setMinX(0);
                params.setMinY(0);
                params.setMaxX(x_i);
                params.setMaxY(y_i);

                spaceManager.defSpace("bambook.Grid." + name, SpatialModels.GRID2D, params);

                for (int i = 0; i < x_i; i++)
                {
                    for (int j = 0; j < y_i; j++)
                    {
                        cell_list.add(i + "," + j);
                    }
                }
                Name newroom = new Name(name);
                grid = new EnumerativeContext("bambook.Grid." + name, cell_list);
                try
                {
                    spaceManager.addLocation("bambook.Rooms", newroom);
                } catch (SISServiceException e1)
                {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }

                name_room = new EnumerativeContext("bambook.Rooms", name);
                lib = new EnumerativeContext("bambook.Libraries", library);
                try
                {
                    mappingsManager.map(name_room, grid);
                    mappingsManager.map(name_room, lib);
                } catch (SISServiceException e)
                {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            else
                System.out.println(
                        "La room chiamata " + name + " esiste gia' o la biblioteca " + library +
                        " non esite.");
        } catch (SISServiceException e2)
        {
            // TODO Auto-generated catch block
            e2.printStackTrace();
        }
    }

    //crea uno scaffale all'interno di una stanza (edges deve essere la lista degli spigoli di uno scaffale ordinata, ATTENZIONE: stanza 3x4 -> (0,0) - (29,39))
    public void createShelf(String shelf, String room, ArrayList<String> edges)
    {
        int x, y, x_p, y_p, x_i, y_i, edge_xmin, edge_xmax, edge_ymin, edge_ymax;
        String elemento;
        ArrayList<String> cellshelf = new ArrayList<String>();
        try
        {
            if (!spaceInspector.locationExists("bambook.Shelves", new Name(shelf)))
            {
                if (spaceInspector.locationExists("bambook.Rooms", new Name(room)))
                {
                    if (controlEdges(edges))
                    {
                        spaceManager.addLocation("bambook.Shelves", new Name(shelf));
                        elemento = edges.get(0);
                        x = (int) (Double.parseDouble(elemento.split(",")[0]) * 10);
                        y = (int) (Double.parseDouble(elemento.split(",")[1]) * 10);
                        elemento = edges.get(1);
                        x_p = (int) (Double.parseDouble(elemento.split(",")[0]) * 10);
                        y_p = (int) (Double.parseDouble(elemento.split(",")[1]) * 10);
                        elemento = edges.get(2);
                        x_i = (int) (Double.parseDouble(elemento.split(",")[0]) * 10);
                        y_i = (int) (Double.parseDouble(elemento.split(",")[1]) * 10);
                        if (x_i < x)
                        {
                            edge_xmin = x_i;
                            edge_xmax = x;
                        }
                        else
                        {
                            edge_xmin = x;
                            edge_xmax = x_i;
                        }
                        if (x_p < edge_xmin)
                            edge_xmin = x_p;
                        if (x_p > edge_xmax)
                            edge_xmax = x_p;

                        if (y_i < y)
                        {
                            edge_ymin = y_i;
                            edge_ymax = y;
                        }
                        else
                        {
                            edge_ymin = y;
                            edge_ymax = y_i;
                        }
                        if (y_p < edge_ymin)
                            edge_ymin = y_p;
                        if (y_p > edge_ymax)
                            edge_ymax = y_p;
                        for (int i = edge_xmin; i <= edge_xmax; i++)
                        {
                            for (int j = edge_ymin; j <= edge_ymax; j++)
                            {
                                cellshelf.add(i + "," + j);
                            }
                        }
                        EnumerativeContext this_cellshelf = new EnumerativeContext(
                                "bambook.Grid." + room, cellshelf);
                        EnumerativeContext this_room = new EnumerativeContext("bambook.Rooms", room);
                        EnumerativeContext this_shelf = new EnumerativeContext("bambook.Shelves", shelf);
                        mappingsManager.map(this_shelf, this_cellshelf);
                        mappingsManager.map(this_room, this_shelf);
                    }
                }
                else
                    System.out.println("La room chiamata " + room + " non esiste");
            }
            else
                System.out.println("Lo shelf chiamato " + shelf + " esiste gi?");
        } catch (SISServiceException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    //crea un beacon all'interno di una cella di una stanza
    public void createBeacon(String macadress, String room, double x, double y)
    {
        int x_i, y_i;
        x = x * 10;
        y = y * 10;
        x_i = (int) x;
        y_i = (int) y;
        EnumerativeContext this_beac, this_room, this_roomgrid;

        try
        {
            if (!spaceInspector.locationExists("bambook.Beacons", new Name(macadress)))
            {
                if (spaceInspector.locationExists("bambook.Rooms", new Name(room)))
                {
                    Name beacon = new Name(macadress);
                    spaceManager.addLocation("bambook.Beacons", beacon);
                    this_beac = new EnumerativeContext("bambook.Beacons", macadress);
                    this_room = new EnumerativeContext("bambook.Rooms", room);
                    this_roomgrid = new EnumerativeContext("bambook.Grid." + room, x_i + "," + y_i);
                    mappingsManager.map(this_room, this_beac);
                    mappingsManager.map(this_beac, this_roomgrid);
                }
                else
                    System.out.println("La room chiamata " + room + " non esiste.");
            }
            else
                System.out.println("Il Beacon con MAC ADRESS: " + macadress +
                                   " ? gi? presente nella lista di beacon");
        } catch (SISServiceException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    //crea un nodo all'interno del grafo e lo collega al parent (senza mapping)
    public void createNode(String library, String parent, String node)
    {
        try
        {
            if (!spaceInspector.locationExists("bambook." + library + ".graph", new Node(node)))
            {
                if (spaceInspector.locationExists(
                        "bambook." + library + ".graph", new Node(parent)))
                {
                    OrientedWeightedGraphParameters params = (OrientedWeightedGraphParameters) spaceInspector.getSpaceInformation(
                            "bambook." + library + ".graph").getParameters();
                    params.getNodeList().add(node);
                    params.getEdgeList().add(new WeightedEdge(parent, node, 1));
                    ArrayList<MappingDescription> listMappings = (ArrayList<MappingDescription>) mappingsInspector.getMappings();

                    spaceManager.undefSpace("bambook." + library + ".graph");
                    spaceManager.defSpace("bambook." + library +
                                          ".graph", SpatialModels.ORIENTEDWEIGHTEDGRAPH, params);

                    //rifaccio i mapping
                    for (MappingDescription map : listMappings)
                    {
                        if (spaceInspector.locationExists("bambook." + library +
                                                          ".graph", new Node(map.getSourceContext().getLocationNames().get(0))))
                        {
                            EnumerativeContext this_node = map.getSourceContext();
                            EnumerativeContext this_shelf = map.getTargetContext();
                            mappingsManager.map(this_node, this_shelf);
                        }
                    }
                }

            }
        } catch (SISServiceException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    //crea un nodo all'interno del grafo e lo collega al parent (con mapping)
    public void createNode(String library, String parent, String node, String shelf)
    {
        try
        {
            if (!spaceInspector.locationExists("bambook." + library + ".graph", new Node(node)))
            {
                System.out.println("passa primo step");
                if (spaceInspector.locationExists(
                        "bambook." + library + ".graph", new Node(parent)))
                {
                    System.out.println("passa secondo step");
                    if (spaceInspector.locationExists("bambook.Shelves", new Name(shelf)))
                    {
                        System.out.println("passa terzo step");
                        OrientedWeightedGraphParameters params = (OrientedWeightedGraphParameters) spaceInspector.getSpaceInformation(
                                "bambook." + library + ".graph").getParameters();
                        params.getNodeList().add(node);
                        params.getEdgeList().add(new WeightedEdge(parent, node, 1));
                        ArrayList<MappingDescription> listMappings = (ArrayList<MappingDescription>) mappingsInspector.getMappings();

                        spaceManager.undefSpace("bambook." + library + ".graph");
                        spaceManager.defSpace("bambook." + library +
                                              ".graph", SpatialModels.ORIENTEDWEIGHTEDGRAPH, params);

                        //rifaccio i mapping
                        for (MappingDescription map : listMappings)
                        {
                            if (spaceInspector.locationExists("bambook." + library +
                                                              ".graph", new Node(map.getSourceContext().getLocationNames().get(0))))
                            {
                                EnumerativeContext this_node = map.getSourceContext();
                                EnumerativeContext this_shelf = map.getTargetContext();
                                mappingsManager.map(this_node, this_shelf);
                            }
                        }

                        this.mapNode(library, node, shelf);
                    }
                }

            }
        } catch (SISServiceException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    //crea un arco all'interno del grafo
    public void createEdge(String library, String from, String to)
    {
        try
        {
            if (spaceInspector.locationExists("bambook." + library + ".graph", new Node(from)) &&
                spaceInspector.locationExists("bambook." + library + ".graph", new Node(from)))
            {
                if (!this.getListEdges(library).contains(from + " --> " + to))
                {
                    OrientedWeightedGraphParameters params = (OrientedWeightedGraphParameters) spaceInspector.getSpaceInformation(
                            "bambook." + library + ".graph").getParameters();
                    params.getEdgeList().add(new WeightedEdge(from, to, 1));
                    ArrayList<MappingDescription> listMappings = (ArrayList<MappingDescription>) mappingsInspector.getMappings();

                    spaceManager.undefSpace("bambook." + library + ".graph");
                    spaceManager.defSpace("bambook." + library +
                                          ".graph", SpatialModels.ORIENTEDWEIGHTEDGRAPH, params);

                    //rifaccio i mapping
                    for (MappingDescription map : listMappings)
                    {
                        if (spaceInspector.locationExists("bambook." + library +
                                                          ".graph", new Node(map.getSourceContext().getLocationNames().get(0))))
                        {
                            EnumerativeContext this_node = map.getSourceContext();
                            EnumerativeContext this_shelf = map.getTargetContext();
                            mappingsManager.map(this_node, this_shelf);
                        }
                    }
                }
            }
        } catch (SISServiceException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    //restituisce la lista di celle che rappresenta una stanza
    public ArrayList<String> getRoom(String room)
    {
        try
        {
            EnumerativeContext ctxs = mappingsInspector.getForwardMapped("bambook.Rooms", room,
                    "bambook.Grid." + room);
            if (ctxs == null)
                return new ArrayList<String>();
            return (ArrayList<String>) ctxs.getLocationNames();
        } catch (SISServiceException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return new ArrayList<String>();
    }

    //restituisce la lista di celle che rappresenta uno shelf
    public ArrayList<String> getShelf(String shelf)
    {
        EnumerativeContext cell, room;
        ArrayList<String> shelfcell = new ArrayList<String>();
        try
        {
            if (spaceInspector.locationExists("bambook.Shelves", new Name(shelf)))
            {
                room = mappingsInspector.getReverseMapped("bambook.Shelves", shelf, "bambook.Rooms");
                cell = mappingsInspector.getForwardMapped("bambook.Shelves", shelf,
                        "bambook.Grid." + room.getLocationNames().get(0));
               /*
                for(int i = 0; i < cell.getLocationNames().size(); i++)
                {
                    shelfcell.add(cell.getLocationNames().get(i));
                }
                */
                return (ArrayList<String>) cell.getLocationNames();
            }
        } catch (SISServiceException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return shelfcell;
    }

    //restituisce la cella della stanza dove è posizionato il beacon
    public String getBeac(String beac)
    {
        EnumerativeContext cell, room;
        String beaconcell = "";
        try
        {
            if (spaceInspector.locationExists("bambook.Beacons", new Name(beac)))
            {
                room = mappingsInspector.getReverseMapped("bambook.Beacons", beac, "bambook.Rooms");
                cell = mappingsInspector.getForwardMapped("bambook.Beacons", beac,
                        "bambook.Grid." + room.getLocationNames().get(0));
                beaconcell = cell.getLocationNames().get(0);

            }
        } catch (SISServiceException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return beaconcell;
    }

    // CODICE AGGIUNTO PER LOCALIZZAZIONE
    // Ottengo la stanza del beacon
    public ArrayList<String> getRoomFromBeacon(String macbeac)
    {
        EnumerativeContext room;
        try
        {
            if (spaceInspector.locationExists("bambook.Beacons", new Name(macbeac)))
            {
                room = mappingsInspector.getReverseMapped("bambook.Beacons", macbeac, "bambook.Rooms");
                return (ArrayList<String>) room.getLocationNames();
            }
        } catch (SISServiceException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return new ArrayList<String>();
    }

    // METODO AGGIUNTO PER LOCALIZZAZIONE
    // Ottengo i valori XY direttamente da SIS.
    public double[] getBeacXY(String macaddr)
    {
        double[] XY = new double[2];
        XY[0] = Double.parseDouble(this.getBeac(macaddr).split(",")[0]); // Previous /10
        XY[1] = Double.parseDouble(this.getBeac(macaddr).split(",")[1]); // Previous /10
        return XY;
    }

    // METODO AGGIUNTO PER LOCALIZZAZIONE
    // Creo e pre-popolo le strutture dati della stanza
    public Room getObjectRoom(String nameRoom, ArrayList<String> locationNames)
    {
        Room tmpRoom = new Room(nameRoom);
        tmpRoom.setXYsFromCells(this.getRoom(tmpRoom.getName()));
        tmpRoom.setParentLibrary(this.getLibraryFromRoom(tmpRoom.getName()));

        ArrayList<String> _tmpShelfbook = locationNames;
        ArrayList<String> _tmpListshelves = this.getListShelves(tmpRoom.getName());
        ArrayList<String> _tmpMacBeacons = this.getListBeacs(nameRoom);
        ArrayList<Shelf> _shelvesbook = new ArrayList<Shelf>();
        ArrayList<Shelf> _shelves = new ArrayList<Shelf>();
        ArrayList<Shelf> _gardens = new ArrayList<Shelf>();
        ArrayList<String> _namesShelvesbook = new ArrayList<String>();
        ArrayList<Estimote> _tmpBeacons = new ArrayList<Estimote>();

        for (String shelf : _tmpShelfbook)
        {
            _namesShelvesbook.add(shelf);
            Shelf s = new Shelf(shelf);
            s.setXYsFromCells(this.getShelf(s.getName()));
            _shelvesbook.add(s);
            _tmpListshelves.remove(s);
        }

        this.getListShelves(tmpRoom.getName()).remove(_shelvesbook);

        //per ogni scaffale, lo creo e lo aggiungo alla lista degli scaffali di questa stanza
        for (String shelf : _tmpListshelves)
        {
            Shelf s = new Shelf(shelf);
            s.setXYsFromCells(this.getShelf(s.getName()));
            if (shelf.split("_")[0].compareTo("Garden") == 0)
                _gardens.add(s);
            else
                _shelves.add(s);
        }

        for (String mac : _tmpMacBeacons)
        {
            Estimote beacon = new Estimote(mac, this.getBeacXY(mac));
            _tmpBeacons.add(beacon);
        }

        tmpRoom.setGardens(_gardens);
        tmpRoom.setShelves(_shelves);
        tmpRoom.setShelvesbook(_shelvesbook);
        tmpRoom.setNameShelvesbook(_namesShelvesbook);
        tmpRoom.setMacbeacons(_tmpMacBeacons);
        tmpRoom.setBeacons(_tmpBeacons);
        return tmpRoom;
    }

    //restituisce la lista di celle che rappresenta un nodo
    public ArrayList<String> getNode(String library, String node)
    {
        EnumerativeContext cell, room;
        ArrayList<String> nodecell = new ArrayList<String>();
        try
        {
            if (spaceInspector.locationExists("bambook." + library + ".graph", new Node(node)) &&
                (!mappingsInspector.getMapped("bambook." + library + ".graph", node).isEmpty()))
            {
                for (String shelf : mappingsInspector.getForwardMapped("bambook." + library + ".graph", node, "bambook.Shelves").getLocationNames())
                {
                    room = mappingsInspector.getReverseMapped("bambook.Shelves", shelf, "bambook.Rooms");
                    cell = mappingsInspector.getForwardMapped("bambook.Shelves", shelf, "bambook.Grid." + room.getLocationNames().get(0));

                    for (int i = 0; i < cell.getLocationNames().size(); i++)
                        nodecell.add(cell.getLocationNames().get(i));
                }
                return nodecell;
            }
        }
        catch (SISServiceException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return nodecell;
    }

    //restituisce la lista di tutte le stanze
    public ArrayList<String> getListRooms()
    {
        NameSpaceParameters params;
        try
        {
            if (spaceInspector.getSpaceNames().contains("bambook.Rooms"))
            {
                SpaceInformation info = spaceInspector.getSpaceInformation("bambook.Rooms");
                params = (NameSpaceParameters) info.getParameters();
                return (ArrayList<String>) params.getNamesLocators();
            }
        }
        catch (SISServiceException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return new ArrayList<String>();
    }

    //restituisce la lista di tutti gli shelf in una stanza
    public ArrayList<String> getListShelves(String room)
    {
        try
        {
            EnumerativeContext ctxs = mappingsInspector.getForwardMapped("bambook.Rooms", room, "bambook.Shelves");
            if (ctxs == null)
                return new ArrayList<String>();
            return (ArrayList<String>) ctxs.getLocationNames();
        }
        catch (SISServiceException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return new ArrayList<String>();
    }

    //restituisce la lista di tutti i beacons in una stanza
    public ArrayList<String> getListBeacs(String room)
    {
        try
        {
            EnumerativeContext ctxs = mappingsInspector.getForwardMapped("bambook.Rooms", room, "bambook.Beacons");
            if (ctxs == null)
                return new ArrayList<String>();
            return (ArrayList<String>) ctxs.getLocationNames();
        }
        catch (SISServiceException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return new ArrayList<String>();
    }

    //restituisce la lista di tutti i nodi del grafo
    public ArrayList<String> getListNodes(String library)
    {
        OrientedWeightedGraphParameters params;
        try
        {
            if (spaceInspector.getSpaceNames().contains("bambook." + library + ".graph"))
            {
                SpaceInformation info = spaceInspector.getSpaceInformation(
                        "bambook." + library + ".graph");
                params = (OrientedWeightedGraphParameters) info.getParameters();
                return (ArrayList<String>) params.getNodeList();
            }

        }
        catch (SISServiceException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return new ArrayList<String>();
    }

    //restituisce la lista di tutti gli archi del grafo
    public ArrayList<String> getListEdges(String library)
    {
        OrientedWeightedGraphParameters params;
        try
        {
            if (spaceInspector.getSpaceNames().contains("bambook." + library + ".graph"))
            {
                SpaceInformation info = spaceInspector.getSpaceInformation("bambook." + library + ".graph");
                params = (OrientedWeightedGraphParameters) info.getParameters();
                ArrayList<WeightedEdge> edges = (ArrayList<WeightedEdge>) params.getEdgeList();
                ArrayList<String> edgeList = new ArrayList<String>();

                for (WeightedEdge edge : edges)
                    edgeList.add(edge.getFromNodeName() + " --> " + edge.getToNodeName());

                return edgeList;
            }
        }
        catch (SISServiceException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return new ArrayList<String>();
    }

    //restituisce la lista di stanze associate a una library
    public ArrayList<String> getLibraryRooms(String library)
    {
        NameSpaceParameters params;
        try
        {
            if (spaceInspector.getSpaceNames().contains("bambook.Libraries"))
            {
                EnumerativeContext ctxs = mappingsInspector.getReverseMapped("bambook.Libraries", library, "bambook.Rooms");
                return (ArrayList<String>) ctxs.getLocationNames();
            }
        }
        catch (SISServiceException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return new ArrayList<String>();
    }

    //METODO AGGIUNTO PER LOCALIZZAZIONE
    //restituisce la lista di librerie associate ad una stanza
    public ArrayList<String> getLibraryFromRoom(String room)
    {
        NameSpaceParameters params;
        try
        {
            EnumerativeContext ctxs = mappingsInspector.getForwardMapped("bambook.Rooms", room, "bambook.Libraries");
            if (ctxs == null) return new ArrayList<String>();
            else return (ArrayList<String>) ctxs.getLocationNames();
        }
        catch (SISServiceException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return new ArrayList<String>();
    }

    //restituisce la lista di librarie
    public ArrayList<String> getLibraries()
    {
        NameSpaceParameters params;

        try
        {
            SpaceInformation info = spaceInspector.getSpaceInformation("bambook.Libraries");
            params = (NameSpaceParameters) info.getParameters();
            return (ArrayList<String>) params.getNamesLocators();
        }
        catch (SISServiceException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return new ArrayList<String>();
    }

    //elimina una libreria
    public void deleteLibrary(final String library)
    {
        ArrayList<String> rooms = getLibraryRooms(library);
        for (String room : rooms)
            deleteRoom(room);
    }

    //elimina una stanza
    public void deleteRoom(final String name)
    {
        ArrayList<String> cell_list = getRoom(name), beacons_list = this.getListBeacs(name), shelves_list = this.getListShelves(name);
        try
        {
            if (spaceInspector.locationExists("bambook.Rooms", new Name(name)))
            {
                for (String beac : beacons_list)
                {
                    deleteBeacon(beac);
                }
                for (String shelf : shelves_list)
                {
                    deleteShelf(shelf);
                }
                EnumerativeContext ctxs = mappingsInspector.getForwardMapped("bambook.Rooms", name, "bambook.Libraries");
                mappingsManager.unmap(new EnumerativeContext("bambook.Rooms", name), ctxs);
                mappingsManager.unmap(new EnumerativeContext("bambook.Rooms", name), new EnumerativeContext("bambook.Grid." + name, cell_list));
                spaceManager.removeLocations("bambook.Rooms", new ArrayList<Location>()
                {{
                    add(new Name(name));
                }});
                spaceManager.undefSpace("bambook.Grid." + name);
            }
            else
                System.out.println("La room chiamata " + name + " non esiste.");
        }
        catch (SISServiceException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    //elimina uno shelf
    public void deleteShelf(final String shelf)
    {
        try
        {
            if (spaceInspector.locationExists("bambook.Shelves", new Name(shelf)))
            {
                ArrayList<String> cells = getShelf(shelf);
                EnumerativeContext ctxs = mappingsInspector.getReverseMapped("bambook.Shelves", shelf, "bambook.Rooms");
                mappingsManager.unmap(new EnumerativeContext("bambook.Rooms", ctxs.getLocationNames().get(0)), new EnumerativeContext("bambook.Shelves", shelf));
                mappingsManager.unmap(new EnumerativeContext("bambook.Shelves", shelf), new EnumerativeContext("bambook.Grid." + ctxs.getLocationNames().get(0), cells));
                EnumerativeContext ctxs2 = mappingsInspector.getForwardMapped("bambook.Rooms", ctxs.getLocationNames().get(0), "bambook.Libraries");
                EnumerativeContext ctxs1 = mappingsInspector.getReverseMapped("bambook.Shelves", shelf, "bambook." + ctxs2.getLocationNames().get(0) + ".graph");
                if (ctxs1.getLocationNames() != null)
                {
                    for (String node : ctxs1.getLocationNames())
                        mappingsManager.unmap(new EnumerativeContext("bambook." + ctxs2.getLocationNames().get(0) + ".graph", node), new EnumerativeContext("bambook.Shelves", shelf));
                }
                spaceManager.removeLocations("bambook.Shelves", new ArrayList<Location>()
                {{
                    add(new Name(shelf));
                }});
            }
            else System.out.println("Lo shelf : " + shelf + " non esiste.");
        }
        catch (SISServiceException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    //elimino lo shelf non mappato
    public void deleteUnMappedShelf(final String shelf)
    {
        try
        {
            if (spaceInspector.locationExists("bambook.Shelves", new Name(shelf)))
            {
                ArrayList<String> cells = getShelf(shelf);
                EnumerativeContext ctxs = mappingsInspector.getReverseMapped("bambook.Shelves", shelf, "bambook.Rooms");
                mappingsManager.unmap(
                        new EnumerativeContext("bambook.Rooms", ctxs.getLocationNames().get(0)),
                        new EnumerativeContext("bambook.Shelves", shelf));
                mappingsManager.unmap(
                        new EnumerativeContext("bambook.Shelves", shelf),
                        new EnumerativeContext(
                                "bambook.Grid." + ctxs.getLocationNames().get(0), cells));

                spaceManager.removeLocations("bambook.Shelves", new ArrayList<Location>()
                {{
                        add(new Name(shelf));
                    }});
            }
            else
                System.out.println("Lo shelf : " + shelf + " non esiste.");
        } catch (SISServiceException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    //elimina un beacon
    public void deleteBeacon(final String macadress)
    {
        try
        {
            if (spaceInspector.locationExists("bambook.Beacons", new Name(macadress)))
            {
                String cell = getBeac(macadress);
                EnumerativeContext ctxs = mappingsInspector.getReverseMapped("bambook.Beacons", macadress, "bambook.Rooms");
                mappingsManager.unmap(
                        new EnumerativeContext("bambook.Rooms", ctxs.getLocationNames().get(0)),
                        new EnumerativeContext("bambook.Beacons", macadress));
                mappingsManager.unmap(
                        new EnumerativeContext("bambook.Beacons", macadress),
                        new EnumerativeContext(
                                "bambook.Grid." + ctxs.getLocationNames().get(0), cell));


                spaceManager.removeLocations("bambook.Beacons", new ArrayList<Location>()
                {{
                        add(new Name(macadress));
                    }});
            }
            else
                System.out.println("Il beacon con MAC ADRESS: " + macadress + " non esiste.");
        } catch (SISServiceException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    //elimina un nodo
    public void deleteNode(String library, final String node)
    {
        try
        {
            SpaceInformation info = spaceInspector.getSpaceInformation(
                    "bambook." + library + ".graph");
            OrientedWeightedGraphParameters params = (OrientedWeightedGraphParameters) info.getParameters();
            if (this.getListNodes(library).contains(node))
            {
                //cancello i figli
                ArrayList<WeightedEdge> edges = (ArrayList<WeightedEdge>) params.getEdgeList();
                for (WeightedEdge edge : edges)
                {
                    if (edge.getFromNodeName().equals(node))
                    {
                        this.deleteNode(library, edge.getToNodeName());
                    }
                }

                //smappo il nodo
                this.unmapNode(library, node);

                //rimuovo gli archi entranti
                this.deleteEdgesToNode(library, node);

                params = (OrientedWeightedGraphParameters) info.getParameters(); //perche altrimenti non vede gli aggiornamenti effettuati da deleteEdgesToNode
                ArrayList<MappingDescription> listMappings = (ArrayList<MappingDescription>) mappingsInspector.getMappings();

                //rimuovo il nodo
                int i = 0;
                int nodeIndex = -1;
                for (String elemNode : params.getNodeList())
                {
                    if (elemNode.equals(node))
                    {
                        nodeIndex = i;
                    }
                    i++;
                }
                if (nodeIndex > -1)
                    params.getNodeList().remove(nodeIndex);


                spaceManager.undefSpace("bambook." + library + ".Graph");
                spaceManager.defSpace("bambook." + library +
                                      ".Graph", SpatialModels.ORIENTEDWEIGHTEDGRAPH, params);

                //rifaccio i mapping
                for (MappingDescription map : listMappings)
                {
                    if (spaceInspector.locationExists("bambook." + library +
                                                      ".graph", new Node(map.getSourceContext().getLocationNames().get(0))))
                    {
                        EnumerativeContext this_node = map.getSourceContext();
                        EnumerativeContext this_shelf = map.getTargetContext();
                        mappingsManager.map(this_node, this_shelf);
                    }
                }
            }
            else
                System.out.println("il nodo chiamato " + node + " non esiste.");
        } catch (SISServiceException e2)
        {
            // TODO Auto-generated catch block
            e2.printStackTrace();
        }
    }

    //elimina un arco
    public void deleteEdge(String library, final String from, final String to)
    {
        try
        {

            if (this.getListEdges(library).contains(from + " --> " + to))
            {
                SpaceInformation info = spaceInspector.getSpaceInformation(
                        "bambook." + library + ".graph");
                OrientedWeightedGraphParameters params = (OrientedWeightedGraphParameters) info.getParameters();

                int i = 0;
                int edgeIndex = -1;
                for (WeightedEdge edge : params.getEdgeList())
                {
                    if (edge.getFromNodeName().equals(from) && edge.getToNodeName().equals(to))
                    {
                        edgeIndex = i;
                    }
                    i++;
                }
                if (edgeIndex > -1)
                    params.getEdgeList().remove(edgeIndex);

                ArrayList<MappingDescription> listMappings = (ArrayList<MappingDescription>) mappingsInspector.getMappings();

                spaceManager.undefSpace("bambook." + library + ".graph");
                spaceManager.defSpace("bambook." + library +
                                      ".graph", SpatialModels.ORIENTEDWEIGHTEDGRAPH, params);

                for (MappingDescription map : listMappings)
                {
                    if (params.getNodeList().contains(map.getSourceContext().getLocationNames().get(0)))
                    {
                        EnumerativeContext this_node = map.getSourceContext();
                        EnumerativeContext this_shelf = map.getTargetContext();
                        mappingsManager.map(this_node, this_shelf);
                    }
                }

            }
            else
                System.out.println("L'arco da " + from + " a " + to + " non esiste.");
        } catch (SISServiceException e2)
        {
            // TODO Auto-generated catch block
            e2.printStackTrace();
        }
    }

    //elimina gli archi entranti in un nodo
    private void deleteEdgesToNode(String library, final String node)
    {
        try
        {
            if (spaceInspector.locationExists("bambook." + library + ".graph", new Node(node)))
            {
                SpaceInformation info = spaceInspector.getSpaceInformation(
                        "bambook." + library + ".graph");
                OrientedWeightedGraphParameters params = (OrientedWeightedGraphParameters) info.getParameters();
                ArrayList<WeightedEdge> edges = (ArrayList<WeightedEdge>) params.getEdgeList();
                for (final WeightedEdge edge : edges)
                {
                    if (edge.getToNodeName().equals(node))
                    {
                        this.deleteEdge(library, edge.getFromNodeName(), node);
                    }
                }
            }
            else
                System.out.println("il nodo chiamato " + node + " non esiste.");
        } catch (SISServiceException e2)
        {
            // TODO Auto-generated catch block
            e2.printStackTrace();
        }
    }

    //AUSILIARI
    //richiamata in createShelf per verificare che la lista di spigoli rappresenti uno shelf quadrato o rettangolare
    private boolean controlEdges(ArrayList<String> edges)
    {
        if (edges.size() != 4)
            return false;
        int x, y, x_p, y_p;
        String elemento;
        elemento = edges.get(0);
        x_p = (int) (Double.parseDouble(elemento.split(",")[0]) * 10);
        y_p = (int) (Double.parseDouble(elemento.split(",")[1]) * 10);
        for (int i = 1; i < edges.size(); i++)
        {
            elemento = edges.get(i);
            x = (int) (Double.parseDouble(elemento.split(",")[0]) * 10);
            y = (int) (Double.parseDouble(elemento.split(",")[1]) * 10);
            if (x != x_p && y != y_p)
                return false;
            x_p = x;
            y_p = y;
        }
        return true;
    }

    //mappa un nodo in uno shelf
    public void mapNode(String library, String node, String shelf)
    {
        try
        {
            if (spaceInspector.locationExists("bambook." + library + ".graph", new Node(node)))
            {
                if (spaceInspector.locationExists("bambook.Shelves", new Name(shelf)))
                {
                    EnumerativeContext this_node = new EnumerativeContext(
                            "bambook." + library + ".graph", node);
                    EnumerativeContext this_shelf = new EnumerativeContext("bambook.Shelves", shelf);
                    mappingsManager.map(this_node, this_shelf);
                }

            }
        } catch (SISServiceException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    //rimuove tutti i mapping di un nodo
    public void unmapNode(String library, String node)
    {
        try
        {
            if (spaceInspector.locationExists("bambook." + library + ".graph", new Node(node)) &&
                (!mappingsInspector.getMapped("bambook.graph", node).isEmpty()))
            {
                EnumerativeContext ctxs = mappingsInspector.getForwardMapped(
                        "bambook." + library + ".graph", node, "bambook.Shelves");
                mappingsManager.unmap(new EnumerativeContext("bambook." + library +
                                                             ".graph", node), new EnumerativeContext("bambook.Shelves", ctxs.getLocationNames().get(0)));
            }
        } catch (SISServiceException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}

