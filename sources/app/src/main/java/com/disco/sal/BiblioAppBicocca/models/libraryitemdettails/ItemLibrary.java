package com.disco.sal.biblioappbicocca.models.libraryitemdettails;

import java.util.ArrayList;

/**
 * Created by lorenzo on 17/09/15.
 */
public class ItemLibrary {
    private String address;
    private String latitude;
    private String longitude;
    private ArrayList<String> mail;
    private ArrayList<String> openingHours;
    private ArrayList<String> site;
    private ArrayList<String> telephone;

    public ItemLibrary(){
        this.mail = new ArrayList<>();
        this.openingHours = new ArrayList<>();
        this.site = new ArrayList<>();
        this.telephone = new ArrayList<>();
    }

    public ItemLibrary(String address, String latitude, String longitude, ArrayList<String> mail, ArrayList<String> openingHours, ArrayList<String> site, ArrayList<String> telephone) {
        /*this.mail = new ArrayList<>();
        this.openingHours = new ArrayList<>();
        this.site = new ArrayList<>();
        this.telephone = new ArrayList<>();*/

        this.address = address;
        this.latitude = latitude;
        this.longitude = longitude;
        this.mail = mail;
        this.openingHours = openingHours;
        this.site = site;
        this.telephone = telephone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public ArrayList<String> getMail() {
        return mail;
    }

    public void setMail(ArrayList<String> mail) {
        this.mail = mail;
    }

    public ArrayList<String> getOpeningHours() {
        return openingHours;
    }

    public void setOpeningHours(ArrayList<String> openingHours) {
        this.openingHours = openingHours;
    }

    public ArrayList<String> getSite() {
        return site;
    }

    public void setSite(ArrayList<String> site) {
        this.site = site;
    }

    public ArrayList<String> getTelephone() {
        return telephone;
    }

    public void setTelephone(ArrayList<String> telephone) {
        this.telephone = telephone;
    }
}
