package com.disco.sal.biblioappbicocca.utility;

import android.app.Activity;
import android.util.DisplayMetrics;

/**
 * Created by lorenzo on 18/09/15.
 */
public class DisplayDimension {

    private DisplayMetrics displayMetrics;

    public DisplayDimension(Activity activity) {
        displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
    }

    public int getDeviceWidth() {
        return displayMetrics.widthPixels;
    }

    public int getDeviceHeight() {
        return displayMetrics.heightPixels;
    }
}