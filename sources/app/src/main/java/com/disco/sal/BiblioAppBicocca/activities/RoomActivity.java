package com.disco.sal.biblioappbicocca.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.disco.sal.biblioappbicocca.R;
import com.disco.sal.biblioappbicocca.adapters.AdapterSIS;
import com.disco.sal.biblioappbicocca.classes.Room;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.GridLabelRenderer;

import java.util.ArrayList;

public class RoomActivity extends ActionBarActivity
{
    ActionBarActivity context;
    Intent intent = null;
    Room currentIntentRoom = null;
    AdapterSIS adapter = null;
    String sublibrary;
    Toolbar toolbar;
    GraphView graph;
    TextView title, room, shelves;
    ListView listsis;
    ScrollView scrollView;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        this.context = this;
        this.intent = getIntent();
        Bundle _bundle = this.intent.getExtras();
        this.currentIntentRoom = (Room) _bundle.getSerializable("searchedroomobj");

        if(this.currentIntentRoom != null)
        {
            ArrayList<String> parentLib = this.currentIntentRoom.getParentLibrary();
            if (parentLib.size() > 0) this.sublibrary = parentLib.get(0);
            else this.sublibrary = "Unknown Library";
        }

        setContentView(R.layout.activity_room);
        scrollView = (ScrollView) findViewById(R.id.scrollViewSIS);
        scrollView.smoothScrollTo(0, 0);

        listsis = (ListView) findViewById(R.id.listViewSIS);
        graph = (GraphView) findViewById(R.id.SISview);
        graph.removeAllSeries();

        toolbar = (Toolbar) findViewById(R.id.toolbargeneric);
        toolbar.setTitle("Locazione");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        context.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        context.getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                onBackPressed();
            }
        });
        title = (TextView) findViewById(R.id.textViewSisTitle);
        title.setText(sublibrary);
        room = (TextView) findViewById(R.id.textViewRoom);
        shelves = (TextView) findViewById(R.id.textNumbershelves);
        shelves.setText(String.valueOf(currentIntentRoom.getSizeShelves()));
        graph.removeAllSeries();

        // MODIFICATO PER RIMOZIONE CHECK MAPPE
        // Solo adesso è possibile utilizzare liberamente l'app
        // con mappature di biblioteche differenti su SIS (solo se presenti su db)!!
        //if (sublibrary.contains("Bicocca_Sede_Centrale"))
        //{
            room.setText(this.currentIntentRoom.getName().replace("_", " "));
            drawGraph();
            this.currentIntentRoom.drawRoom(graph);

            adapter = new AdapterSIS(this, currentIntentRoom);
            listsis = (ListView) findViewById(R.id.listViewSIS);
            listsis.setAdapter(adapter);
            adapter.replaceWith(this.currentIntentRoom.getShelves());

            listsis.setOnItemClickListener(new AdapterView.OnItemClickListener()
            {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id)
                {
                    adapter.changeStatus(position, graph);
                    adapter.replaceWith(currentIntentRoom.getShelves());
                }
            });

            listsis.setOnTouchListener(new View.OnTouchListener()
        {
            // Setting on Touch Listener for handling the touch inside ScrollView
            @Override
            public boolean onTouch(View v, MotionEvent event)
            {
                // Disallow the touch request for parent scroll on touch of child view
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    return false;
                }
            });
        //}
    }

    private void drawGraph()
    {
        graph.getGridLabelRenderer().setGridStyle(GridLabelRenderer.GridStyle.NONE);
        graph.getGridLabelRenderer().setHorizontalLabelsVisible(false);
        graph.getGridLabelRenderer().setVerticalLabelsVisible(false);
        graph.getViewport().setXAxisBoundsManual(true);
        graph.getViewport().setYAxisBoundsManual(true);

        //calcolo dimensione grafico in base alla dimensione della stanza selezionata (il grafico deve essere quadrato per le proporzioni grafiche)
        double dimGraph = 0;
        for (double[] xy : this.currentIntentRoom.getXYs())
        {
            if (xy[0] > dimGraph)
            {
                dimGraph = xy[0];
            }
            if (xy[1] > dimGraph)
            {
                dimGraph = xy[1];
            }
        }
        graph.getViewport().setMaxX(dimGraph);
        graph.getViewport().setMaxY(dimGraph);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu)
    {
        return false;
    }
}
