package com.disco.sal.biblioappbicocca.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Spinner;

import com.disco.sal.biblioappbicocca.R;

import java.util.ArrayList;
import java.util.Calendar;

public class AdvanceSearchActivity extends ActionBarActivity
{
    private EditText title;
    private EditText author;
    private EditText publisher;
    private EditText place;
    private EditText code;
    private Spinner material;
    private Spinner spinYear;
    private Spinner language;
    private Toolbar toolbar;
    private Button btnSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_advance_search);
        toolbar = (Toolbar) findViewById(R.id.toolbargeneric);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        btnSearch = (Button) findViewById(R.id.btnSearch);

        final ScrollView scrollViewFileredSearch = (ScrollView) findViewById(R.id.scrollViewFileredSearch);
        material = (Spinner) findViewById(R.id.spinnerType);
        spinYear = (Spinner) findViewById(R.id.spinnerYear);
        language = (Spinner) findViewById(R.id.spinnerLanguage);
        title = (EditText) findViewById(R.id.editTextTitle);
        title.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {

            }

            @Override
            public void afterTextChanged(Editable s)
            {
                if (s.toString().contains("\n"))
                {
                    title.setText(title.getText().toString().replace("\n", ""));
                    author.requestFocus();
                }

            }
        });
        author = (EditText) findViewById(R.id.editTextAutore);
        author.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {

            }

            @Override
            public void afterTextChanged(Editable s)
            {
                if (s.toString().contains("\n"))
                {
                    author.setText(author.getText().toString().replace("\n", ""));
                    publisher.requestFocus();

                }

            }
        });
        publisher = (EditText) findViewById(R.id.editTextEditore);
        publisher.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {

            }

            @Override
            public void afterTextChanged(Editable s)
            {
                if (s.toString().contains("\n"))
                {
                    publisher.setText(publisher.getText().toString().replace("\n", ""));
                    place.requestFocus();

                }

            }
        });
        place = (EditText) findViewById(R.id.editTextPlace);
        place.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {

            }

            @Override
            public void afterTextChanged(Editable s)
            {
                if (s.toString().contains("\n"))
                {
                    place.setText(place.getText().toString().replace("\n", ""));
                    code.requestFocus();

                }

            }
        });
        code = (EditText) findViewById(R.id.editTextCode);
        code.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {

            }

            @Override
            public void afterTextChanged(Editable s)
            {
                if (s.toString().contains("\n"))
                {
                    code.setText(code.getText().toString().replace("\n", ""));
                    InputMethodManager imm = (InputMethodManager) getSystemService(
                            Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(code.getWindowToken(), 0);
                    scrollViewFileredSearch.post(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            scrollViewFileredSearch.fullScroll(ScrollView.FOCUS_DOWN);
                        }
                    });

                }

            }
        });

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getApplicationContext(), R.array.type_spinner_values, R.layout.spinner_item);
        adapter.setDropDownViewResource(R.layout.spinner_item);
        material.setAdapter(adapter);


        ArrayAdapter<CharSequence> lingAdap = ArrayAdapter.createFromResource(getApplicationContext(), R.array.language_spinner_values, R.layout.spinner_item);
        lingAdap.setDropDownViewResource(R.layout.spinner_item);
        language.setAdapter(lingAdap);


        ArrayList<String> year = new ArrayList<>();
        year.add(getString(R.string.year_spinner_all));
        Calendar calendar = Calendar.getInstance();
        int yearNow = calendar.get(Calendar.YEAR);
        for (int i = yearNow; i > 0; i--)
            year.add(String.valueOf(i));
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(getApplicationContext(), R.layout.spinner_item, year);
        dataAdapter.setDropDownViewResource(R.layout.spinner_item);
        spinYear.setAdapter(dataAdapter);
        btnSearch.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                MainActivity.titolo = title.getText().toString();
                MainActivity.autore = author.getText().toString();
                MainActivity.editore = publisher.getText().toString();
                MainActivity.luogo = place.getText().toString();
                MainActivity.codice = code.getText().toString();
                setMaterial(material.getSelectedItem().toString());
                MainActivity.anno = spinYear.getSelectedItem().toString();
                setLanguage(language.getSelectedItem().toString());
                MainActivity.documentsPreviews = null;
                MainActivity.offset = 1;
                MainActivity.query = "";
                MainActivity.serverDown = false;
                MainActivity.offsetBound = false;
                MainActivity.flagAdvancedSearch = true;
                finish();
            }
        });
        //hide keyboard
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case android.R.id.home:
                finish();

        }
        return super.onOptionsItemSelected(item);
    }

    private void setMaterial(String s)
    {

        if (s.equals(getResources().getStringArray(R.array.type_spinner_values)[0]))
        {
            MainActivity.materiale = "Tutti";
        }
        else if (s.equals(getResources().getStringArray(R.array.type_spinner_values)[1]))
        {
            MainActivity.materiale = "BK";
        }
        else if (s.equals(getResources().getStringArray(R.array.type_spinner_values)[2]))
        {
            MainActivity.materiale = "SE";
        }
        else if (s.equals(getResources().getStringArray(R.array.type_spinner_values)[3]))
        {
            MainActivity.materiale = "AQ";
        }
        else if (s.equals(getResources().getStringArray(R.array.type_spinner_values)[4]))
        {
            MainActivity.materiale = "CM";
        }
        else if (s.equals(getResources().getStringArray(R.array.type_spinner_values)[5]))
        {
            MainActivity.materiale = "NB";
        }
        else if (s.equals(getResources().getStringArray(R.array.type_spinner_values)[6]))
        {
            MainActivity.materiale = "VM";
        }
    }

    private void setLanguage(String s)
    {

        if (s.equals(getResources().getStringArray(R.array.language_spinner_values)[0]))
        {
            MainActivity.lingua = "Tutte";
        }
        else if (s.equals(getResources().getStringArray(R.array.language_spinner_values)[1]))
        {
            MainActivity.lingua = "ITA";
        }
        else if (s.equals(getResources().getStringArray(R.array.language_spinner_values)[2]))
        {
            MainActivity.lingua = "ENG";
        }
        else if (s.equals(getResources().getStringArray(R.array.language_spinner_values)[3]))
        {
            MainActivity.lingua = "FRE";
        }
        else if (s.equals(getResources().getStringArray(R.array.language_spinner_values)[4]))
        {
            MainActivity.lingua = "SPA";
        }
        else if (s.equals(getResources().getStringArray(R.array.language_spinner_values)[5]))
        {
            MainActivity.lingua = "GER";
        }
        else if (s.equals(getResources().getStringArray(R.array.language_spinner_values)[6]))
        {
            MainActivity.lingua = "RUS";
        }
        else if (s.equals(getResources().getStringArray(R.array.language_spinner_values)[7]))
        {
            MainActivity.lingua = "CHI";
        }
        else if (s.equals(getResources().getStringArray(R.array.language_spinner_values)[8]))
        {
            MainActivity.lingua = "JPN";
        }
    }
}
