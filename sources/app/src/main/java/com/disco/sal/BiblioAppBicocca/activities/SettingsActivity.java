package com.disco.sal.biblioappbicocca.activities;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.disco.sal.biblioappbicocca.R;


public class SettingsActivity extends PreferenceActivity
{
    // Preferences
    private SharedPreferences mPrefs = null;

    // Preference change listener
    private PreferenceChangeListener mPreferenceListener = null;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
        mPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        mPreferenceListener = new PreferenceChangeListener();
        mPrefs.registerOnSharedPreferenceChangeListener(mPreferenceListener);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState)
    {
        super.onPostCreate(savedInstanceState);

        LinearLayout root = (LinearLayout) findViewById(android.R.id.list).getParent().getParent().getParent();
        Toolbar bar = (Toolbar) LayoutInflater.from(this).inflate(R.layout.toolbar, root, false);
        bar.setTitle(getString(R.string.title_activity_setting));
        root.addView(bar, 0); // insert at top
        //chiudi l'activity se premi sulla freccia indietro nella toolbar
        bar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                finish();
            }
        });
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        mPrefs.unregisterOnSharedPreferenceChangeListener(mPreferenceListener);
    }


    // Handle preferences changes
    private class PreferenceChangeListener implements SharedPreferences.OnSharedPreferenceChangeListener
    {

        @Override
        public void onSharedPreferenceChanged(SharedPreferences prefs, String key)
        {
            if (key.equals(getString(R.string.key_paging)))
            {
                MainActivity.paging = prefs.getInt(getString(R.string.key_paging), 12);
            }
            if (key.equals(getString(R.string.key_cover)))
            {
                MainActivity.cover = prefs.getBoolean(getString(R.string.key_cover), true);
            }
            if (key.equals(getString(R.string.key_new_arrivals_argument)))
            {
                MainActivity.typeSearchArguments = prefs.getString(getString(R.string.key_new_arrivals_argument), getResources().getStringArray(R.array.new_arrivals_values)[0]);
            }
            if (key.equals(getString(R.string.key_new_arrivals_seat)))
            {
                MainActivity.typeSearchSeat = prefs.getString(getString(R.string.key_new_arrivals_seat), getResources().getStringArray(R.array.new_arrivals_values)[0]);
            }
            if (key.equals(getString(R.string.key_new_arrivals_disciplinary_area)))
            {
                MainActivity.typeSearchDisciplinary = prefs.getString(getString(R.string.key_new_arrivals_disciplinary_area), getResources().getStringArray(R.array.new_arrivals_values)[0]);
            }
        }
    }
}
