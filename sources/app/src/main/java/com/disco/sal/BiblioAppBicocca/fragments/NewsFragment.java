package com.disco.sal.biblioappbicocca.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.disco.sal.biblioappbicocca.R;
import com.disco.sal.biblioappbicocca.activities.MainActivity;
import com.disco.sal.biblioappbicocca.activities.NewsActivity;
import com.disco.sal.biblioappbicocca.adapters.AdapterNews;
import com.disco.sal.biblioappbicocca.classes.HttpRequest;


public class NewsFragment extends android.support.v4.app.Fragment
{

    private ListView listViewNews;
    View myInflatedView;

    public static NewsFragment newInstance()
    {
        NewsFragment fragment = new NewsFragment();
        return fragment;
    }

    public NewsFragment()
    {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        MainActivity.serverDown = false;
        myInflatedView = inflater.inflate(R.layout.fragment_news, container, false);
        View header = getActivity().getLayoutInflater().inflate(R.layout.list_news_header, null, false);
        listViewNews = (ListView) myInflatedView.findViewById(R.id.listViewNews);
        listViewNews.addHeaderView(header, null, false);
        listViewNews.setEmptyView(myInflatedView.findViewById(R.id.empty));
        if (MainActivity.news == null)
        {
            LoadNews loadNews = new LoadNews(getActivity());
            loadNews.execute();
        }
        else
        {
            AdapterNews adapterNews = new AdapterNews(getActivity(), MainActivity.news);
            listViewNews.setAdapter(adapterNews);
            listViewNews.setOnItemClickListener(new NewsItemClickListener());
        }
        return myInflatedView;
    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
    }

    public class LoadNews extends AsyncTask<String, String, String>
    {

        Activity context;
        private ProgressDialog progressDialog = null;

        public LoadNews(Activity context)
        {
            this.context = context;
        }

        @Override
        protected void onPostExecute(String result)
        {
            if (progressDialog != null)
            {
                progressDialog.dismiss();
            }
            TextView txt;
            View lay = myInflatedView.findViewById(R.id.empty);
            txt = (TextView) lay.findViewById(R.id.emptySetNews);
            if (MainActivity.checkError())
            {
                AdapterNews adapterNews = new AdapterNews(context, MainActivity.news);
                listViewNews.setAdapter(adapterNews);
            }

            if (MainActivity.serverDown)
            {
                txt.setText(getString(R.string.connection_error));
            }
            else if (MainActivity.errorGeneric)
            {
                txt.setText(getString(R.string.generic_error));
            }
            listViewNews.setOnItemClickListener(new NewsItemClickListener());
        }

        @Override
        protected String doInBackground(String... params)
        {
            if (isOnline())
                HttpRequest.getNews();
            else
                MainActivity.serverDown = true;
            return "Finish";

        }

        @Override
        protected void onPreExecute()
        {
            if (isOnline())
            {
                progressDialog = ProgressDialog.show(context, getString(R.string.download_alert_title), getString(R.string.news_list_download_alert_text), true);
                progressDialog.setCancelable(false);
            }

        }

        @Override
        protected void onProgressUpdate(String... values)
        {

        }


    }

    private class NewsItemClickListener implements ListView.OnItemClickListener
    {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id)
        {
            selectItem(position);
        }
    }

    private void selectItem(int position)
    {
        position--;
        String url = MainActivity.news.get(position).getId();
        Intent intent = new Intent(getActivity(), NewsActivity.class);
        intent.putExtra("position", position);
        intent.putExtra("url", url);
        MainActivity.reset();
        startActivity(intent);
    }

    public boolean isOnline()
    {
        try
        {
            ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            return netInfo != null && netInfo.isConnectedOrConnecting();
        } catch (Exception e)
        {
            return false;
        }
    }
}
