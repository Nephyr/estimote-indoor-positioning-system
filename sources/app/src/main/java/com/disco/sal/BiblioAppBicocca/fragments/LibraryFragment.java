package com.disco.sal.biblioappbicocca.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.disco.sal.biblioappbicocca.R;
import com.disco.sal.biblioappbicocca.activities.MainActivity;
import com.disco.sal.biblioappbicocca.adapters.AdapterLibraries;
import com.disco.sal.biblioappbicocca.classes.HttpRequest;
import com.disco.sal.biblioappbicocca.models.Libraries;

public class LibraryFragment extends Fragment
{

    //Object
    private static Libraries libraries;

    // RecycleerView
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private LinearLayoutManager mLayoutManager;

    private TextView emptyRecycleView;


    // TODO: Rename and change types and number of parameters
    public static LibraryFragment newInstance()
    {
        LibraryFragment fragment = new LibraryFragment();

        return fragment;
    }

    public LibraryFragment()
    {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_library, container, false);
        // Initialize
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.libraries_recycler_view);
        emptyRecycleView = (TextView) rootView.findViewById(R.id.empty_recycler_view);

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this.getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        if (libraries == null || libraries.getLibraries().size() == 0)
        {
            libraries = new Libraries();
            LoadLibraries loadLibraries = new LoadLibraries(getActivity());
            loadLibraries.execute();
        }
        else
        {
            mAdapter = new AdapterLibraries(getActivity(), libraries);
            mRecyclerView.setAdapter(mAdapter);
        }

    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        try
        {
        } catch (ClassCastException e)
        {
            throw new ClassCastException(activity.toString()
                                         + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach()
    {
        super.onDetach();

    }

    public class LoadLibraries extends AsyncTask<String, String, String>
    {

        Activity context;
        private ProgressDialog progressDialog = null;

        public LoadLibraries(Activity context)
        {
            this.context = context;
        }

        @Override
        protected void onPostExecute(String result)
        {
            if (progressDialog != null)
            {
                progressDialog.dismiss();
            }

            if (MainActivity.checkError())
            {
                if (isAdded())
                {
                    mAdapter = new AdapterLibraries(context, libraries);
                    mRecyclerView.setAdapter(mAdapter);
                }
            }

            if (MainActivity.serverDown)
            {
                emptyRecycleView.setVisibility(View.VISIBLE);
                emptyRecycleView.setText(getString(R.string.connection_error));
            }
            else if (MainActivity.errorGeneric)
            {
                emptyRecycleView.setVisibility(View.VISIBLE);
                emptyRecycleView.setText(getString(R.string.generic_error));
            }

        }

        @Override
        protected String doInBackground(String... params)
        {
            if (isOnline())
                HttpRequest.getLibraries(libraries);
            else
                MainActivity.serverDown = true;
            return "Finish";

        }

        @Override
        protected void onPreExecute()
        {
            if (isOnline())
            {
                progressDialog = ProgressDialog.show(context, getString(R.string.libraries_alert_title), getString(R.string.libraries_list_download_alert_text), true);
                progressDialog.setCancelable(false);
            }

        }

        @Override
        protected void onProgressUpdate(String... values)
        {

        }


    }


    public boolean isOnline()
    {
        try
        {
            ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            return netInfo != null && netInfo.isConnectedOrConnecting();
        } catch (Exception e)
        {
            return false;
        }
    }

}
