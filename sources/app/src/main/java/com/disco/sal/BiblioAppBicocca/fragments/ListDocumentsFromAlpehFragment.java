package com.disco.sal.biblioappbicocca.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.disco.sal.biblioappbicocca.R;
import com.disco.sal.biblioappbicocca.activities.DocumentDetailsActivity;
import com.disco.sal.biblioappbicocca.activities.MainActivity;
import com.disco.sal.biblioappbicocca.adapters.AdapterDocumentsPreviews;
import com.disco.sal.biblioappbicocca.classes.HttpRequest;

import java.util.ArrayList;

public class ListDocumentsFromAlpehFragment extends android.support.v4.app.Fragment
{

    private static final String ARG_PARAM1 = "param1";

    private String mParam1;

    private String query;
    private ListView listViewDocuments;
    boolean loadingMore = false;

    AdapterDocumentsPreviews adapterDocumentsPreviews;
    boolean firstLoading = false;

    ViewGroup container = null;
    LayoutInflater inflater = null;
    View myInflatedView;


    public static ListDocumentsFromAlpehFragment newInstance(String param1)
    {
        ListDocumentsFromAlpehFragment fragment = new ListDocumentsFromAlpehFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    public ListDocumentsFromAlpehFragment()
    {
        // Required empty public constructor

    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        setHasOptionsMenu(true);
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
        {
            mParam1 = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState)
    {
        this.inflater = inflater;
        this.container = container;
        //Questa variabile controlla che sia la prima volta che viene caricata la listview
        //Quando diventa true vuol dire che deve effettuare il primo caricamento
        firstLoading = false;
        // Inflate the layout for this fragment
        myInflatedView = inflater.inflate(R.layout.fragment_list_documents_opac, container, false);
        listViewDocuments = (ListView) myInflatedView.findViewById(R.id.listViewDocuments);
        listViewDocuments.setEmptyView(myInflatedView.findViewById(R.id.empty));


        return myInflatedView;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        inflater.inflate(R.menu.menu_documents_previews, menu);
        if (!query.equals(""))
            ((MainActivity) getActivity()).getSupportActionBar().setTitle(query);
        else
            ((MainActivity) getActivity()).getSupportActionBar().setTitle(getString(R.string.filter_search_toolbar_title));
        return;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();


        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
        if (MainActivity.query != null)
        {
            if (MainActivity.documentsPreviews == null ||
                !MainActivity.query.equals(MainActivity.queryPrec))
            {
                MainActivity.offset = 1;
                MainActivity.documentsPreviews = new ArrayList<>();
                MainActivity.queryPrec = MainActivity.query;
                firstLoading = true;
                loadingMore = true;
            }
            adapterDocumentsPreviews = new AdapterDocumentsPreviews(getActivity(), MainActivity.documentsPreviews);
            //listViewDocuments.setAdapter(adapterDocumentsPreviews);
            //enables filtering for the contents of the given ListView
            listViewDocuments.setTextFilterEnabled(true);
            listViewDocuments.setOnScrollListener(new AbsListView.OnScrollListener()
            {

                @Override
                public void onScrollStateChanged(AbsListView view, int scrollState)
                {
                }

                @Override
                public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount)
                {
                    if (MainActivity.checkError())
                    {
                        int lastInScreen = firstVisibleItem + visibleItemCount;
                        if ((lastInScreen == totalItemCount) && !(loadingMore))
                        {
                            if (isOnline())
                            {
                                LoadDocumentsPreviews loadDocumentsPreviews = new LoadDocumentsPreviews(getActivity());
                                loadDocumentsPreviews.execute();
                            }
                        }
                    }
                    else
                    {
                        // TODO: 28/06/15 Gestire errore
                    }
                }
            });
            if (firstLoading)
            {
                LoadDocumentsPreviews loadDocumentsPreviews = new LoadDocumentsPreviews(getActivity());
                loadDocumentsPreviews.execute();
            }
            else
            {
                listViewDocuments.setAdapter(adapterDocumentsPreviews);
            }
        }
        else
        {
            MainActivity.reset();
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.container, SearchFragment.newInstance(), "tag_fragment_search")
                    .commit();
        }


    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        try
        {
            query = getArguments().getString(ARG_PARAM1);
        } catch (ClassCastException e)
        {
            throw new ClassCastException(
                    activity.toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
    }

    public class LoadDocumentsPreviews extends AsyncTask<String, String, String>
    {

        Activity context;
        private ProgressDialog progressDialog = null;

        public LoadDocumentsPreviews(Activity context)
        {
            this.context = context;
        }

        @Override
        protected void onPostExecute(String result)
        {
            progressDialog.dismiss();
            TextView txt;
            View lay = myInflatedView.findViewById(R.id.empty);
            txt = (TextView) lay.findViewById(R.id.emptySet);
            if (MainActivity.emptySet)
            {
                txt.setText(getString(R.string.no_douments_error));
            }
            else if (MainActivity.errorConnection)
            {
                txt.setText(getString(R.string.connection_error));
            }
            else if (!MainActivity.checkError())
            {
                txt.setText(getString(R.string.generic_error));
            }
            adapterDocumentsPreviews.notifyDataSetChanged();
            loadingMore = false;
            listViewDocuments.setOnItemClickListener(new NewsItemClickListener());
            if (firstLoading == true)
            {
                LinearLayout linearLayout = new LinearLayout(getActivity().getApplicationContext());
                linearLayout.setOrientation(LinearLayout.VERTICAL);
                TextView textView = new TextView(getActivity().getApplicationContext());
                textView.setText(
                        getActivity().getResources().getString(R.string.number_of_results) + " " +
                        MainActivity.totalItem);
                //textView.setGravity(Gravity.CENTER);
                textView.setTextColor(Color.GRAY);
                textView.setTextSize(18);
                textView.setPadding(15, 5, 5, 5);
                linearLayout.addView(textView);
                listViewDocuments.addHeaderView(linearLayout, null, false);
                listViewDocuments.setAdapter(adapterDocumentsPreviews);
                firstLoading = false;
            }
        }

        @Override
        protected String doInBackground(String... params)
        {
            loadingMore = true;
            if (isOnline())
            {
                if (checKSimpleSearch())
                {
                    HttpRequest.getSimpleSearch(MainActivity.query);
                }
                else
                {
                    HttpRequest.getFilterSearch();
                }
            }
            else
                MainActivity.errorConnection = true;
            MainActivity.offset += MainActivity.paging;
            return "Finish";
        }

        @Override
        protected void onPreExecute()
        {
            progressDialog = ProgressDialog.show(context, getString(R.string.download_alert_title), getString(R.string.documents_preview_download_alert_text), true);
            progressDialog.setCancelable(false);
        }

        @Override
        protected void onProgressUpdate(String... values)
        {
        }

    }

    private boolean checKSimpleSearch()
    {
        if (MainActivity.titolo == null && MainActivity.autore == null &&
            MainActivity.editore == null && MainActivity.codice == null &&
            MainActivity.luogo == null && MainActivity.materiale == null &&
            MainActivity.anno == null && MainActivity.lingua == null)
            return true;
        else
            return false;
    }

    private class NewsItemClickListener implements ListView.OnItemClickListener
    {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id)
        {
            selectItem(position);
        }
    }

    private void selectItem(int position)
    {
        if (MainActivity.documentsPreviews != null)
        {
            position--;
            String url = MainActivity.documentsPreviews.get(position).getId();
            Intent intent = new Intent(getActivity(), DocumentDetailsActivity.class);
            intent.putExtra("position", position);
            intent.putExtra("url", url);
            MainActivity.document = null;
            startActivity(intent);
        }
    }

    public boolean isOnline()
    {
        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    @Override
    public void onResume()
    {
        if (MainActivity.documentsPreviews == null)
        {
            MainActivity.reset();
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.container, SearchFragment.newInstance(), "tag_fragment_search")
                    .commit();

        }

        super.onResume();

    }
}
