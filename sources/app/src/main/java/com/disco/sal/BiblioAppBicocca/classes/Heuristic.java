package com.disco.sal.biblioappbicocca.classes;

import com.estimote.sdk.Beacon;

import java.util.ArrayList;

/**
 * Created by Marco on 31/01/2016.
 */
public interface Heuristic
{
    void heuristic(ArrayList<Beacon> beacons, double[][] centers);
}
