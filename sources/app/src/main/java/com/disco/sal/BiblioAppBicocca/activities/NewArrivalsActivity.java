package com.disco.sal.biblioappbicocca.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.disco.sal.biblioappbicocca.R;
import com.disco.sal.biblioappbicocca.adapters.AdapterNewArrivals;
import com.disco.sal.biblioappbicocca.classes.HttpRequest;


public class NewArrivalsActivity extends ActionBarActivity
{

    String url = null;
    int position;
    int ricarica = 0;
    String scelta = null;
    int array;
    ListView listViewNewArrivals;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_arrivals);
        MainActivity.reset();
        toolbar = (Toolbar) findViewById(R.id.toolbargeneric);
        setSupportActionBar(toolbar);
        listViewNewArrivals = (ListView) findViewById(R.id.listViewNewArrivals);
        listViewNewArrivals.setEmptyView(findViewById(R.id.empty));
        MainActivity.viewNewArrivals = true;
        Intent intent = getIntent();
        url = intent.getStringExtra("url");
        position = intent.getIntExtra("position", 0);
        ricarica = intent.getIntExtra("ricarica", 0);
        scelta = intent.getStringExtra("scelta");
        array = intent.getIntExtra("array", 0);
        if (MainActivity.newArrivals.isEmpty() || ricarica == -1)
        {
            LoadNewArrivals loadNewArrivals = new LoadNewArrivals(this);
            loadNewArrivals.execute();
        }
        else
        {
            AdapterNewArrivals adapterNews = new AdapterNewArrivals(getApplicationContext(), MainActivity.newArrivals);
            listViewNewArrivals.setAdapter(adapterNews);
            listViewNewArrivals.setOnItemClickListener(new NewsItemClickListener());
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        String title = null;
        switch (array)
        {
            case 0:
                title = MainActivity.subFiltersArgumentBicocca.get(position).getName();
                break;
            case 1:
                title = MainActivity.subFiltersArgumentInsubria.get(position).getName();
                break;
            case 2:
                title = MainActivity.subFiltersArgumentAll.get(position).getName();
                break;
            case 3:
                title = MainActivity.subFiltersDisciplinaryAreaBicocca.get(position).getName();
                break;
            case 4:
                title = MainActivity.subFiltersDisciplinaryAreaInsubria.get(position).getName();
                break;
            case 5:
                title = MainActivity.subFiltersDisciplinaryAreaAll.get(position).getName();
                break;
            case 6:
                title = MainActivity.subFiltersSeatBicocca.get(position).getName();
                break;
            case 7:
                title = MainActivity.subFiltersSeatInsubria.get(position).getName();
                break;
            case 8:
                title = MainActivity.subFiltersSeatAll.get(position).getName();
                break;
        }
        getSupportActionBar().setTitle(title);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        return super.onOptionsItemSelected(item);
    }

    public class LoadNewArrivals extends AsyncTask<String, String, String>
    {

        Activity context;
        private ProgressDialog progressDialog = null;

        public LoadNewArrivals(Activity context)
        {
            this.context = context;
        }

        @Override
        protected void onPostExecute(String result)
        {
            progressDialog.dismiss();
            if (MainActivity.checkError())
            {
                AdapterNewArrivals adapterNews = new AdapterNewArrivals(context, MainActivity.newArrivals);
                listViewNewArrivals.setAdapter(adapterNews);
                listViewNewArrivals.setOnItemClickListener(new NewsItemClickListener());
            }
            else
            {
                TextView txt;
                View lay = findViewById(R.id.empty);
                txt = (TextView) lay.findViewById(R.id.emptySetNewsArrivals);
                if (MainActivity.serverDown)
                {
                    txt.setText(getString(R.string.connection_error));
                }
                else if (MainActivity.errorGeneric)
                {
                    txt.setText(getString(R.string.generic_error));
                }
            }
        }

        @Override
        protected String doInBackground(String... params)
        {
            if (isOnline())
                HttpRequest.getNewArrivals(url);
            else
                MainActivity.serverDown = true;
            return "Finish";

        }

        @Override
        protected void onPreExecute()
        {
            progressDialog = ProgressDialog.show(context, getString(R.string.download_alert_title), getString(R.string.new_arrivals_download_alert_text), true);
            progressDialog.setCancelable(false);
        }

        @Override
        protected void onProgressUpdate(String... values)
        {
        }


    }

    private class NewsItemClickListener implements ListView.OnItemClickListener
    {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id)
        {
            selectItem(position);
        }
    }

    private void selectItem(int position)
    {
        String url = MainActivity.newArrivals.get(position).getUrl();
        Intent intent = new Intent(getApplicationContext(), DocumentDetailsActivity.class);
        intent.putExtra("position", position);
        intent.putExtra("url", url);
        MainActivity.document = null;
        startActivity(intent);
    }

    public boolean isOnline()
    {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }
}
