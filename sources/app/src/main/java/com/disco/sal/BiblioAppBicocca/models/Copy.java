package com.disco.sal.biblioappbicocca.models;

/**
 * Created by lorenzo on 12/05/15.
 */
public class Copy {
    private String barcode;
    private String collection;
    private String dueDate;
    private String loanStatus;
    private String location;
    private String subLibrary;

    public Copy(String barcode, String collection, String dueDate, String loanStatus, String location, String subLibrary) {
        this.barcode = barcode;
        this.collection = collection;
        this.dueDate = dueDate;
        this.loanStatus = loanStatus;
        this.location = location;
        this.subLibrary = subLibrary;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getCollection() {
        return collection;
    }

    public void setCollection(String collection) {
        this.collection = collection;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public String getLoanStatus() {
        return loanStatus;
    }

    public void setLoanStatus(String loanStatus) {
        this.loanStatus = loanStatus;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getSubLibrary() {
        return subLibrary;
    }

    public void setSubLibrary(String subLibrary) {
        this.subLibrary = subLibrary;
    }
}
