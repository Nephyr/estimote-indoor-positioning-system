package com.disco.sal.biblioappbicocca.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.disco.sal.biblioappbicocca.R;
import com.disco.sal.biblioappbicocca.classes.Room;
import com.disco.sal.biblioappbicocca.classes.Shelf;
import com.jjoe64.graphview.GraphView;

import java.util.ArrayList;
import java.util.Collection;

public class AdapterSIS extends BaseAdapter
{
    private LayoutInflater inflater;
    private ArrayList<Shelf> shelves;
    private Boolean[] flags;
    private Context context;
    private boolean first;

    public AdapterSIS(Context context, Room room)
    {
        this.inflater = LayoutInflater.from(context);
        this.shelves = new ArrayList<Shelf>();
        this.flags = new Boolean[room.getSizeShelves()];
        this.context = context;
        first = true;
    }

    public void replaceWith(Collection<Shelf> newBeacons)
    {
        this.shelves.clear();
        this.shelves.addAll(newBeacons);
        if (first)
        {
            for (int i = 0; i < newBeacons.size(); i++)
            {
                flags[i] = true;
            }
            first = false;
        }
        notifyDataSetChanged();
    }

    @Override
    public int getCount()
    {
        return shelves.size();
    }

    @Override
    public Shelf getItem(int position)
    {
        return shelves.get(position);
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent)
    {
        view = inflateIfRequired(view, position, parent);
        bind(getItem(position), view, position);
        return view;
    }

    private void bind(Shelf shelf, View view, int position)
    {
        ViewHolder holder = (ViewHolder) view.getTag();
        holder.shelf.setText("Scaffale numero: " + shelf.getName());
        if (flags[position])
            holder.status.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_green));
        else
            holder.status.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_red));
    }

    private View inflateIfRequired(View view, int position, ViewGroup parent)
    {
        if (view == null)
        {
            view = inflater.inflate(R.layout.shelf_item, null);
            view.setTag(new ViewHolder(view));
        }
        return view;
    }

    static class ViewHolder
    {
        final TextView shelf;
        public ImageView status;

        ViewHolder(View view)
        {
            shelf = (TextView) view.findViewById(R.id.textViewShelf);
            status = (ImageView) view.findViewById(R.id.statusSIS);
        }
    }

    public void changeStatus(int position, GraphView graph)
    {
        if (flags[position])
        {
            flags[position] = false;
            shelves.get(position).colorShelfRed(graph);
        }
        else
        {
            flags[position] = true;
            shelves.get(position).colorShelf(graph);
        }
    }
}
