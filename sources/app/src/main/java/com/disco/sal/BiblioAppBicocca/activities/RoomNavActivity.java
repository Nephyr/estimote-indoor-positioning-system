/*package com.disco.sal.biblioappbicocca.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Pair;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.disco.sal.biblioappbicocca.R;
import com.disco.sal.biblioappbicocca.adapters.AdapterSIS;
import com.disco.sal.biblioappbicocca.classes.AVGDistanceThreeNearestBeacons;
import com.disco.sal.biblioappbicocca.classes.Room;
import com.disco.sal.biblioappbicocca.classes.SISManager;
import com.disco.sal.biblioappbicocca.classes.TaskCanceler;
import com.estimote.sdk.Beacon;
import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.Region;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.GridLabelRenderer;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.PointsGraphSeries;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public class RoomNavActivity extends ActionBarActivity
{
    private final static int REQUEST_ENABLE_BT = 1;
    private static final Region ALL_ESTIMOTE_BEACONS_REGION = new Region("rid", null, null, null);
    private Handler handler;
    BeaconManager beaconManager;
    private String macNearestBeacon;
    FindBeacons findBeacons;
    private ProgressDialog progressDialog;
    //private TaskCanceler taskCanceler;
    private ActionBarActivity context;
    //private boolean recheck;
    private int n_rechecks;
    //private int c = 0;
    private int nManagedBeaconsAtTime; //indica il numero di beacons stabiliti per l'euristica
    private boolean mapLoaded; //flag che indica se ho già caricato una prima volta la mappa del piano in cui sono
    private boolean popupWrongRoom; //flag che indica se deve aprirsi il popup che indica all'utente di essere nella stanza sbagliata (falso = non deve aprirsi)
    AdapterSIS adapter;
    ListView listsis;
    ScrollView scrollView;
    Intent intent;
    String sublibrary, url;
    Toolbar toolbar;
    GraphView graph;
    TextView title, room, shelves;

    private AVGDistanceThreeNearestBeacons euristica;
    private int countEuristica;
    PointsGraphSeries<DataPoint> userPosition;
    private boolean wrongLibrary;
    private boolean isRangingStarted;
    private SisNetworkTask sisTask;

    // MONITOR BTH STATUS
    private final BroadcastReceiver mBthReceiver = new BroadcastReceiver()
    {
        public void onReceive(Context context, Intent intent)
        {
            String action = intent.getAction();
            //controllo che sia stato chiamato per un cambiamento di stato del bt
            if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(action))
            {
                //torna -1 se non trova il risultato di extra_state. La prima parte mi restituisce il power state
                if (intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, -1) == BluetoothAdapter.STATE_TURNING_OFF)
                {
                    // Pulisco la posizione rimuovendo il segnalino dalla mappa.
                    if (userPosition != null)
                        graph.removeSeries(userPosition);

                    if(findBeacons != null)
                    {
                        findBeacons.cancel(true);
                        if(isRangingStarted)
                            beaconManager.stopRanging(ALL_ESTIMOTE_BEACONS_REGION);
                    }
                        //if (findBeacons.getStatus() == AsyncTask.Status.RUNNING)
                        //{
                            //qua devo mettere gli stessi controlli di onStop?? pensaci

                            //qui in teoria dovrei controllare che il ranging sia stato attivato. Se sono (s)fortunato potrei
                            //ad esempio tornare indietro nell'istante in cui è partito l'async task ma non il ranging, e se succede sono guai
                            //if(isRangingStarted)
                           // {

                                //isRangingStarted = false;
                            //}
                        //}
                }
                else if (intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, -1) == BluetoothAdapter.STATE_TURNING_ON)
                {
                    //proseguo solo quando il bluetooth è pronto per essere usato (in questo caso era già acceso)
                    progressToNextStep();
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_nav);

        context = this;
        intent = getIntent();
        sublibrary = intent.getStringExtra("library").replace(" ", "_");
        nManagedBeaconsAtTime = 3;

    }

    @Override
    public void onStop()
    {
        super.onStop();
        if(findBeacons != null)
            //if (findBeacons.getStatus() == AsyncTask.Status.RUNNING)
            {
                findBeacons.cancel(true);
                //qui in teoria dovrei controllare che il ranging sia stato attivato. Se sono (s)fortunato potrei
                //ad esempio tornare indietro nell'istante in cui è partito l'async task ma non il ranging, e se succede sono guai
                if(isRangingStarted)
                    beaconManager.stopRanging(ALL_ESTIMOTE_BEACONS_REGION);
               // beaconManager.disconnect();
            }

        // Unregister broadcast listeners
        unregisterReceiver(mBthReceiver);
    }

    @Override
    public void onStart()
    {
        super.onStart();

        mapLoaded      = false;
        popupWrongRoom = true;
        countEuristica = 0;
        n_rechecks     = 0;
        wrongLibrary   = false;
        isRangingStarted = false;
        MainActivity.beaconCurrentPair = null;
        sisTask = new SisNetworkTask();

        euristica = new AVGDistanceThreeNearestBeacons();


        // Register for broadcasts on BluetoothAdapter state change
        IntentFilter filter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);

        //il broadcast receiver viene associato all'intent filter di prima e verrà invocato con qualsiasi broadcast intent che corrisponde al filtro
        registerReceiver(mBthReceiver, filter);

        // mi occupo del bluetooth
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        if (mBluetoothAdapter == null)
        {
            // Device does not support Bluetooth. Va sistemato, nel senso che ora si chiude l'alert e appare lo scheletro del layout
            AlertDialog.Builder alert = new AlertDialog.Builder(RoomNavActivity.this);
            alert.setMessage(R.string.bt_not_supported);
            alert.setPositiveButton("OK", null);
            alert.setCancelable(true);
            alert.create().show();
        }
        else if (!mBluetoothAdapter.isEnabled())
        {
            //creo un intent per lanciare l'activity che permette all'utente di accendere il bluetooth
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);

            //faccio partire l'activity tramite l'intent e ci associo un "request code" che identifica la richiesta (bt in questo caso)
            //forse può funzionare anche con una normale start activity
            //startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            startActivity(enableBtIntent);
        }
        else
        {
            //proseguo solo quando il bluetooth è già pronto per essere usato (in questo caso era già acceso)
            progressToNextStep();
        }
    }

    /*@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        //è arrivato un risultato da una activity, controllo che sia quello riferito alla richiesta del bluetooth
        if (requestCode == REQUEST_ENABLE_BT)
        {
            if (resultCode == RESULT_OK)
            {
                // Request granted - bluetooth is turning on...
                System.out.println("bt acceso");
                //proseguo solo quando il bluetooth è pronto per essere usato (in questo caso è stato appena acceso)
                //progressToNextStep();
            }
            if (resultCode == RESULT_CANCELED)
            {
                // Request denied by user, or an error was encountered while
                // attempting to enable bluetooth
                System.out.println("l'utente non vuole accendere il bt");
            }
        }
    }*/

    /*private void progressToNextStep()
    {
        progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
        progressDialog.setTitle(R.string.wait_title);
        progressDialog.setMessage(context.getResources().getString(R.string.find_nearest_beacon));
        progressDialog.show();

        handler       = new Handler();
        beaconManager = new BeaconManager(this);
        findBeacons   = new FindBeacons();

        //recheck.run();
        findBeacons.execute();
        handler.postDelayed(recheck, 15000);
    }

    Runnable recheck = new Runnable()
    {
        @Override
        public void run()
        {
            n_rechecks++;
            if (progressDialog != null && progressDialog.isShowing())
            {
                progressDialog.dismiss();
            }

            if (macNearestBeacon == null)
            {
                AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
                builder1.setMessage(R.string.beacons_not_found);
                builder1.setPositiveButton(R.string.affirmative_answer, new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int id)
                    {
                        // User clicked OK button
                        if (n_rechecks >= 4)
                        {
                            findBeacons.cancel(true);
                            beaconManager.stopRanging(ALL_ESTIMOTE_BEACONS_REGION);
                            // è andata male, faccio apparire le info della altra activity con un messaggio di errore
                            //System.out.println("sono morto");
                            view(true);

                            AlertDialog.Builder alertFinal = new AlertDialog.Builder(RoomNavActivity.this);
                            alertFinal.setMessage(R.string.localization_ko);
                            alertFinal.setPositiveButton("OK", null);
                            alertFinal.setCancelable(true);
                            alertFinal.create().show();
                        }
                        else
                        {
                            handler.postDelayed(recheck, 6000);
                        }

                    }
                });
                builder1.setNegativeButton(R.string.negative_answer, new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int id)
                    {
                        // User cancelled the dialog
                        findBeacons.cancel(true);
                        beaconManager.stopRanging(ALL_ESTIMOTE_BEACONS_REGION);
                        // è andata male, faccio apparire le info della altra activity con un messaggio di errore
                        //System.out.println("sono morto");
                        view(true);
                        AlertDialog.Builder alertFinal = new AlertDialog.Builder(RoomNavActivity.this);
                        alertFinal.setMessage(R.string.localization_ko);
                        alertFinal.setPositiveButton("OK", null);
                        alertFinal.setCancelable(true);
                        alertFinal.create().show();
                    }
                });
                AlertDialog dialog1 = builder1.create();
                dialog1.setCancelable(false);
                dialog1.setCanceledOnTouchOutside(false);
                dialog1.show();
            }
        }
    };

    private class SisNetworkTask extends AsyncTask<String, Void, Void>
    {
        ProgressDialog p1;

        @Override
        protected void onPreExecute()
        {
            //super.onPreExecute();
            p1 = new ProgressDialog(context);
            p1.setMessage("sto recuperando le info della stanza");
            p1.setCancelable(false);
            p1.show();

        }

        @Override
        // TODO: Propagare la modifica sulla classe ROOM eliminando il Pair
        protected Void doInBackground(String... macaddr)
        {
            ArrayList<String> tmpR = SISManager.getSISManager().getRoomFromBeacon(macaddr[0]);
            ArrayList<String> tmpS = SISManager.getSISManager().getListShelves(tmpR.get(0));
            Room tmpRoom           = SISManager.getSISManager().getObjectRoom(tmpR.get(0), tmpS);

            Pair<String, Room> resPair;
            if(tmpR.size() > 0) resPair = new Pair<String, Room>(tmpRoom.getParentLibrary().get(0), tmpRoom);
            else resPair = new Pair<String, Room>("UNKNOWN", null);
            MainActivity.beaconCurrentPair =  resPair;
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid)
        {
            //super.onPostExecute(aVoid);
            p1.dismiss();
        }
    }

    private class FindBeacons extends AsyncTask<Void, Void, Void>
    {
        @Override
        protected Void doInBackground(Void... params)
        {
            beaconManager.setRangingListener(new BeaconManager.RangingListener()
            {
                @Override
                public void onBeaconsDiscovered(Region region, final List beacons)
                {
                    if (beacons.isEmpty())
                        return;

                    macNearestBeacon = ((Beacon) beacons.get(0)).getMacAddress().toString();
                    macNearestBeacon = macNearestBeacon.substring(1, macNearestBeacon.length() - 1);

                    //controllo se il beacon trovato sia tra quelli caricati su SIS. In realtà dovrei farlo con tutti i beacons che
                    //userò e dovrei farlo appena trovo beacons scremando quelli che non sono mappati su SIS, di modo da non usare
                    //mai (quindi manco per la trilaterazione) beacons non mappati. La scrematura avverrebbe all'inizio, se la List
                    //beacons non è vuota, e dopo setterei macNearestBeacon, ma non so quanti beacons troverò.


                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();
                    if(sisTask.getStatus() != AsyncTask.Status.RUNNING)
                    {
                        sisTask.execute("DE:03:E7:D9:C6:17");
                    }

                    if(MainActivity.beaconCurrentPair==null)
                        return;

                    Pair<String, Room> tmpPair = MainActivity.beaconCurrentPair;

                    //non ho identificato la stanza in cui sono, capita se i beacons non sono stati mappati
                    if(MainActivity.beaconCurrentPair.second == null)
                        return;

                    //controllo in che biblioteca sono (se la identifico)
                    if (!MainActivity.beaconCurrentPair.first.equals(sublibrary) &&
                        !MainActivity.beaconCurrentPair.first.equals("UNKNOWN"))
                    {
                        if (wrongLibrary) return;
                        else wrongLibrary = true;

                        AlertDialog.Builder builder3 = new AlertDialog.Builder(context);
                        String msg = getResources().getString(R.string.wrong_library);
                        msg = msg + sublibrary + " " + MainActivity.room.getName();
                        builder3.setMessage(msg);
                        builder3.setPositiveButton("OK", null);
                        AlertDialog dialog3 = builder3.create();
                        dialog3.setCancelable(false);
                        dialog3.setCanceledOnTouchOutside(false);
                        dialog3.show();
                        return;
                    }

                    if(tmpPair == null)
                    {
                        view(false); //codice per caricare il piano giusto la prima volta (con "false" carico quello del piano in cui sono)
                        checkRightRoom(); //codice per controllare che io sia nella giusta stanza
                    }
                    else if (tmpPair.second != null)
                    {
                        //controllo se ho cambiato piano
                        if (!MainActivity.beaconCurrentPair.second.getName().equals(tmpPair.second.getName()))
                        {
                            view(false);
                            checkRightRoom();

                            // Pulisco la posizione rimuovendo il segnalino dalla mappa.
                            if (userPosition != null)
                                graph.removeSeries(userPosition);
                        }
                    }
                    else return;

                    if (beacons.size() >= nManagedBeaconsAtTime)
                    {
                        //faccio le cose da qui in avanti se ho trovato sufficienti beacons per il mio algoritmo di posizionamento
                        //non ha senso copiare il mac di tutti i beacons trovati, tanto con l'algoritmo usato ora ne uso tre alla volta...
                        //nel caso si cambia l'attributo nManagedBeaconsAtTime
                        //e finalmente si usa l'algoritmo di posizionamento con l'euristica seguito dal codice per far apparire la posizione
                        ArrayList<Beacon> listaB = new ArrayList<Beacon>();

                        for (int i = 0; i < nManagedBeaconsAtTime; i++)
                            listaB.add((Beacon) beacons.get(i));

                        //euristica.heuristic(listaB);
                        countEuristica++;

                        if (countEuristica == 4)
                        {
                            countEuristica = 0;
                            double[] posizione = euristica.getMedia();
                            // a questo punto visualizzo la posizione
                            updatePosition(posizione[0], posizione[1]);
                        }
                    }
                }
            });

            beaconManager.connect(new BeaconManager.ServiceReadyCallback()
            {
                @Override
                public void onServiceReady()
                {
                    beaconManager.startRanging(ALL_ESTIMOTE_BEACONS_REGION);
                    isRangingStarted = true;
                }
            });

            return null;
        }

        /*private Pair<String, Room> isInRightRoom(String[] macBeacons)
        {
            HashMap<String, HashMap<Room, Integer>> beaconCheckRoom = new HashMap<String, HashMap<Room, Integer>>();
            for (Map.Entry<String, ArrayList<Room>> element : MainActivity.cacheRooms.entrySet())
            {
                beaconCheckRoom.put(element.getKey(), new HashMap<Room, Integer>());
                for (int i = 0; i < element.getValue().size(); i++)
                {
                    Room tmpRoom = element.getValue().get(i);
                    int tmpCount = 0;

                    for (int n = 0; n < macBeacons.length; n++)
                        if (tmpRoom.getMacbeacons().contains(macBeacons[n]))
                            tmpCount++;

                    beaconCheckRoom.get(element.getKey()).put(tmpRoom, tmpCount);
                }
            }

            int tmpMax = 0;
            Pair<String, Room> resPair = new Pair<String, Room>("UNKNOWN", null);

            for (Map.Entry<String, ArrayList<Room>> element : MainActivity.cacheRooms.entrySet())
                for (int i = 0; i < element.getValue().size(); i++)
                {
                    int tmpCount = 0;
                    Room tmpRoom = element.getValue().get(i);

                    if (!beaconCheckRoom.get(element.getKey()).isEmpty())
                        tmpCount = beaconCheckRoom.get(element.getKey()).get(tmpRoom);

                    if (tmpMax < tmpCount)
                    {
                        resPair = new Pair<String, Room>(element.getKey(), tmpRoom);
                        tmpMax = tmpCount;
                    }
                }

            return resPair;
        }
        */

        /*public void updatePosition(double x, double y)
        {
                    if (userPosition != null)
                        graph.removeSeries(userPosition);

                    userPosition = new PointsGraphSeries<DataPoint>(new DataPoint[]{
                            new DataPoint(x, y),
                    });
                    userPosition.setShape(PointsGraphSeries.Shape.POINT);
                    userPosition.setColor(Color.parseColor("#FFFF4444"));
                    userPosition.setSize(8);
                    graph.addSeries(userPosition);
        }

        private void checkRightRoom()
        {
            //popup di errore "sei nel piano sbagliato, recati a ... "
            //ricordati di controllare che il popup non esista già, o apparirano a raffica (OK)
            if (!MainActivity.beaconCurrentPair.second.getName().equals(MainActivity.room.getName()))
            {
                if (popupWrongRoom)
                {
                    popupWrongRoom = false;
                    AlertDialog.Builder builder2 = new AlertDialog.Builder(context);
                    String msg = getResources().getString(R.string.wrong_room);
                    msg = msg + MainActivity.room.getName();
                    builder2.setMessage(msg);
                    builder2.setPositiveButton("OK", new DialogInterface.OnClickListener()
                    {
                        public void onClick(DialogInterface dialog, int id)
                        {
                            // User clicked OK button
                            popupWrongRoom = true;
                        }
                    });
                    AlertDialog dialog2 = builder2.create();
                    dialog2.setCancelable(false);
                    dialog2.setCanceledOnTouchOutside(false);
                    dialog2.show();
                }
            }
        }
    }

    private void view(final boolean defaultView)
    {
        runOnUiThread(new Runnable()
        {
            public void run()
            {
                final Room r;
                boolean draw;
                if (defaultView)
                {
                    r = MainActivity.room;
                    draw = true;
                }
                else
                {
                    r = MainActivity.beaconCurrentPair.second;
                    draw = false;
                }
                setContentView(R.layout.activity_room);
                scrollView = (ScrollView) findViewById(R.id.scrollViewSIS);
                scrollView.smoothScrollTo(0, 0);
                intent = getIntent();
                listsis = (ListView) findViewById(R.id.listViewSIS);
                graph = (GraphView) findViewById(R.id.SISview);
                graph.removeAllSeries();
                toolbar = (Toolbar) findViewById(R.id.toolbargeneric);
                toolbar.setTitle("Locazione");
                setSupportActionBar(toolbar);
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setDisplayShowHomeEnabled(true);
                context.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                context.getSupportActionBar().setDisplayShowHomeEnabled(true);
                toolbar.setNavigationOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        onBackPressed();
                    }
                });
                title = (TextView) findViewById(R.id.textViewSisTitle);
                room = (TextView) findViewById(R.id.textViewRoom);
                //sublibrary = intent.getStringExtra("library");
                if (defaultView)
                    title.setText(sublibrary + ":");
                else
                    title.setText(MainActivity.beaconCurrentPair.first + ":");
                shelves = (TextView) findViewById(R.id.textNumbershelves);
                graph.removeAllSeries();
                if (sublibrary.contains("Bicocca_Sede_Centrale"))
                {
                    room.setText(r.getName().replace("_", " "));
                    drawGraph(draw);
                    r.drawRoom(graph);

                    adapter = new AdapterSIS(context);
                    listsis = (ListView) findViewById(R.id.listViewSIS);
                    listsis.setAdapter(adapter);
                    adapter.replaceWith(r.getShelves());

                    listsis.setOnItemClickListener(new AdapterView.OnItemClickListener()
                    {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id)
                        {
                            adapter.changeStatus(position, graph);
                            adapter.replaceWith(r.getShelves());
                        }
                    });

                    listsis.setOnTouchListener(new View.OnTouchListener()
                    {
                        // Setting on Touch Listener for handling the touch inside ScrollView
                        @Override
                        public boolean onTouch(View v, MotionEvent event)
                        {
                            // Disallow the touch request for parent scroll on touch of child view
                            v.getParent().requestDisallowInterceptTouchEvent(true);
                            return false;
                        }
                    });
                }

            }
        });

    }

    private void drawGraph(boolean defaultView)
    {
        Room r;
        if (defaultView)
            r = MainActivity.room;
        else
            r = MainActivity.beaconCurrentPair.second;
        graph.getGridLabelRenderer().setGridStyle(GridLabelRenderer.GridStyle.NONE);
        graph.getGridLabelRenderer().setHorizontalLabelsVisible(false);
        graph.getGridLabelRenderer().setVerticalLabelsVisible(false);


        graph.getViewport().setXAxisBoundsManual(true);
        graph.getViewport().setYAxisBoundsManual(true);

        //calcolo dimensione grafico in base alla dimensione della stanza selezionata (il grafico deve essere quadrato per le proporzioni grafiche)
        double dimGraph = 0;
        for (double[] xy : r.getXYs())
        {
            if (xy[0] > dimGraph)
            {
                dimGraph = xy[0];
            }
            if (xy[1] > dimGraph)
            {
                dimGraph = xy[1];
            }
        }
        graph.getViewport().setMaxX(dimGraph);
        graph.getViewport().setMaxY(dimGraph);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu)
    {
        return false;
    }
}*/



