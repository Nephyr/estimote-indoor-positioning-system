package com.disco.sal.biblioappbicocca.classes;

import android.graphics.Color;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Matteo Vegini on 08/11/2015.
 * Modified by Marco Sgura on 05/02/2016
 */
public class Shelf implements Serializable
{
    String name;
    ArrayList<double[]> XYs; //lista spigloli in metri
    LineGraphSeries<DataPoint> shelfWalla;

    public Shelf(String name)
    {
        this.name = name;
    }

    public void drawShelf(GraphView graphView)
    {
        double x1, y1, x2, y2;
        int size = this.XYs.size();
        LineGraphSeries<DataPoint> shelfWalls;
        for (int i = 0; i < size - 1; i++)
        {
            x1 = (this.XYs.get(i))[0];
            y1 = (this.XYs.get(i))[1];
            x2 = (this.XYs.get(i + 1))[0];
            y2 = (this.XYs.get(i + 1))[1];
            shelfWalls = new LineGraphSeries<DataPoint>(new DataPoint[]{
                    new DataPoint(x1, y1),
                    new DataPoint(x2, y2),
            });
            shelfWalls.setColor(Color.parseColor("#8B4513"));
            shelfWalls.setThickness(1);
            graphView.addSeries(shelfWalls);
        }
        x1 = (this.XYs.get(0))[0];
        y1 = (this.XYs.get(0))[1];
        x2 = (this.XYs.get(size - 1))[0];
        y2 = (this.XYs.get(size - 1))[1];
        shelfWalls = new LineGraphSeries<DataPoint>(new DataPoint[]{
                new DataPoint(x1, y1),
                new DataPoint(x2, y2),
        });
        shelfWalls.setColor(Color.parseColor("#8B4513"));
        shelfWalls.setThickness(1);
        graphView.addSeries(shelfWalls);
    }

    public void colorShelf(GraphView graphView)
    {
        int size = XYs.size();
        double x1, y1;
        DataPoint[] dataPoints = new DataPoint[5];
        graphView.removeSeries(shelfWalla);
        for (int i = 0; i < size; i++)
        {
            x1 = (XYs.get(i))[0];
            y1 = (XYs.get(i))[1];
            dataPoints[i] = new DataPoint(x1, y1);
        }
        x1 = (XYs.get(0))[0];
        y1 = (XYs.get(0))[1];
        dataPoints[4] = new DataPoint(x1, y1);
        shelfWalla = new LineGraphSeries<DataPoint>(dataPoints);
        shelfWalla.setColor(Color.GREEN);
        shelfWalla.setThickness(3);
        graphView.addSeries(shelfWalla);
    }

    public void colorGarden(GraphView graphView)
    {
        int size = XYs.size();
        double x1, x2, y1, y2;
        LineGraphSeries<DataPoint> shelfWallg;
        for (int i = 0; i < size - 1; i++)
        {
            x1 = (XYs.get(i))[0];
            y1 = (XYs.get(i))[1];
            x2 = (XYs.get(i + 1))[0];
            y2 = (XYs.get(i + 1))[1];
            shelfWallg = new LineGraphSeries<DataPoint>(new DataPoint[]{
                    new DataPoint(x1, y1),
                    new DataPoint(x2, y2),
            });
            shelfWallg.setColor(Color.BLACK);
            shelfWallg.setThickness(5);
            graphView.addSeries(shelfWallg);
        }
        x1 = (XYs.get(0))[0];
        y1 = (XYs.get(0))[1];
        x2 = (XYs.get(size - 1))[0];
        y2 = (XYs.get(size - 1))[1];
        shelfWallg = new LineGraphSeries<DataPoint>(new DataPoint[]{
                new DataPoint(x1, y1),
                new DataPoint(x2, y2),
        });
        shelfWallg.setColor(Color.BLACK);
        shelfWallg.setThickness(5);
        graphView.addSeries(shelfWallg);
    }

    public void colorShelfRed(GraphView graphView)
    {
        int size = XYs.size();
        double x1, y1;
        DataPoint[] dataPoints = new DataPoint[5];
        graphView.removeSeries(shelfWalla);
        for (int i = 0; i < size; i++)
        {
            x1 = (XYs.get(i))[0];
            y1 = (XYs.get(i))[1];
            dataPoints[i] = new DataPoint(x1, y1);
        }
        x1 = (XYs.get(0))[0];
        y1 = (XYs.get(0))[1];
        dataPoints[4] = new DataPoint(x1, y1);
        shelfWalla = new LineGraphSeries<DataPoint>(dataPoints);
        shelfWalla.setColor(Color.RED);
        shelfWalla.setThickness(3);
        graphView.addSeries(shelfWalla);
    }

    public void setXYsFromCells(ArrayList<String> cellslist)
    {
        ArrayList<double[]> list = new ArrayList<>();
        double[] tmp;
        for (int i = 0; i < cellslist.size(); i++)
        {
            tmp = new double[2];
            tmp[0] = Double.parseDouble(cellslist.get(i).split(",")[0]);
            tmp[1] = Double.parseDouble(cellslist.get(i).split(",")[1]);
            list.add(tmp);
        }

        double x1 = list.get(0)[0] / 10;
        double y1 = list.get(0)[1] / 10;
        double x2 = 0, y2 = 0;

        for (int i = 0; i < list.size(); i++)
        {
            if ((list.get(i)[0] / 10) < x1)
                x1 = (list.get(i)[0] / 10);
            if ((list.get(i)[1] / 10) < y1)
                y1 = (list.get(i)[1] / 10);

            if (((list.get(i)[0] / 10) + 0.1) > x2)
                x2 = (list.get(i)[0] / 10) + 0.1;
            if (((list.get(i)[1] / 10) + 0.1) > y2)
                y2 = (list.get(i)[1] / 10) + 0.1;
        }

        ArrayList<double[]> XYs = new ArrayList<>();
        XYs.add(new double[]{x1, y1});
        XYs.add(new double[]{x2, y1});
        XYs.add(new double[]{x2, y2});
        XYs.add(new double[]{x1, y2});

        this.XYs = XYs;
    }


    public String getName()
    {
        return this.name;
    }

    public ArrayList<double[]> getXYs()
    {
        return this.XYs;
    }
}

