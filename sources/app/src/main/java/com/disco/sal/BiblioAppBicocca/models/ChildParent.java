package com.disco.sal.biblioappbicocca.models;

/**
 * Created by lorenzo on 13/05/15.
 */
public class ChildParent {
    private String title;
    private String url;

    public ChildParent(String title, String url) {
        this.title = title;
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
