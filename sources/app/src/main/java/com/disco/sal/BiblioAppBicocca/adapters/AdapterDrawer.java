package com.disco.sal.biblioappbicocca.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.disco.sal.biblioappbicocca.R;
import com.disco.sal.biblioappbicocca.models.DrawerItem;

import java.util.List;

/**
 * Created by lorenzo on 08/04/15.
 */
public class AdapterDrawer extends ArrayAdapter<DrawerItem>
{
    Context context;
    List<DrawerItem> drawerItemList;
    int layoutResID;

    public AdapterDrawer(Context context, int layoutResourceID, List<DrawerItem> listItems)
    {
        super(context, layoutResourceID, listItems);
        this.context = context;
        this.drawerItemList = listItems;
        this.layoutResID = layoutResourceID;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.item_list_drawer, parent, false);
        TextView txtTitle = (TextView) rowView.findViewById(R.id.drawer_name);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.drawer_icon);
        imageView.setImageDrawable(rowView.getResources().getDrawable(drawerItemList.get(position).getImgResID()));
        txtTitle.setText(drawerItemList.get(position).getItemName());
        return rowView;
    }

}
