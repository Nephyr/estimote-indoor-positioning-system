package com.disco.sal.biblioappbicocca.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.disco.sal.biblioappbicocca.R;
import com.disco.sal.biblioappbicocca.models.News;

import java.util.ArrayList;

/**
 * Created by lorenzo on 28/04/15.
 */

public class AdapterNews extends ArrayAdapter<News>
{
    private final Context context;
    private final ArrayList<News> news;


    public AdapterNews(Context context, ArrayList<News> news)
    {

        super(context, R.layout.item_news, news);
        this.context = context;
        this.news = news;

    }

    @SuppressLint("ViewHolder")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.item_news, parent, false);
        TextView title = (TextView) rowView.findViewById(R.id.titleNews);
        TextView date = (TextView) rowView.findViewById(R.id.dateNews);
        TextView tag = (TextView) rowView.findViewById((R.id.tagNews));
        News news = getItem(position);
        title.setText(news.getTitle());
        date.setText(news.getDate());
        tag.setText(news.getTag().toUpperCase());
        return rowView;
    }
}

