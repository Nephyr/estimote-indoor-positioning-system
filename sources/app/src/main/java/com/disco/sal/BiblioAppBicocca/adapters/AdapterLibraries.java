package com.disco.sal.biblioappbicocca.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.disco.sal.biblioappbicocca.R;
import com.disco.sal.biblioappbicocca.classes.HttpRequest;
import com.disco.sal.biblioappbicocca.models.Libraries;
import com.disco.sal.biblioappbicocca.utility.DisplayDimension;

/**
 * Created by lorenzo on 17/09/15.
 */
public class AdapterLibraries extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{

    private static final int VIEW_TYPE_LIBRARY = 0;
    private static final int VIEW_TYPE_NULL = 1;

    private final Context mContext;
    private static Libraries libraries;


    public AdapterLibraries(Context mContext, Libraries libraries)
    {

        this.mContext = mContext;
        this.libraries = libraries;


    }

    @Override
    public int getItemCount()
    {
        return libraries.getLibraries().size();
    }

    @Override
    public int getItemViewType(int position)
    {
        if (position < libraries.getLibraries().size())
            return VIEW_TYPE_LIBRARY;
        else
            return VIEW_TYPE_NULL;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v;
        if (viewType == 0)
        {
            v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.fragment_item_library, parent, false);
            return new ViewHolderLibrary(v);
        }
        else
        {
            return null;
        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position)
    {
        if (holder instanceof ViewHolderLibrary)
        {
            ViewHolderLibrary vh = (ViewHolderLibrary) holder;
            String txtString = "";
            vh.name.setText((CharSequence) libraries.getLibraries().get(position).getNameLibrary());


            LoadImageStaticMap loadImageStaticMap = new LoadImageStaticMap(vh, position);
            loadImageStaticMap.execute();


            for (String opening : libraries.getLibraries().get(position).getItemLibrary().getOpeningHours())
            {
                txtString += opening + "<br/>";
            }

            txtString = txtString.substring(0, txtString.length() - 5);
            vh.openingHours.setText(Html.fromHtml(txtString));
            txtString = "";
            txtString = libraries.getLibraries().get(position).getItemLibrary().getAddress();
            vh.address.setText(Html.fromHtml(txtString));
            txtString = "";

            for (String opening : libraries.getLibraries().get(position).getItemLibrary().getMail())
                txtString += opening;

            vh.email.setText(mContext.getString(R.string.libraries_email));
            final String finalTxtStringEmail = txtString;
            vh.email.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    Intent emailIntent = new Intent(Intent.ACTION_SEND);
                    emailIntent.setType("text/html");
                    emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{finalTxtStringEmail});
                    mContext.startActivity(emailIntent);
                }
            });
            txtString = "";
            for (String opening : libraries.getLibraries().get(position).getItemLibrary().getSite())
                txtString += opening;

            vh.site.setText(mContext.getString(R.string.libraries_site));
            final String finalTxtStringSite = txtString;
            vh.site.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(finalTxtStringSite));
                    mContext.startActivity(browserIntent);
                }
            });

            txtString = "";
            for (String opening : libraries.getLibraries().get(position).getItemLibrary().getTelephone())
                txtString += opening + "<br/>";

            txtString = txtString.substring(0, txtString.length() - 5);
            vh.telephone.setText(Html.fromHtml(txtString));
        }


    }


    public static class ViewHolderLibrary extends RecyclerView.ViewHolder
    {

        TextView name;
        TextView openingHours;
        TextView email;
        TextView address;
        TextView telephone;
        TextView site;
        ImageView imageMap;

        public ViewHolderLibrary(View itemView)
        {
            super(itemView);
            this.name = (TextView) itemView.findViewById(R.id.name_library);
            this.imageMap = (ImageView) itemView.findViewById(R.id.imageView_map);
            this.openingHours = (TextView) itemView.findViewById(R.id.openingHours_library);
            this.email = (TextView) itemView.findViewById(R.id.email_library);
            this.address = (TextView) itemView.findViewById(R.id.address_library);
            this.telephone = (TextView) itemView.findViewById(R.id.telephone_library);
            this.site = (TextView) itemView.findViewById(R.id.site_library);

        }
    }

    private class LoadImageStaticMap extends AsyncTask<Bitmap, String, Bitmap>
    {

        ViewHolderLibrary viewHolderLibrary;
        int position;

        public LoadImageStaticMap(ViewHolderLibrary vh, int position)
        {
            this.viewHolderLibrary = vh;
            this.position = position;
        }

        @Override
        protected void onPostExecute(Bitmap bmp)
        {
            viewHolderLibrary.imageMap.setImageBitmap(bmp);

        }

        @Override
        protected Bitmap doInBackground(Bitmap... params)
        {

            Bitmap bm = HttpRequest.getStaticMap(createUriStaticMap());
            return bm;

        }

        private String createUriStaticMap()
        {
            DisplayDimension displayDimension = new DisplayDimension((Activity) mContext);

            int widthMap = displayDimension.getDeviceWidth();
            int heightMap = displayDimension.getDeviceHeight() / 3;
            Uri.Builder uri = new Uri.Builder();

            uri.scheme(HttpRequest.PARAM_MAP_SCHEME);
            uri.authority(HttpRequest.PARAM_MAP_AUTHORITY);
            uri.path(HttpRequest.PARAM_MAP_STATIC_PATH);
            uri.appendQueryParameter(HttpRequest.PARAM_MAP_CENTER,
                    libraries.getLibraries().get(position).getItemLibrary().getLatitude() + "," +
                    libraries.getLibraries().get(position).getItemLibrary().getLongitude());
            uri.appendQueryParameter(HttpRequest.PARAM_MAP_ZOOM, String.valueOf(16));
            uri.appendQueryParameter(HttpRequest.PARAM_MAP_SCALE, String.valueOf(2));
            uri.appendQueryParameter(HttpRequest.PARAM_MAP_SIZE,
                    String.valueOf(widthMap) + "x" + String.valueOf(heightMap));
            uri.appendQueryParameter(HttpRequest.PARAM_MAP_TYPE_LABEL, HttpRequest.PARAM_MAP_TYPE);
            uri.appendQueryParameter(HttpRequest.PARAM_MAP_SENSOR, String.valueOf(false));
            uri.appendQueryParameter(HttpRequest.PARAM_MAP_MARKERS, "|" +
                                                                    libraries.getLibraries().get(position).getItemLibrary().getLatitude() +
                                                                    "," +
                                                                    libraries.getLibraries().get(position).getItemLibrary().getLongitude());
            return uri.build().toString();
        }

    }

    ;
}
