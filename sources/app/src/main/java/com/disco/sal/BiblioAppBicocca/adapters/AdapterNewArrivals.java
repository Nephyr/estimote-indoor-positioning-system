package com.disco.sal.biblioappbicocca.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.disco.sal.biblioappbicocca.R;
import com.disco.sal.biblioappbicocca.activities.MapNewArrivalActivty;
import com.disco.sal.biblioappbicocca.models.NewArrival;

import java.util.ArrayList;

/**
 * Created by lorenzo on 07/05/15.
 */
public class AdapterNewArrivals extends ArrayAdapter<NewArrival>
{
    private final Context context;
    private final ArrayList<NewArrival> newArrivals;

    public AdapterNewArrivals(Context context, ArrayList<NewArrival> newArrivals)
    {

        super(context, R.layout.item_new_arrival, newArrivals);
        this.context = context;
        this.newArrivals = newArrivals;

    }

    @SuppressLint("ViewHolder")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.item_new_arrival, parent, false);
        TextView title = (TextView) rowView.findViewById(R.id.titleNewDocument);
        TextView year = (TextView) rowView.findViewById(R.id.yearNewDocument);
        TextView editore = (TextView) rowView.findViewById(R.id.editoreNewDocument);
        TextView argomento = (TextView) rowView.findViewById(R.id.argomentoNewDocuments);
        TextView library = (TextView) rowView.findViewById(R.id.libraryNewDocument);
        TextView location = (TextView) rowView.findViewById(R.id.locationNewDocument);
        ImageView imageLocation = (ImageView) rowView.findViewById(R.id.imageLocation);
        final NewArrival newArrival = getItem(position);
        title.setText(newArrival.getTitle());
        year.setText(Html.fromHtml("<strong>" + context.getString(R.string.year) + "</strong> " +
                                   newArrival.getYear()));
        editore.setText(Html.fromHtml(
                "<strong>" + context.getString(R.string.editor) + "</strong> " +
                newArrival.getEditor()));
        argomento.setText(Html.fromHtml(
                "<strong>" + context.getString(R.string.argument) + "</strong> " +
                newArrival.getArgument()));
        library.setText(newArrival.getLibrary());
        location.setText(newArrival.getLocation().trim());
        imageLocation.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(context, MapNewArrivalActivty.class);
                intent.putExtra("library", newArrival.getLibrary());
                context.startActivity(intent);
            }
        });

        return rowView;
    }

}
