package com.disco.sal.biblioappbicocca.models;

/**
 * Created by lorenzo on 28/04/15.
 */
public class News {
    private String title;
    private String date;
    private String id;
    private String tag;
    private String description;
    private String guid;
    private String link;

    public News(String title, String date, String id, String tag) {
        this.title = title;
        this.date = date;
        this.id = id;
        this.tag = tag;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }
}
