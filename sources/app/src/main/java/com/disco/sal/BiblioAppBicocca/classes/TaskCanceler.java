package com.disco.sal.biblioappbicocca.classes;

import android.os.AsyncTask;

import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.Region;

/**
 * Created by Marco on 23/01/2016.
 */
public class TaskCanceler implements Runnable
{
    private AsyncTask task;
    private BeaconManager beaconManager;
    private static final Region ALL_ESTIMOTE_BEACONS_REGION = new Region("rid", null, null, null);

    public TaskCanceler(AsyncTask task, BeaconManager beaconManager)
    {
        this.task = task;
        this.beaconManager = beaconManager;
    }

    @Override
    public void run()
    {
        if (task.getStatus() == AsyncTask.Status.RUNNING)
            task.cancel(true);
        beaconManager.stopRanging(ALL_ESTIMOTE_BEACONS_REGION);
    }
}