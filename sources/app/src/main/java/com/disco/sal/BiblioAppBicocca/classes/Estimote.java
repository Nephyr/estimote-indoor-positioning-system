package com.disco.sal.biblioappbicocca.classes;

import java.io.Serializable;

/**
 * Created by Marco on 15/01/2016.
 */
public class Estimote implements Serializable
{
    String macaddress;
    double[] XYs = new double[2];

    public Estimote(String macaddress, double[] XYs)
    {
        this.macaddress = macaddress;
        this.XYs[0] = XYs[0] / 10;
        this.XYs[1] = XYs[1] / 10;
    }

    public double[] getXYs()
    {
        return XYs;
    }

    public String getMacaddress()
    {
        return macaddress;
    }
}
