package com.disco.sal.biblioappbicocca.models;

/**
 * Created by lorenzo on 07/05/15.
 */
public class NewArrival {
    private String argument;
    private String arrivalDate;
    private String author;
    private String editor;
    private String library;
    private String location;
    private String title;
    private String url;
    private String year;
    private int image;

    public NewArrival(String argument, String arrivalDate, String author, String editor, String library, String location, String title, String url, String year, int image) {
        this.argument = argument;
        this.arrivalDate = arrivalDate;
        this.author = author;
        this.editor = editor;
        this.library = library;
        this.location = location;
        this.title = title;
        this.url = url;
        this.year = year;
        this.image = image;
    }

    public NewArrival(String argument, String arrivalDate, String author, String editor, String library, String location, String title, String url, String year) {
        this.argument = argument;
        this.arrivalDate = arrivalDate;
        this.author = author;
        this.editor = editor;
        this.library = library;
        this.location = location;
        this.title = title;
        this.url = url;
        this.year = year;
    }

    public String getArgument() {
        return argument;
    }

    public void setArgument(String argument) {
        this.argument = argument;
    }

    public String getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(String arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getEditor() {
        return editor;
    }

    public void setEditor(String editor) {
        this.editor = editor;
    }

    public String getLibrary() {
        return library;
    }

    public void setLibrary(String library) {
        this.library = library;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
