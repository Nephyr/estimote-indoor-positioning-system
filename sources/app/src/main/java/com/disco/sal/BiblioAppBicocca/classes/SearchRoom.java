package com.disco.sal.biblioappbicocca.classes;

import java.util.ArrayList;
import sal.sis.datatypes.EnumerativeContext;
import sal.sis.datatypes.NameSpaceParameters;
import sal.sis.datatypes.OrientedWeightedGraphParameters;
import sal.sis.datatypes.SpaceInformation;
import sal.sis.datatypes.WeightedEdge;
import sal.sis.exceptions.SISServiceException;
import sal.sis.interfaces.IMappingsInspector;
import sal.sis.interfaces.ISpaceInspector;

/**
 * Created by Matteo Vegini on 08/11/2015.
 * Modified by Marco Sgura on 05/02/2016
 */
public class SearchRoom
{
    //Configurazione SIS
    //SIS _sis;
    SISManager sis;
    ISpaceInspector spaceInspector;
    IMappingsInspector mappingsInspector;
    SpaceInformation info;
    NameSpaceParameters params_name;
    ArrayList<String> uni;
    ArrayList<WeightedEdge> listcollection;
    ArrayList<WeightedEdge> listsubargument;
    ArrayList<WeightedEdge> listaTesto;
    ArrayList<WeightedEdge> listargument;
    String[] search;
    String first;
    String second;
    String sublibrary;
    String location;
    String fondo;

    boolean t = false, flag = false;

    public SearchRoom(String sublibrary, String location, String fondo)
    {
        try
        {
            //_sis = SISServiceFactory.build("http://149.132.178.195:8080/SIS2ws-dev/", "141db2486934d803e1fcb5baa3dccdec");
            sis = SISManager.getSISManager();
            spaceInspector = sis.spaceInspector;
            mappingsInspector = sis.mappingsInspector;
            info = spaceInspector.getSpaceInformation("bambook.Libraries");
            params_name = (NameSpaceParameters) info.getParameters();
            uni = (ArrayList<String>) params_name.getNamesLocators();
            listsubargument = new ArrayList<WeightedEdge>();
            listaTesto = new ArrayList<WeightedEdge>();
            listcollection = new ArrayList<WeightedEdge>();
            listargument = new ArrayList<WeightedEdge>();
            search = new String[2];
        }
        catch (SISServiceException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        this.sublibrary = sublibrary;
        this.location = location;
        this.fondo = fondo;
    }

    // METODO MODIFICATO PER RIMOZIONE MAINACTIVITY.ROOM
    // Solo adesso è possibile utilizzare liberamente l'app
    // con mappature di biblioteche differenti su SIS (solo se presenti su db)!!
    public Room execute()
    {
        // if[ biblioteca NON è mappata in SIS ]:
        String u = sublibrary.replace(" ", "_");
        if (!uni.contains(u))
            return null;

        // else:
        setSearchTree();
        try
        {
            // Introduce troppi ritardi, il contains sopra è equivalente!
            //for (String u : uni)
            //{
                //if (u.compareTo(sublibrary.replace(" ", "_")) == 0)
                //{
                    info = spaceInspector.getSpaceInformation("bambook." + u + ".graph");
                    OrientedWeightedGraphParameters params_graf = (OrientedWeightedGraphParameters) info.getParameters();
                    ArrayList<WeightedEdge> listar = (ArrayList<WeightedEdge>) params_graf.getEdgeList();

                    for (WeightedEdge arco : listar)
                        if (arco.getFromNodeName().compareTo("root") == 0)
                        {
                            listcollection.add(arco);
                        }

                    for (WeightedEdge edge : listcollection)
                        if (edge.getToNodeName().compareTo(search[0]) == 0)
                        {
                            if (search[0].compareTo("CDD") == 0)
                            {
                                return searchTree("CDD", u, listar);
                            }
                            else if (search[0].compareTo("TESTO") == 0)
                            {
                                return searchTree("TESTO", u, listar);
                            }
                            else
                            {
                                EnumerativeContext ctxs = mappingsInspector.getForwardMapped("bambook." + u + ".graph", edge.getToNodeName(), "bambook.Shelves");
                                return sis.getObjectRoom(mappingsInspector.getMapped("bambook.Shelves", ctxs.getLocationNames().get(0)).get(0).getSpaceName().toString().split("\\.")[2], (ArrayList) ctxs.getLocationNames());
                            }
                        }
                //}
            //}
        }
        catch (SISServiceException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // Con exception meglio ritornare un NULL
        return null;
    }

    // METODO MODIFICATO PER RIMOZIONE CHECK MAPPE
    // Solo adesso è possibile utilizzare liberamente l'app
    // con mappature di biblioteche differenti su SIS (solo se presenti su db)!!
    public void setSearchTree()
    {
        // Non ha più senso poichè viene fatto in modo più furbo a monte!
        //if (sublibrary.compareTo("Bicocca Sede Centrale") == 0)
        //{
            if ((location.contains("CDD")) ||
                (location.contains("TESTO"))
               )
            {
                search = location.split(" ");
                if (search[1].contains("."))
                {
                    first = search[1].split("\\.")[0];
                    second = search[1].split("\\.")[1];
                }
            }
            /*if (location.contains("TESTO"))
            {
                search = location.split(" ");
                if (search[1].contains("."))
                {
                    first = search[1].split("\\.")[0];
                    second = search[1].split("\\.")[1];
                }
            }*/
            else
            {
                search[0] = location.split(" ")[0];   // periodico
            }
        //}
    }

    Room searchTree(String collection, String uni, ArrayList<WeightedEdge> edges)
    {
        try
        {
            for (WeightedEdge edge : edges)
                if (edge.getFromNodeName().compareTo(collection) == 0)
                {
                    listargument.add(edge);
                }

            for (WeightedEdge cd : listargument)
            {
                if (Integer.parseInt(cd.getToNodeName().split("_")[1]) >= Integer.parseInt(first))
                    t = true;

                if (t && Integer.parseInt(cd.getToNodeName().split("_")[1]) == Integer.parseInt(first))
                {
                    for (WeightedEdge edge : edges)
                        if (edge.getFromNodeName().compareTo(cd.getToNodeName()) == 0)
                            listsubargument.add(edge);

                    if (listsubargument.size() != 0)
                    {
                        for (WeightedEdge node : listsubargument)
                        {
                            if ((node.getToNodeName().split("_")[1].compareTo("END") == 0) ||
                                (Integer.parseInt(second) <= Integer.parseInt(node.getToNodeName().split("_")[1]))
                               )
                            /*{
                                EnumerativeContext ctxs = mappingsInspector.getForwardMapped("bambook." + uni + ".graph", node.getToNodeName(), "bambook.Shelves");
                                t = false;
                                flag = true;
                                return sis.getObjectRoom(mappingsInspector.getMapped("bambook.Shelves", ctxs.getLocationNames().get(0)).get(0).getSpaceName().toString().split("\\.")[2], (ArrayList) ctxs.getLocationNames());
                            }
                            else if (Integer.parseInt(second) <= Integer.parseInt(node.getToNodeName().split("_")[1]))*/
                            {
                                t    = false;
                                flag = true;
                                EnumerativeContext ctxs = mappingsInspector.getForwardMapped("bambook." + uni + ".graph", node.getToNodeName(), "bambook.Shelves");
                                return sis.getObjectRoom(mappingsInspector.getMapped("bambook.Shelves", ctxs.getLocationNames().get(0)).get(0).getSpaceName().toString().split("\\.")[2], (ArrayList) ctxs.getLocationNames());
                            }
                        }
                    }
                }
                else if (t && Integer.parseInt(cd.getToNodeName().split("_")[1]) > Integer.parseInt(first))
                {
                    listsubargument.clear();
                    for (WeightedEdge arco : edges)
                        if (arco.getFromNodeName().compareTo(cd.getToNodeName()) == 0)
                            listsubargument.add(arco);

                    t = false;
                    flag = true;
                    EnumerativeContext ctxs = mappingsInspector.getForwardMapped("bambook." + uni + ".graph", listsubargument.get(0).getToNodeName(), "bambook.Shelves");
                    return sis.getObjectRoom(mappingsInspector.getMapped("bambook.Shelves", ctxs.getLocationNames().get(0)).get(0).getSpaceName().toString().split("\\.")[2], (ArrayList) ctxs.getLocationNames());
                }

                if (flag) break;
            }
        }
        catch (SISServiceException e) {}
        return null;
    }
}

