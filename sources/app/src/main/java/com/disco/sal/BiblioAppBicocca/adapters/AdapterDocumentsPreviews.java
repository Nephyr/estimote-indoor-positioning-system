package com.disco.sal.biblioappbicocca.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.disco.sal.biblioappbicocca.R;
import com.disco.sal.biblioappbicocca.models.DocumentPreview;

import java.util.ArrayList;

/**
 * Created by lorenzo on 17/03/15.
 */
public class AdapterDocumentsPreviews extends ArrayAdapter<DocumentPreview>
{

    private final Context context;
    private final ArrayList<DocumentPreview> documentsPreviews;


    public AdapterDocumentsPreviews(Context context, ArrayList<DocumentPreview> documentsPreviews)
    {

        super(context, R.layout.item_document_preview, documentsPreviews);
        this.context = context;
        this.documentsPreviews = documentsPreviews;

    }

    public void add(DocumentPreview libro)
    {
        documentsPreviews.add(libro);
    }

    @SuppressLint("ViewHolder")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = rowView = inflater.inflate(R.layout.item_document_preview, parent, false);
        LinearLayout layout = (LinearLayout) rowView.findViewById(R.id.linear_layout_search);
        TextView title = (TextView) rowView.findViewById(R.id.title);
        TextView number = (TextView) rowView.findViewById(R.id.number);
        ImageView documentTypeImage = (ImageView) rowView.findViewById(R.id.documentTypeImage);
        DocumentPreview libro = getItem(position);
        if (!libro.getTitle().isEmpty())
        {
            title.setText(libro.getTitle());
        }
        else
        {
            title.setText(context.getString(R.string.no_title));
        }
        int numberInt = position + 1;
        number.setText(" " + numberInt + " ");

        //libro moderdo e libro antico
        if (libro.getType().equalsIgnoreCase("BK") || libro.getType().equalsIgnoreCase("AQ"))
        {
            documentTypeImage.setImageResource(R.drawable.ic_bk);
        }
        //periodici
        else if (libro.getType().equalsIgnoreCase("SE"))
        {
            documentTypeImage.setImageResource(R.drawable.ic_se);
        }
        //cartografia
        else if (libro.getType().equalsIgnoreCase("CM"))
        {
            documentTypeImage.setImageResource(R.drawable.ic_cm);
        }
        //materiale multimediale
        else if (libro.getType().equalsIgnoreCase("NB"))
        {
            documentTypeImage.setImageResource(R.drawable.ic_nb);
        }
        //film/documentari
        else if (libro.getType().equalsIgnoreCase("VM"))
        {
            documentTypeImage.setImageResource(R.drawable.ic_vm);
        }

        ArrayList<String> publications = libro.getPublication();
        for (String publication : publications)
        {
            TextView text = new TextView(context);
            text.setText(publication);
            text.setTextSize(14);
            text.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            layout.addView(text);
        }
        return rowView;
    }

}
