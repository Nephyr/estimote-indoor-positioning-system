package com.disco.sal.biblioappbicocca.classes;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.StrictMode;

import com.disco.sal.biblioappbicocca.R;
import com.disco.sal.biblioappbicocca.activities.MainActivity;
import com.disco.sal.biblioappbicocca.models.ChildParent;
import com.disco.sal.biblioappbicocca.models.Copy;
import com.disco.sal.biblioappbicocca.models.Document;
import com.disco.sal.biblioappbicocca.models.DocumentPreview;
import com.disco.sal.biblioappbicocca.models.Libraries;
import com.disco.sal.biblioappbicocca.models.NewArrival;
import com.disco.sal.biblioappbicocca.models.News;
import com.disco.sal.biblioappbicocca.models.SubFilter;
import com.disco.sal.biblioappbicocca.models.libraryitemdettails.ItemLibrary;
import com.disco.sal.biblioappbicocca.models.libraryitemdettails.Library;

import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Created by lorenzo on 14/03/15.
 */
public class HttpRequest extends MainActivity
{

    private static final String URLBASE = "http://monster6.disco.unimib.it/API/";
    private static final String SEARCH = "documents/search/?catalog=aleph&";
    private static final String NEWS = "http://monster6.disco.unimib.it/API/news";
    private static final String LIBARRIES = "http://monster6.disco.unimib.it/API/libraries";
    private static final String DOCUMENTNEWSARGUMENTBICOCCA = "http://monster6.disco.unimib.it/API/documents/new/argument?library=bicocca";
    private static final String DOCUMENTNEWSARGUMENTINSUBRIA = "http://monster6.disco.unimib.it/API/documents/new/argument?library=insubria";
    private static final String DOCUMENTNEWSARGUMENTALL = "http://monster6.disco.unimib.it/API/documents/new/argument?library=all";
    private static final String DOCUMENTNEWSSEATBICOCCA = "http://monster6.disco.unimib.it/API/documents/new/seat?library=bicocca";
    private static final String DOCUMENTNEWSSEATINSUBRIA = "http://monster6.disco.unimib.it/API/documents/new/seat?library=insubria";
    private static final String DOCUMENTNEWSSEATALL = "http://monster6.disco.unimib.it/API/documents/new/seat?library=all";
    private static final String DOCUMENTNEWSDISCIPLINARYBICOCCA = "http://monster6.disco.unimib.it/API/documents/new/disciplinaryArea?library=bicocca";
    private static final String DOCUMENTNEWSDISCIPLINARYINSUBRIA = "http://monster6.disco.unimib.it/API/documents/new/disciplinaryArea?library=insubria";
    private static final String DOCUMENTNEWSDISCIPLINARYALL = "http://monster6.disco.unimib.it/API/documents/new/disciplinaryArea?library=all";
    private static String session = " ";

    // Map
    public static final String PARAM_MAP_SCHEME = "http";
    public static final String PARAM_MAP_AUTHORITY = "maps.googleapis.com";
    public static final String PARAM_MAP_STATIC_PATH = "maps/api/staticmap";
    public static final String PARAM_MAP_SCALE = "scale";
    public static final String PARAM_MAP_SIZE = "size";
    public static final String PARAM_MAP_CENTER = "center";
    public static final String PARAM_MAP_ZOOM = "zoom";
    public static final String PARAM_MAP_TYPE_LABEL = "maptype";
    public static final String PARAM_MAP_TYPE = "roadmap";
    public static final String PARAM_MAP_SENSOR = "sensor";
    public static final String PARAM_MAP_MARKERS = "markers";
    public static final String PARAM_MAP_KEY = "key";

    private static String getResponseByLink(String link)
    {
        String result = "";
        InputStream is = null;
        try
        {

            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

            HttpParams httpParameters = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParameters, 10000);
            HttpConnectionParams.setSoTimeout(httpParameters, 10000);

            HttpClient httpclient = new DefaultHttpClient(httpParameters);
            HttpGet httpget = new HttpGet(link);
            HttpEntity entity = httpclient.execute(httpget).getEntity();
            is = entity.getContent();
        } catch (Exception e)
        {
            errorConnection = true;
            return "Errore nella connessione http ";
        }
        if (is != null)
        {
            //converto la risposta in stringa
            try
            {
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null)
                {
                    sb.append(line + "\n");
                }
                is.close();
                result = sb.toString();
            } catch (Exception e)
            {
                return "Errore nel convertire il risultato ";
            }
        }
        return result;
    }

    public static void getSimpleSearch(String query)
    {
        try
        {
            query = URLEncoder.encode(query, "utf-8");
            session = URLEncoder.encode(session, "utf-8");
            String paging = String.valueOf(MainActivity.paging);
            String offset = String.valueOf(MainActivity.offset);
            paging = URLEncoder.encode(paging, "utf-8");
            offset = URLEncoder.encode(offset, "utf-8");
            JSONObject jGeneralDocument = new JSONObject(getResponseByLink(
                    URLBASE + SEARCH + "s=" + query + "&paging=" + paging + "&offset=" + offset +
                    "&session=" + session));
            JSONObject jo = jGeneralDocument.getJSONObject("documentsAleph");
            if (jo.getString("status").equals("ok"))
            {
                session = jo.getString("session");
                JSONArray jsonArray = new JSONArray(jo.getString("documents"));
                for (int i = 0; i < jsonArray.length(); i++)
                {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    JSONArray ja = jsonObject.getJSONArray("author");
                    ArrayList<String> authors = new ArrayList<>();
                    JSONArray jp = jsonObject.getJSONArray("publication");
                    ArrayList<String> publications = new ArrayList<>();
                    JSONArray jc = jsonObject.getJSONArray("corporateBody");
                    ArrayList<String> corporateBodies = new ArrayList<>();

                    if (ja.length() != 0)
                    {
                        for (int j = 0; j < ja.length(); j++)
                        {
                            authors.add(ja.getString(j));
                        }
                    }

                    if (jc.length() != 0)
                    {
                        for (int j = 0; j < jc.length(); j++)
                        {
                            corporateBodies.add(jc.getString(j));
                        }
                    }
                    if (jp.length() != 0)
                    {
                        for (int j = 0; j < jp.length(); j++)
                        {
                            publications.add(jp.getString(j));
                        }
                    }
                    MainActivity.totalItem = jo.getString("totalItems");
                    MainActivity.documentsPreviews.add(new DocumentPreview(jsonObject.getString("title"), authors, corporateBodies, jsonObject.getString("url"), jsonObject.getString("type"), publications));
                }
            }
            else if (jo.getString("status").equals("offset out of bounds"))
            {
                MainActivity.offsetBound = true;
            }
            else if (jo.getString("status").equals("empty set"))
            {
                MainActivity.emptySet = true;
            }
            else if (jo.getString("status").equals("DB error"))
            {
                MainActivity.errorDb = true;
            }
            else if (jo.getString("status").equals("Error parsing find request [internal error code 31]"))
            {
                MainActivity.errorFind = true;
            }
            else if (jo.getString("status").equals("error") ||
                     jo.getString("status").equals("Too many hits. Refine your request [internal error code]"))
            {
                MainActivity.errorGeneric = true;
            }
            else if (jo.getString("status").equals("Aleph communication error"))
            {
                MainActivity.errorAlephComunication = true;
            }
            else
            {
                MainActivity.errorGeneric = true;
            }

        } catch (Exception e)
        {
            errorGeneric = true;
        }
    }

    public static ArrayList<News> getNews()
    {

        try
        {
            JSONObject jo = new JSONObject(getResponseByLink(NEWS));

            if (jo.getString("status").equals("ok"))
            {
                MainActivity.news = new ArrayList<>();
                JSONArray jsonArray = new JSONArray(jo.getString("news"));
                for (int i = 0; i < jsonArray.length(); i++)
                {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    String date = jsonObject.getString("date").substring(0, jsonObject.getString("date").indexOf("T"));
                    String[] splitDate;
                    splitDate = date.split("-");
                    date = splitDate[2] + "/" + splitDate[1] + "/" + splitDate[0];
                    MainActivity.news.add(new News(jsonObject.getString("title"), date, jsonObject.getString("url"), jsonObject.getString("tag")));

                }
            }
            else
            {
                MainActivity.errorGeneric = true;
            }
        } catch (Exception e)
        {
            MainActivity.errorGeneric = true;
        }
        return news;

    }

    public static void getSingleNews(String url, int position)
    {
        try
        {
            JSONObject jo = new JSONObject(getResponseByLink(url));

            if (jo.getString("status").equals("ok"))
            {
                JSONArray jsonArray = new JSONArray(jo.getString("news"));
                for (int i = 0; i < jsonArray.length(); i++)
                {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    MainActivity.news.get(position).setDescription(jsonObject.getString("description"));
                    MainActivity.news.get(position).setGuid(jsonObject.getString("guid"));
                    MainActivity.news.get(position).setLink(jsonObject.getString("link"));
                }
            }
            else
            {
                MainActivity.errorGeneric = true;

            }
        } catch (Exception e)
        {
            MainActivity.errorGeneric = true;
        }

    }

    public static void getSubFiltersByArgument(String typeSearchArguments)
    {
        try
        {
            JSONObject jo;
            switch (typeSearchArguments)
            {
                case "change1":
                    jo = new JSONObject(getResponseByLink(DOCUMENTNEWSARGUMENTBICOCCA));
                    break;
                case "change2":
                    jo = new JSONObject(getResponseByLink(DOCUMENTNEWSARGUMENTINSUBRIA));
                    break;
                case "change3":
                    jo = new JSONObject(getResponseByLink(DOCUMENTNEWSARGUMENTALL));
                    break;
                default:
                    jo = new JSONObject();

            }
            if (jo.get("status").equals("ok"))
            {
                if (typeSearchArguments.equals("change1") || typeSearchArguments.equals("change3"))
                {
                    MainActivity.subFiltersArgumentBicocca = new ArrayList<>();
                    JSONArray jsonArray = new JSONArray(jo.getString("bicoccaFilters"));
                    for (int i = 0; i < jsonArray.length(); i++)
                    {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        MainActivity.subFiltersArgumentBicocca.add(new SubFilter(jsonObject.getString("id"), jsonObject.getString("name"), jsonObject.getString("numberOfNew"), jsonObject.getString("url"), R.drawable.ic_bicocca));
                    }
                }
                if (typeSearchArguments.equals("change2") || typeSearchArguments.equals("change3"))
                {
                    MainActivity.subFiltersArgumentInsubria = new ArrayList<>();
                    JSONArray jsonArray = new JSONArray(jo.getString("insubriaFilters"));
                    for (int i = 0; i < jsonArray.length(); i++)
                    {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        MainActivity.subFiltersArgumentInsubria.add(new SubFilter(jsonObject.getString("id"), jsonObject.getString("name"), jsonObject.getString("numberOfNew"), jsonObject.getString("url"), R.drawable.ic_insubria));
                    }
                }

            }
            else
            {
                MainActivity.errorGeneric = true;
            }
        } catch (Exception e)
        {
            MainActivity.errorGeneric = true;
        }

    }

    public static void getSubFiltersBySeat(String typeSearchArguments)
    {
        try
        {
            JSONObject jo;
            switch (typeSearchArguments)
            {
                case "change1":
                    jo = new JSONObject(getResponseByLink(DOCUMENTNEWSSEATBICOCCA));
                    break;
                case "change2":
                    jo = new JSONObject(getResponseByLink(DOCUMENTNEWSSEATINSUBRIA));
                    break;
                case "change3":
                    jo = new JSONObject(getResponseByLink(DOCUMENTNEWSSEATALL));
                    break;
                default:
                    jo = new JSONObject();

            }
            if (jo.get("status").equals("ok"))
            {
                if (typeSearchArguments.equals("change1") || typeSearchArguments.equals("change3"))
                {
                    MainActivity.subFiltersSeatBicocca = new ArrayList<>();
                    JSONArray jsonArray = new JSONArray(jo.getString("bicoccaFilters"));
                    for (int i = 0; i < jsonArray.length(); i++)
                    {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        MainActivity.subFiltersSeatBicocca.add(new SubFilter(jsonObject.getString("id"), jsonObject.getString("name"), jsonObject.getString("numberOfNew"), jsonObject.getString("url"), R.drawable.ic_bicocca));
                    }
                }
                if (typeSearchArguments.equals("change2") || typeSearchArguments.equals("change3"))
                {
                    MainActivity.subFiltersSeatInsubria = new ArrayList<>();
                    JSONArray jsonArray = new JSONArray(jo.getString("insubriaFilters"));
                    for (int i = 0; i < jsonArray.length(); i++)
                    {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        MainActivity.subFiltersSeatInsubria.add(new SubFilter(jsonObject.getString("id"), jsonObject.getString("name"), jsonObject.getString("numberOfNew"), jsonObject.getString("url"), R.drawable.ic_insubria));
                    }
                }

            }
            else
                MainActivity.errorGeneric = true;
        } catch (Exception e)
        {
            MainActivity.errorGeneric = true;
        }

    }

    public static void getSubFiltersByDisciplinary(String typeSearchDisciplinaryArea)
    {
        try
        {
            JSONObject jo;
            switch (typeSearchDisciplinaryArea)
            {
                case "change1":
                    jo = new JSONObject(getResponseByLink(DOCUMENTNEWSDISCIPLINARYBICOCCA));
                    break;
                case "change2":
                    jo = new JSONObject(getResponseByLink(DOCUMENTNEWSDISCIPLINARYINSUBRIA));
                    break;
                case "change3":
                    jo = new JSONObject(getResponseByLink(DOCUMENTNEWSDISCIPLINARYALL));
                    break;
                default:
                    jo = new JSONObject();

            }
            if (jo.get("status").equals("ok"))
            {
                if (typeSearchDisciplinaryArea.equals("change1") ||
                    typeSearchDisciplinaryArea.equals("change3"))
                {
                    MainActivity.subFiltersDisciplinaryAreaBicocca = new ArrayList<>();
                    JSONArray jsonArray = new JSONArray(jo.getString("bicoccaFilters"));
                    for (int i = 0; i < jsonArray.length(); i++)
                    {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        MainActivity.subFiltersDisciplinaryAreaBicocca.add(new SubFilter(jsonObject.getString("id"), jsonObject.getString("name"), jsonObject.getString("numberOfNew"), jsonObject.getString("url"), R.drawable.ic_bicocca));
                    }
                }
                if (typeSearchDisciplinaryArea.equals("change2") ||
                    typeSearchDisciplinaryArea.equals("change3"))
                {
                    MainActivity.subFiltersDisciplinaryAreaInsubria = new ArrayList<>();
                    JSONArray jsonArray = new JSONArray(jo.getString("insubriaFilters"));
                    for (int i = 0; i < jsonArray.length(); i++)
                    {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        MainActivity.subFiltersDisciplinaryAreaInsubria.add(new SubFilter(jsonObject.getString("id"), jsonObject.getString("name"), jsonObject.getString("numberOfNew"), jsonObject.getString("url"), R.drawable.ic_insubria));
                    }
                }

            }
            else
                MainActivity.errorGeneric = true;
        } catch (Exception e)
        {
            MainActivity.errorGeneric = true;
        }
    }

    public static void getNewArrivals(String url)
    {
        MainActivity.newArrivals = new ArrayList<>();
        JSONObject jo = null;
        try
        {
            jo = new JSONObject(getResponseByLink(url));
        } catch (JSONException e)
        {
            e.printStackTrace();
        }
        JSONArray jsonArray = null;
        try
        {
            if (jo.get("status").equals("ok"))
            {
                try
                {
                    jsonArray = new JSONArray(jo.getString("newDocuments"));
                } catch (JSONException e)
                {
                    e.printStackTrace();
                }
                for (int i = 0; i < jsonArray.length(); i++)
                {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    MainActivity.newArrivals.add(new NewArrival(jsonObject.getString("argument"), jsonObject.getString("arrivalDate"), jsonObject.getString("author"), jsonObject.getString("editor"), jsonObject.getString("library"), jsonObject.getString("location"), jsonObject.getString("title"), jsonObject.getString("url"), jsonObject.getString("year")));
                }
            }
            else
            {
                MainActivity.errorGeneric = true;
            }
        } catch (Exception e)
        {
            MainActivity.errorGeneric = true;
        }

    }

    public static void getFilterSearch()
    {
        String queryTot = "";
        try
        {
            queryTot += URLBASE + SEARCH;
            if (MainActivity.titolo != null && !MainActivity.titolo.isEmpty())
            {
                MainActivity.titolo = URLEncoder.encode(MainActivity.titolo, "utf-8");
                queryTot += "&title=" + MainActivity.titolo;
            }
            if (MainActivity.autore != null && !MainActivity.autore.isEmpty())
            {
                MainActivity.autore = URLEncoder.encode(MainActivity.autore, "utf-8");
                queryTot += "&author=" + MainActivity.autore;
            }
            if (MainActivity.editore != null && !MainActivity.editore.isEmpty())
            {
                MainActivity.editore = URLEncoder.encode(MainActivity.editore, "utf-8");
                queryTot += "&publisher=" + MainActivity.editore;
            }
            if (MainActivity.luogo != null && !MainActivity.luogo.isEmpty())
            {
                MainActivity.luogo = URLEncoder.encode(MainActivity.luogo, "utf-8");
                queryTot += "&publication_place=" + MainActivity.luogo;
            }
            if (MainActivity.codice != null && !MainActivity.codice.isEmpty())
            {
                MainActivity.codice = URLEncoder.encode(MainActivity.codice, "utf-8");
                queryTot += "&code=" + MainActivity.codice;
            }
            if (MainActivity.materiale != null && !MainActivity.materiale.isEmpty() &&
                !MainActivity.materiale.equals("Tutti"))
            {
                MainActivity.materiale = URLEncoder.encode(MainActivity.materiale, "utf-8");
                queryTot += "&type=" + MainActivity.materiale;
            }
            if (MainActivity.lingua != null && !MainActivity.lingua.isEmpty() &&
                !MainActivity.lingua.equals("Tutte"))
            {
                MainActivity.lingua = URLEncoder.encode(MainActivity.lingua, "utf-8");
                queryTot += "&language=" + MainActivity.lingua;
            }
            if (MainActivity.anno != null && !MainActivity.anno.isEmpty() &&
                !MainActivity.anno.equals("Tutti"))
            {
                MainActivity.anno = URLEncoder.encode(MainActivity.anno, "utf-8");
                queryTot += "&year=" + MainActivity.anno;
            }

            queryTot += "&paging=" + MainActivity.paging;
            queryTot += "&offset=" + MainActivity.offset;
            queryTot += "&sesssion=" + URLEncoder.encode(session, "utf-8");
            JSONObject jGeneralDocument = new JSONObject(getResponseByLink(queryTot));
            JSONObject jo = jGeneralDocument.getJSONObject("documentsAleph");
            if (jo.getString("status").equals("ok"))
            {
                session = jo.getString("session");
                JSONArray jsonArray = new JSONArray(jo.getString("documents"));
                for (int i = 0; i < jsonArray.length(); i++)
                {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    JSONArray ja = jsonObject.getJSONArray("author");
                    ArrayList<String> authors = new ArrayList<>();
                    JSONArray jp = jsonObject.getJSONArray("publication");
                    ArrayList<String> publications = new ArrayList<>();
                    JSONArray jc = jsonObject.getJSONArray("corporateBody");
                    ArrayList<String> corporateBodies = new ArrayList<>();

                    if (ja.length() != 0)
                    {
                        for (int j = 0; j < ja.length(); j++)
                        {
                            authors.add(ja.getString(j));
                        }
                    }

                    if (jc.length() != 0)
                    {
                        for (int j = 0; j < jc.length(); j++)
                        {
                            corporateBodies.add(jc.getString(j));
                        }
                    }
                    if (jp.length() != 0)
                    {
                        for (int j = 0; j < jp.length(); j++)
                        {
                            publications.add(jp.getString(j));
                        }
                    }
                    MainActivity.totalItem = jo.getString("totalItems");
                    MainActivity.documentsPreviews.add(new DocumentPreview(jsonObject.getString("title"), authors, corporateBodies, jsonObject.getString("url"), jsonObject.getString("type"), publications));
                }
            }
            else if (jo.getString("status").equals("offset out of bounds"))
            {
                MainActivity.offsetBound = true;
            }
            else if (jo.getString("status").equals("empty set"))
            {
                MainActivity.emptySet = true;
            }
            else if (jo.getString("status").equals("DB error"))
            {
                MainActivity.errorDb = true;
            }
            else if (jo.getString("status").equals("Error parsing find request [internal error code 31]"))
            {
                MainActivity.errorFind = true;
            }
            else if (jo.getString("status").equals("error"))
            {
                MainActivity.errorGeneric = true;
            }
            else if (jo.getString("status").equals("Aleph communication error"))
            {
                MainActivity.errorAlephComunication = true;
            }
            else if (jo.getString("status").equals("provide valid search parameters"))
            {
                MainActivity.errorValidSearchParameters = true;
            }
            else
                MainActivity.errorGeneric = true;
        } catch (Exception e)
        {
            MainActivity.errorGeneric = true;
        }
    }

    public static void getDocument(String url)
    {
        //Inizializzo variabili
        ArrayList<String> CDD = new ArrayList<>();
        ArrayList<String> author = new ArrayList<>();
        ArrayList<ChildParent> child = new ArrayList<>();
        ArrayList<String> collection = new ArrayList<>();
        ArrayList<String> contentNote = new ArrayList<>();
        ArrayList<String> corporateBody = new ArrayList<>();
        ArrayList<String> edition = new ArrayList<>();
        ArrayList<String> generalNote = new ArrayList<>();
        ArrayList<String> holdings = new ArrayList<>();
        ArrayList<String> isbn = new ArrayList<>();
        ArrayList<String> issn = new ArrayList<>();
        ArrayList<ChildParent> parent = new ArrayList<>();
        ArrayList<String> physicalDescription = new ArrayList<>();
        ArrayList<String> publication = new ArrayList<>();
        String thumbnail;
        String title;
        String type;
        String url_this;

        ArrayList<Copy> copies = new ArrayList<>();
        try
        {
            if (MainActivity.cover)
            {
                url += "/?cover=yes";
                url += "&session=" + URLEncoder.encode(session, "utf-8");
            }
            else
                url += "/?session=" + URLEncoder.encode(session, "utf-8");
            JSONObject jo = new JSONObject(getResponseByLink(url));
            if (jo.getString("status").equals("ok"))
            {
                session = jo.getString("session");
                JSONArray jsonArray = new JSONArray(jo.getString("documents"));
                JSONObject jsonObject = jsonArray.getJSONObject(0);
                JSONArray cddJS = jsonObject.getJSONArray("CDD");
                if (cddJS.length() != 0)
                    for (int i = 0; i < cddJS.length(); i++)
                        CDD.add(cddJS.getString(i));

                JSONArray authorJS = jsonObject.getJSONArray("author");
                if (authorJS.length() != 0)
                    for (int i = 0; i < authorJS.length(); i++)
                        author.add(authorJS.getString(i));

                JSONArray childJS = jsonObject.getJSONArray("child");
                if (childJS.length() != 0)
                {
                    for (int i = 0; i < childJS.length(); i++)
                    {
                        JSONObject jsonChild = childJS.getJSONObject(i);
                        ChildParent childParent = new ChildParent(jsonChild.getString("title"), jsonChild.getString("url"));
                        child.add(childParent);
                    }

                }

                JSONArray collectionJS = jsonObject.getJSONArray("collection");
                if (collectionJS.length() != 0)
                    for (int i = 0; i < collectionJS.length(); i++)
                        collection.add(collectionJS.getString(i));
                JSONArray contentNoteJS = jsonObject.getJSONArray("contentNote");
                if (contentNoteJS.length() != 0)
                    for (int i = 0; i < contentNoteJS.length(); i++)
                        contentNote.add(contentNoteJS.getString(i));

                JSONArray corporateBodyJS = jsonObject.getJSONArray("corporateBody");
                if (corporateBodyJS.length() != 0)
                    for (int i = 0; i < corporateBodyJS.length(); i++)
                        corporateBody.add(corporateBodyJS.getString(i));

                JSONArray editionJS = jsonObject.getJSONArray("edition");
                if (editionJS.length() != 0)
                    for (int i = 0; i < editionJS.length(); i++)
                        edition.add(editionJS.getString(i));

                JSONArray generalNoteJS = jsonObject.getJSONArray("generalNote");
                if (generalNoteJS.length() != 0)
                    for (int i = 0; i < generalNoteJS.length(); i++)
                        generalNote.add(generalNoteJS.getString(i));
                JSONArray holdingsJS = jsonObject.getJSONArray("holdings");
                if (holdingsJS.length() != 0)
                    for (int i = 0; i < holdingsJS.length(); i++)
                        holdings.add(holdingsJS.getString(i));

                JSONArray isbnJS = jsonObject.getJSONArray("isbn");
                if (isbnJS.length() != 0)
                    for (int i = 0; i < isbnJS.length(); i++)
                        isbn.add(isbnJS.getString(i));

                JSONArray issnJS = jsonObject.getJSONArray("issn");
                if (issnJS.length() != 0)
                    for (int i = 0; i < issnJS.length(); i++)
                        issn.add(issnJS.getString(i));

                JSONObject parentJsOBj = jsonObject.getJSONObject("parent");
                if (parentJsOBj.length() != 0)
                {
                    ChildParent childParent = new ChildParent(parentJsOBj.getString("title"), parentJsOBj.getString("url"));
                    parent.add(childParent);
                }

                JSONArray physicalDescriptionJS = jsonObject.getJSONArray("physicalDescription");
                if (physicalDescriptionJS.length() != 0)
                    for (int i = 0; i < physicalDescriptionJS.length(); i++)
                        physicalDescription.add(physicalDescriptionJS.getString(i));

                JSONArray publicationJS = jsonObject.getJSONArray("publication");
                if (publicationJS.length() != 0)
                    for (int i = 0; i < publicationJS.length(); i++)
                        publication.add(publicationJS.getString(i));

                thumbnail = jsonObject.getString("thumbnail");
                title = jsonObject.getString("title");
                type = jsonObject.getString("type");
                url_this = jsonObject.getString("url");

                MainActivity.document = new Document(CDD, author, child, collection, contentNote, corporateBody, edition, generalNote,
                        holdings, isbn, issn, parent, physicalDescription, publication, thumbnail, type, title, url, copies);

            }
            else
                MainActivity.errorGeneric = true;

        } catch (Exception e)
        {
            MainActivity.errorGeneric = true;
        }


    }

    public static void getCopies(String url)
    {
        try
        {
            url += "/loans/?session=" + session;
            JSONObject jo = new JSONObject(getResponseByLink(url));
            if (jo.getString("status").equalsIgnoreCase("ok"))
            {
                JSONArray jsonArray = new JSONArray(jo.getString("loans"));
                for (int i = 0; i < jsonArray.length(); i++)
                {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    Copy copies = new Copy(jsonObject.getString("barcode"), jsonObject.getString("collection"), jsonObject.getString("dueDate"), jsonObject.getString("loanStatus"), jsonObject.getString("location"), jsonObject.getString("subLibrary"));
                    MainActivity.document.getCopieses().add(copies);
                }
            }
            else
                MainActivity.errorGeneric = true;
        } catch (Exception e)
        {
            MainActivity.errorGeneric = true;
        }
    }

    public static void getLibraries(Libraries libraries)
    {
        JSONObject jo = null;
        ItemLibrary itemLibrary;
        JSONArray jsonArray = null;
        try
        {
            jo = new JSONObject(getResponseByLink(LIBARRIES));
        } catch (JSONException e)
        {
            e.printStackTrace();
        }

        try
        {
            if (jo.get("status").equals("ok"))
            {
                try
                {
                    jsonArray = new JSONArray(jo.getString("libraries"));
                } catch (JSONException e)
                {
                    e.printStackTrace();
                }
                for (int i = 0; i < jsonArray.length(); i++)
                {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    itemLibrary = getItemLibrary(jsonObject.getString("url"));
                    libraries.getLibraries().add(new Library(jsonObject.getString("name"), jsonObject.getString("url"), itemLibrary));
                }
            }
            else
            {
                MainActivity.errorGeneric = true;
            }
        } catch (Exception e)
        {
            MainActivity.errorGeneric = true;
        }
    }

    private static ItemLibrary getItemLibrary(String url)
    {
        JSONObject jo = null;
        ItemLibrary itemLibrary = null;
        JSONArray jsonArray = null;
        try
        {
            jo = new JSONObject(getResponseByLink(url));
        } catch (JSONException e)
        {
            e.printStackTrace();
        }

        try
        {
            if (jo.get("status").equals("ok"))
            {
                try
                {

                    jsonArray = new JSONArray(jo.getString("libraries"));
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    itemLibrary = new ItemLibrary();
                    itemLibrary.setAddress(jsonObject.getString("address"));
                    itemLibrary.setLatitude(jsonObject.getString("latitude"));
                    itemLibrary.setLongitude(jsonObject.getString("longitude"));

                    JSONArray mailJS = jsonObject.getJSONArray("mail");
                    if (mailJS.length() != 0)
                        for (int i = 0; i < mailJS.length(); i++)
                            itemLibrary.getMail().add(mailJS.getString(i));

                    JSONArray openingHoursJS = jsonObject.getJSONArray("openingHours");
                    if (openingHoursJS.length() != 0)
                        for (int i = 0; i < openingHoursJS.length(); i++)
                            itemLibrary.getOpeningHours().add(openingHoursJS.getString(i));

                    JSONArray siteJS = jsonObject.getJSONArray("site");
                    if (siteJS.length() != 0)
                        for (int i = 0; i < siteJS.length(); i++)
                            itemLibrary.getSite().add(siteJS.getString(i));

                    JSONArray telephoneJS = jsonObject.getJSONArray("telephone");
                    if (telephoneJS.length() != 0)
                        for (int i = 0; i < telephoneJS.length(); i++)
                            itemLibrary.getTelephone().add(telephoneJS.getString(i));

                } catch (JSONException e)
                {
                    e.printStackTrace();
                }

            }
            else
            {
                MainActivity.errorGeneric = true;
            }
        } catch (Exception e)
        {
            MainActivity.errorGeneric = true;
        }
        return itemLibrary;
    }

    public static Bitmap getStaticMap(String url)
    {

        Bitmap bmp = null;
        HttpClient httpclient = new DefaultHttpClient();
        HttpGet request = new HttpGet(url);

        InputStream in = null;
        try
        {
            in = httpclient.execute(request).getEntity().getContent();
            bmp = BitmapFactory.decodeStream(in);
            in.close();
        } catch (IllegalStateException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ClientProtocolException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return bmp;

    }

}

