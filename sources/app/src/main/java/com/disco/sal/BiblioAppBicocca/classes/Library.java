package com.disco.sal.biblioappbicocca.classes;

import java.util.ArrayList;

/**
 * Created by Marco on 01/02/2016.
 */
public class Library
{
    private String name;
    private ArrayList<Room> rooms = new ArrayList<Room>();

    public Library(String name)
    {
        this.name = name;
    }

    public ArrayList<Room> getRooms()
    {
        return rooms;
    }
}
