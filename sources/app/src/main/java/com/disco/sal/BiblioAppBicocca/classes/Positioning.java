package com.disco.sal.biblioappbicocca.classes;

/**
 * Created by Marco on 31/01/2016.
 */
public interface Positioning
{
    double[] positioning(double[][] centers, double[] distances);
}
