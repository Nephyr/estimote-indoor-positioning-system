package com.disco.sal.biblioappbicocca.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.disco.sal.biblioappbicocca.R;
import com.disco.sal.biblioappbicocca.classes.HttpRequest;
import com.disco.sal.biblioappbicocca.models.ChildParent;
import com.disco.sal.biblioappbicocca.models.Copy;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;

public class DocumentDetailsActivity extends ActionBarActivity
{

    private Toolbar toolbar;
    private String url;
    TextView title;
    ImageView cover;
    ImageView arrowUpDown;
    ImageView arrowUpDownInfo;
    ImageView arrowUpDownNote;
    ImageView arrowUpDownChild;
    ImageView arrowUpDownParent;
    ImageView arrowUpDownCopies;
    Boolean viewCopies = false;
    CardView cardViewAuthors;
    LinearLayout linearAuthors;
    CardView cardViewinfo;
    LinearLayout linearInfo;
    CardView cardViewNote;
    LinearLayout linearNote;
    CardView cardViewChild;
    LinearLayout linearChild;
    CardView cardViewParent;
    LinearLayout linearParent;
    CardView cardViewCopies;
    LinearLayout linearCopies;
    Activity context;
    String location = "";


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_document);
        MainActivity.resetError();
        this.context = this;
        Intent intent = getIntent();
        url = intent.getStringExtra("url");
        if (url != null && !url.isEmpty())
        {
            cover = (ImageView) findViewById(R.id.imageCover);
            arrowUpDown = (ImageView) findViewById(R.id.arrow);
            arrowUpDownInfo = (ImageView) findViewById(R.id.arrowInfo);
            arrowUpDownNote = (ImageView) findViewById(R.id.arrowNote);
            arrowUpDownChild = (ImageView) findViewById(R.id.arrowChild);
            arrowUpDownParent = (ImageView) findViewById(R.id.arrowParent);
            arrowUpDownCopies = (ImageView) findViewById(R.id.arrowCopie);
            linearAuthors = (LinearLayout) findViewById(R.id.linearAuthor);

            linearAuthors.setVisibility(View.GONE);
            cardViewAuthors = (CardView) findViewById(R.id.card_view_autori);
            cardViewAuthors.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    if (linearAuthors.getVisibility() == View.GONE)
                    {
                        arrowUpDown.setImageDrawable(getResources().getDrawable(R.drawable.ic_arrow_drop_up_black_24dp));
                        linearAuthors.setVisibility(View.VISIBLE);
                    }
                    else
                    {
                        arrowUpDown.setImageDrawable(getResources().getDrawable(R.drawable.ic_arrow_drop_down_black_24dp));
                        linearAuthors.setVisibility(View.GONE);
                    }

                }
            });

            linearInfo = (LinearLayout) findViewById(R.id.linearInfo);
            linearInfo.setVisibility(View.GONE);
            cardViewinfo = (CardView) findViewById(R.id.card_view_info);
            cardViewinfo.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    if (linearInfo.getVisibility() == View.GONE)
                    {
                        arrowUpDownInfo.setImageDrawable(getResources().getDrawable(R.drawable.ic_arrow_drop_up_black_24dp));
                        linearInfo.setVisibility(View.VISIBLE);
                    }
                    else
                    {
                        arrowUpDownInfo.setImageDrawable(getResources().getDrawable(R.drawable.ic_arrow_drop_down_black_24dp));
                        linearInfo.setVisibility(View.GONE);
                    }

                }
            });

            linearNote = (LinearLayout) findViewById(R.id.linearNote);
            linearNote.setVisibility(View.GONE);
            cardViewNote = (CardView) findViewById(R.id.card_view_note);
            cardViewNote.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    if (linearNote.getVisibility() == View.GONE)
                    {
                        arrowUpDownNote.setImageDrawable(getResources().getDrawable(R.drawable.ic_arrow_drop_up_black_24dp));
                        linearNote.setVisibility(View.VISIBLE);
                    }
                    else
                    {
                        arrowUpDownNote.setImageDrawable(getResources().getDrawable(R.drawable.ic_arrow_drop_down_black_24dp));
                        linearNote.setVisibility(View.GONE);
                    }

                }
            });

            linearChild = (LinearLayout) findViewById(R.id.linearChild);
            linearChild.setVisibility(View.GONE);
            cardViewChild = (CardView) findViewById(R.id.card_view_child);
            cardViewChild.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    if (linearChild.getVisibility() == View.GONE)
                    {
                        arrowUpDownChild.setImageDrawable(getResources().getDrawable(R.drawable.ic_arrow_drop_up_black_24dp));
                        linearChild.setVisibility(View.VISIBLE);
                    }
                    else
                    {
                        arrowUpDownChild.setImageDrawable(getResources().getDrawable(R.drawable.ic_arrow_drop_down_black_24dp));
                        linearChild.setVisibility(View.GONE);
                    }

                }
            });

            linearParent = (LinearLayout) findViewById(R.id.linearParent);
            linearParent.setVisibility(View.GONE);
            cardViewParent = (CardView) findViewById(R.id.card_view_parent);
            cardViewParent.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    if (linearParent.getVisibility() == View.GONE)
                    {
                        arrowUpDownParent.setImageDrawable(getResources().getDrawable(R.drawable.ic_arrow_drop_up_black_24dp));
                        linearParent.setVisibility(View.VISIBLE);
                    }
                    else
                    {
                        arrowUpDownParent.setImageDrawable(getResources().getDrawable(R.drawable.ic_arrow_drop_down_black_24dp));
                        linearParent.setVisibility(View.GONE);
                    }

                }
            });

            linearCopies = (LinearLayout) findViewById(R.id.linearCopie);
            linearCopies.setVisibility(View.GONE);
            cardViewCopies = (CardView) findViewById(R.id.card_view_copie);
            cardViewCopies.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    if (linearCopies.getVisibility() == View.GONE)
                    {
                        arrowUpDownCopies.setImageDrawable(getResources().getDrawable(R.drawable.ic_arrow_drop_up_black_24dp));
                        if (MainActivity.document.getCopieses().size() == 0)
                        {
                            LoadCopies loadCopies = new LoadCopies();
                            loadCopies.execute();
                        }
                        linearCopies.setVisibility(View.VISIBLE);
                    }
                    else
                    {
                        arrowUpDownCopies.setImageDrawable(getResources().getDrawable(R.drawable.ic_arrow_drop_down_black_24dp));
                        linearCopies.setVisibility(View.GONE);
                    }

                }
            });

            if (MainActivity.document == null)
            {
                LoadDocument loadDocument = new LoadDocument(this);
                loadDocument.execute();
            }
            else
            {
                setCard();
            }

        }
        // TODO: 28/06/15 getstire errore
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case android.R.id.home:
                finish();

        }
        return super.onOptionsItemSelected(item);
    }

    public class LoadDocument extends AsyncTask<String, String, String>
    {

        private ProgressDialog progressDialog = null;

        public LoadDocument(Activity context)
        {

        }

        @Override
        protected void onPostExecute(String result)
        {
            progressDialog.dismiss();
            toolbar = (Toolbar) findViewById(R.id.toolbargeneric);
            setSupportActionBar(toolbar);
            if (MainActivity.checkError())
            {
                setCard();
            }
            else
            {
                cardViewAuthors.setVisibility(View.GONE);
                cardViewChild.setVisibility(View.GONE);
                cardViewCopies.setVisibility(View.GONE);
                cardViewinfo.setVisibility(View.GONE);
                cardViewNote.setVisibility(View.GONE);
                cardViewParent.setVisibility(View.GONE);
                ImageView img = (ImageView) findViewById(R.id.imageCover);
                img.setVisibility(View.GONE);
                if (MainActivity.serverDown)
                {
                    TextView txt;
                    View lay = findViewById(R.id.empty);
                    lay.setVisibility(View.VISIBLE);
                    txt = (TextView) lay.findViewById(R.id.emptyDocument);
                    txt.setText(getString(R.string.connection_error));
                }
                else
                {
                    TextView txt;
                    View lay = findViewById(R.id.empty);
                    lay.setVisibility(View.VISIBLE);
                    txt = (TextView) lay.findViewById(R.id.emptyDocument);
                    txt.setText(getString(R.string.generic_error));
                }
                MainActivity.resetError();
            }
        }

        @Override
        protected String doInBackground(String... params)
        {
            if (isOnline())
            {
                HttpRequest.getDocument(url);
            }
            else
            {
                MainActivity.serverDown = true;
            }
            return "Finish";

        }

        @Override
        protected void onPreExecute()
        {
            progressDialog = ProgressDialog.show(context, getString(R.string.download_alert_title), getString(R.string.document_details_download_alert_text), true);
            progressDialog.setCancelable(false);
        }

        @Override
        protected void onProgressUpdate(String... values)
        {
        }


    }

    public class LoadImage extends AsyncTask<String, String, String>
    {

        Activity context;
        private ProgressDialog progressDialog = null;
        Bitmap bitmap;

        public LoadImage(Activity context)
        {
            this.context = context;
        }

        @Override
        protected void onPostExecute(String result)
        {
            progressDialog.dismiss();
            cover.setImageBitmap(bitmap);

        }

        @Override
        protected String doInBackground(String... params)
        {
            URL newurl = null;
            try
            {
                newurl = new URL(MainActivity.document.getThumbnail());
                bitmap = BitmapFactory.decodeStream(newurl.openConnection().getInputStream());

            } catch (MalformedURLException e)
            {
                e.printStackTrace();
            } catch (IOException e)
            {
                e.printStackTrace();
            }
            return "Finish";

        }

        @Override
        protected void onPreExecute()
        {
            progressDialog = ProgressDialog.show(context, getString(R.string.download_alert_title), getString(R.string.image_download_alert_text), true);
            progressDialog.setCancelable(false);
        }

        @Override
        protected void onProgressUpdate(String... values)
        {
        }


    }

    public class LoadCopies extends AsyncTask<String, String, String>
    {

        private ProgressDialog progressDialog = null;

        public LoadCopies()
        {

        }

        @Override
        protected void onPostExecute(String result)
        {
            final float scale = getResources().getDisplayMetrics().density;
            final int padding_8_px = (int) (8 * scale + 0.5f);
            final int padding_2_px = (int) (2 * scale + 0.5f);
            progressDialog.dismiss();
            linearCopies.removeAllViews();
            if (MainActivity.checkError())
            {
                setCopy();
                viewCopies = true;
            }
            else
            {
                TextView txt = new TextView(context);
                txt.setTextSize(16);
                txt.setPadding(padding_8_px, padding_2_px, padding_8_px, padding_2_px);
                txt.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                if (MainActivity.serverDown)
                {
                    txt.setText(getString(R.string.connection_error));
                }
                else if (!MainActivity.checkError())
                {
                    //non vi è un errore specifico per la mancanza delle copie, ma se si arriva qua molto probilmente il documento non possiede copie
                    //txt.setText(getString(R.string.generic_error));
                    txt.setText(getString(R.string.error_copy));
                }
                linearCopies.addView(txt);
                MainActivity.resetError();

            }
        }

        @Override
        protected String doInBackground(String... params)
        {
            if (isOnline())
                HttpRequest.getCopies(url);
            else
                MainActivity.serverDown = true;
            return "Finish";

        }

        @Override
        protected void onPreExecute()
        {
            progressDialog = ProgressDialog.show(context, getString(R.string.download_alert_title), getString(R.string.copies_download_alert_text), true);
            progressDialog.setCancelable(false);
        }

        @Override
        protected void onProgressUpdate(String... values)
        {
        }


    }

    public void setCard()
    {
        final float scale = getResources().getDisplayMetrics().density;
        final int padding_8_px = (int) (8 * scale + 0.5f);
        final int padding_2_px = (int) (2 * scale + 0.5f);
        title = (TextView) findViewById(R.id.title);
        title.setText(MainActivity.document.getTitle());
        if (!MainActivity.document.getAuthor().isEmpty() ||
            !MainActivity.document.getCorporateBody().isEmpty())
        {
            for (String author : MainActivity.document.getAuthor())
            {
                TextView text = new TextView(context);
                text.setText(author);
                text.setTextSize(16);
                text.setPadding(padding_8_px, padding_2_px, padding_8_px, padding_2_px);
                text.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                linearAuthors.addView(text);
            }
            for (String corpore : MainActivity.document.getCorporateBody())
            {
                TextView text = new TextView(context);
                text.setText(corpore);
                text.setTextSize(16);
                text.setPadding(padding_8_px, padding_2_px, padding_8_px, padding_2_px);
                text.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                linearAuthors.addView(text);
            }
        }
        else
        {
            cardViewAuthors.setVisibility(View.GONE);
        }
        if (!MainActivity.document.getPublication().isEmpty() ||
            !MainActivity.document.getPhysicalDescription().isEmpty() ||
            !MainActivity.document.getCollection().isEmpty() ||
            !MainActivity.document.getIsbn().isEmpty() ||
            !MainActivity.document.getCDD().isEmpty() ||
            !MainActivity.document.getEdition().isEmpty())
        {
            String stringText;
            if (!MainActivity.document.getPublication().isEmpty())
            {
                TextView textPublication = new TextView(context);
                stringText = "<strong>" + context.getString(R.string.publication) + "</strong> ";
                for (String publication : MainActivity.document.getPublication())
                {
                    stringText = stringText + publication + "<br/>";
                }
                //rimuovo ultimo a capo
                stringText = stringText.substring(0, stringText.length() - 5);
                textPublication.setText(Html.fromHtml(stringText));
                textPublication.setTextSize(16);
                textPublication.setPadding(padding_8_px, padding_2_px, padding_8_px, padding_2_px);
                textPublication.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                linearInfo.addView(textPublication);
            }
            if (!MainActivity.document.getPhysicalDescription().isEmpty())
            {
                TextView textPhysicalDescription = new TextView(context);
                stringText = "<strong>" + context.getString(R.string.physical_description) +
                             "</strong> ";
                for (String physicalDescription : MainActivity.document.getPhysicalDescription())
                {
                    stringText = stringText + physicalDescription + "<br/>";
                }
                //rimuovo ultimo a capo
                stringText = stringText.substring(0, stringText.length() - 5);
                textPhysicalDescription.setText(Html.fromHtml(stringText));
                textPhysicalDescription.setTextSize(16);
                textPhysicalDescription.setPadding(padding_8_px, padding_2_px, padding_8_px, padding_2_px);
                textPhysicalDescription.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                linearInfo.addView(textPhysicalDescription);
            }
            if (!MainActivity.document.getCollection().isEmpty())
            {
                TextView textCollection = new TextView(context);
                stringText =
                        "<strong>" + context.getString(R.string.collection_info) + "</strong> ";
                for (String collection : MainActivity.document.getCollection())
                {
                    stringText = stringText + collection + "<br/>";
                }
                //rimuovo ultimo a capo
                stringText = stringText.substring(0, stringText.length() - 5);
                textCollection.setText(Html.fromHtml(stringText));
                textCollection.setTextSize(16);
                textCollection.setPadding(padding_8_px, padding_2_px, padding_8_px, padding_2_px);
                textCollection.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                linearInfo.addView(textCollection);
            }
            if (!MainActivity.document.getIsbn().isEmpty())
            {
                TextView textISBN = new TextView(context);
                stringText = "<strong>" + context.getString(R.string.isbn) + "</strong> ";
                for (String isbn : MainActivity.document.getIsbn())
                {
                    stringText = stringText + isbn + "<br/>";
                }
                //rimuovo ultimo a capo
                stringText = stringText.substring(0, stringText.length() - 5);
                textISBN.setText(Html.fromHtml(stringText));
                textISBN.setTextSize(16);
                textISBN.setPadding(padding_8_px, padding_2_px, padding_8_px, padding_2_px);
                textISBN.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                linearInfo.addView(textISBN);
            }
            if (!MainActivity.document.getIssn().isEmpty())
            {
                TextView textISSN = new TextView(context);
                stringText = "<strong>" + context.getString(R.string.issn) + "</strong> ";
                for (String issn : MainActivity.document.getIssn())
                {
                    stringText = stringText + issn + "<br/>";
                }
                //rimuovo ultimo a capo
                stringText = stringText.substring(0, stringText.length() - 5);
                textISSN.setText(Html.fromHtml(stringText));
                textISSN.setTextSize(16);
                textISSN.setPadding(padding_8_px, padding_2_px, padding_8_px, padding_2_px);
                textISSN.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                linearInfo.addView(textISSN);
            }
            if (!MainActivity.document.getEdition().isEmpty())
            {
                TextView textEdition = new TextView(context);
                stringText = "<strong>" + context.getString(R.string.edition) + "</strong> ";
                for (String edition : MainActivity.document.getEdition())
                {
                    stringText = stringText + edition + "<br/>";
                }
                //rimuovo ultimo a capo
                stringText = stringText.substring(0, stringText.length() - 5);
                textEdition.setText(Html.fromHtml(stringText));
                textEdition.setTextSize(16);
                textEdition.setPadding(padding_8_px, padding_2_px, padding_8_px, padding_2_px);
                textEdition.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                linearInfo.addView(textEdition);
            }
            if (!MainActivity.document.getCDD().isEmpty())
            {
                TextView textCDD = new TextView(context);
                stringText = "<strong>" + context.getString(R.string.cdd) + "</strong> ";
                for (String cdd : MainActivity.document.getCDD())
                {
                    stringText = stringText + cdd + "<br/>";
                }
                //rimuovo ultimo a capo
                stringText = stringText.substring(0, stringText.length() - 5);
                textCDD.setText(Html.fromHtml(stringText));
                textCDD.setTextSize(16);
                textCDD.setPadding(padding_8_px, padding_2_px, padding_8_px, padding_2_px);
                textCDD.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                linearInfo.addView(textCDD);
            }
        }
        else
        {
            cardViewinfo.setVisibility(View.GONE);
        }

        if (!MainActivity.document.getGeneralNote().isEmpty() ||
            !MainActivity.document.getHoldings().isEmpty())
        {

            for (String general : MainActivity.document.getGeneralNote())
            {
                TextView text = new TextView(context);
                text.setText(general);
                text.setTextSize(16);
                text.setPadding(padding_8_px, padding_2_px, padding_8_px, padding_2_px);
                text.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                linearNote.addView(text);
            }

            for (String holding : MainActivity.document.getHoldings())
            {
                TextView text = new TextView(context);
                text.setText(holding);
                text.setTextSize(16);
                text.setPadding(padding_8_px, padding_2_px, padding_8_px, padding_2_px);
                text.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                linearNote.addView(text);
            }

        }
        else
        {
            cardViewNote.setVisibility(View.GONE);
        }
        if (!MainActivity.document.getChild().isEmpty())
        {
            for (int i = 0; i < MainActivity.document.getChild().size(); i++)
            {
                ChildParent childParent = MainActivity.document.getChild().get(i);
                linearChild.addView(childOrParentLink(childParent.getTitle(), childParent.getUrl()));
            }
        }
        else
        {
            cardViewChild.setVisibility(View.GONE);
        }
        if (!MainActivity.document.getParent().isEmpty())
        {
            for (int i = 0; i < MainActivity.document.getParent().size(); i++)
            {
                ChildParent childParent = MainActivity.document.getParent().get(i);
                linearParent.addView(childOrParentLink(childParent.getTitle(), childParent.getUrl()));

            }
        }
        else
        {
            cardViewParent.setVisibility(View.GONE);
        }
        if (!MainActivity.document.getThumbnail().isEmpty())
        {
            LoadImage loadImage = new LoadImage(context);
            loadImage.execute();
        }
        else
        {
            //libro moderdo e libro antico
            if (MainActivity.document.getType().equalsIgnoreCase("BK") ||
                MainActivity.document.getType().equalsIgnoreCase("AQ"))
            {
                cover.setImageResource(R.drawable.ic_bk);
            }
            //periodici
            else if (MainActivity.document.getType().equalsIgnoreCase("SE"))
            {
                cover.setImageResource(R.drawable.ic_se);
            }
            //cartografia
            else if (MainActivity.document.getType().equalsIgnoreCase("CM"))
            {
                cover.setImageResource(R.drawable.ic_cm);
            }
            //materiale multimediale
            else if (MainActivity.document.getType().equalsIgnoreCase("NB"))
            {
                cover.setImageResource(R.drawable.ic_nb);
            }
            //film/documentari
            else if (MainActivity.document.getType().equalsIgnoreCase("VM"))
            {
                cover.setImageResource(R.drawable.ic_vm);
            }
        }
    }

    public void setCopy()
    {
        Copy copies;
        int countCopy = 1;
        ArrayList<String> library = new ArrayList<>();
        for (int i = 0; i < MainActivity.document.getCopieses().size(); i++)
        {
            copies = MainActivity.document.getCopieses().get(i);
            library.add(copies.getSubLibrary());

        }
        Collections.sort(library);
        location = library.get(0);
        for (int i = 1; i < library.size(); i++)
        {

            if (!library.get(i).equalsIgnoreCase(location))
            {
                linearCopies.addView(copiesLink(location, countCopy));
                countCopy = 1;
                location = library.get(i);

            }
            else
                countCopy++;
        }
        linearCopies.addView(copiesLink(location, countCopy));
    }

    public boolean isOnline()
    {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    private View copiesLink(String library, int count)
    {
        final float scale = getResources().getDisplayMetrics().density;
        final int padding_8_px = (int) (8 * scale + 0.5f);
        final int padding_2_px = (int) (2 * scale + 0.5f);
        LinearLayout LL = new LinearLayout(this);
        LL.setOrientation(LinearLayout.HORIZONTAL);
        ViewGroup.LayoutParams LLParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        LL.setLayoutParams(LLParams);
        ImageView imageViewArrow = new ImageView(this);
        LinearLayout.LayoutParams imageParmas = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        imageParmas.gravity = Gravity.CENTER;
        imageViewArrow.setImageResource(R.drawable.ic_chevron_right_black_24dp);
        imageViewArrow.setLayoutParams(imageParmas);
        TextView text = new TextView(context);
        text.setText(Html.fromHtml(
                "<strong>" + library + "</strong><br>" + getString(R.string.number_of_copies) +
                " " + count));
        text.setTextSize(16);
        text.setPadding(padding_8_px, padding_2_px, padding_8_px, padding_8_px);
        final String libraryStatic = library;
        LinearLayout.LayoutParams textParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        text.setLayoutParams(textParams);
        LL.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(context, CopiesDetailsActivity.class);
                intent.putExtra("library", libraryStatic);
                intent.putExtra("urlDocument", url);
                viewCopies = false;
                context.startActivity(intent);
            }
        });
        LL.addView(text);
        LL.addView(imageViewArrow);
        return LL;
    }

    private View childOrParentLink(String title, String url)
    {
        final float scale = getResources().getDisplayMetrics().density;
        final int padding_8_px = (int) (8 * scale + 0.5f);
        final int padding_2_px = (int) (2 * scale + 0.5f);
        LinearLayout LL = new LinearLayout(this);
        LL.setOrientation(LinearLayout.HORIZONTAL);
        ViewGroup.LayoutParams LLParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        LL.setLayoutParams(LLParams);
        ImageView imageViewArrow = new ImageView(this);
        LinearLayout.LayoutParams imageParmas = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        imageParmas.gravity = Gravity.CENTER;
        imageParmas.weight = (float) 0.1;
        imageViewArrow.setImageResource(R.drawable.ic_chevron_right_black_24dp);
        imageViewArrow.setLayoutParams(imageParmas);
        TextView text = new TextView(context);
        text.setText(title);
        text.setTextSize(16);
        text.setPadding(padding_8_px, padding_2_px, padding_8_px, padding_8_px);
        final String urlFinal = url;
        LinearLayout.LayoutParams textParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        textParams.weight = (float) 0.9;
        text.setLayoutParams(textParams);
        LL.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                MainActivity.document = null;
                Intent intent = new Intent(context, DocumentDetailsActivity.class);
                intent.putExtra("url", urlFinal);
                context.startActivity(intent);

            }
        });
        LL.addView(text);
        LL.addView(imageViewArrow);
        return LL;
    }

    @Override
    public void onResume()
    {
        //previene bug consultazione documento genitore senza copie
        arrowUpDownCopies.setImageDrawable(getResources().getDrawable(R.drawable.ic_arrow_drop_down_black_24dp));
        linearCopies.setVisibility(View.GONE);
        super.onResume();
    }
}