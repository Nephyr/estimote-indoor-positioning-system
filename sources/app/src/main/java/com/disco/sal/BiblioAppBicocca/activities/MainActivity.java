package com.disco.sal.biblioappbicocca.activities;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.disco.sal.biblioappbicocca.R;
import com.disco.sal.biblioappbicocca.adapters.AdapterDrawer;
import com.disco.sal.biblioappbicocca.classes.IntentIntegrator;
import com.disco.sal.biblioappbicocca.classes.IntentResult;
import com.disco.sal.biblioappbicocca.fragments.InfoFragment;
import com.disco.sal.biblioappbicocca.fragments.LibraryFragment;
import com.disco.sal.biblioappbicocca.fragments.ListDocumentsFromAlpehFragment;
import com.disco.sal.biblioappbicocca.fragments.NewsFragment;
import com.disco.sal.biblioappbicocca.fragments.ScanFragment;
import com.disco.sal.biblioappbicocca.fragments.SearchFragment;
import com.disco.sal.biblioappbicocca.fragments.SubFiltersFragment;
import com.disco.sal.biblioappbicocca.models.Document;
import com.disco.sal.biblioappbicocca.models.DocumentPreview;
import com.disco.sal.biblioappbicocca.models.DrawerItem;
import com.disco.sal.biblioappbicocca.models.NewArrival;
import com.disco.sal.biblioappbicocca.models.News;
import com.disco.sal.biblioappbicocca.models.SubFilter;

import java.util.ArrayList;

//Versione app 102
public class MainActivity extends ActionBarActivity
{

    private DrawerLayout mDrawerLayout;
    private ListView mNavDrawerList;
    private LinearLayout mNavDrawerContainer;
    private ActionBarDrawerToggle mDrawerToggle;
    private Handler mHandler = new Handler();
    private int mSelectedNavDrawerItem;
    private CharSequence mDrawerTitle;
    private ArrayList<DrawerItem> navDrawerItems;
    private AdapterDrawer adapter;
    protected static Toolbar toolbar = null;
    private MenuItem searchMenuItem;
    private CharSequence toolbarTitle;

    public static ArrayList<DocumentPreview> documentsPreviews = null;
    public static String totalItem = null;
    public static Document document = null;
    public static String query = null;
    public static String queryPrec = null;
    public static int paging = 12;
    public static int offset = 1;
    public static boolean cover = true;
    public static ArrayList<News> news = null;
    public static boolean viewNewArrivals = false;

    //Variabili errori
    public static boolean serverDown = false;
    public static boolean offsetBound = false;
    public static boolean emptySet = false;
    public static boolean errorFind = false;
    public static boolean errorDb = false;
    public static boolean errorGeneric = false;
    public static boolean errorAlephComunication = false;
    public static boolean errorValidSearchParameters = false;
    public static boolean errorConnection = false;

    //Variabili nuovi arrivi
    //valore dello spinner con i filtri per in nuovi arrivi
    public static String valueSpinner = "";
    //indice dello spinner con i filtri per i nuovi arrivi
    public static int indexSpinner = 0;
    public static String typeSearchArguments = "";
    public static String typeSearchSeat = "";
    public static String typeSearchDisciplinary = "";
    //public static ArrayList<String> typeSearchArgumentsPrec = new ArrayList<>();
    //public static ArrayList<String> typeSearchSeatprec = new ArrayList<>();
    //public static ArrayList<String> typeSearchDisciplinaryAreaPrec = new ArrayList<>();
    public static ArrayList<SubFilter> subFiltersArgumentBicocca = null;
    public static ArrayList<SubFilter> subFiltersArgumentInsubria = null;
    public static ArrayList<SubFilter> subFiltersArgumentAll = null;
    public static ArrayList<SubFilter> subFiltersSeatBicocca = null;
    public static ArrayList<SubFilter> subFiltersSeatInsubria = null;
    public static ArrayList<SubFilter> subFiltersSeatAll = null;
    public static ArrayList<SubFilter> subFiltersDisciplinaryAreaBicocca = null;
    public static ArrayList<SubFilter> subFiltersDisciplinaryAreaInsubria = null;
    public static ArrayList<SubFilter> subFiltersDisciplinaryAreaAll = null;
    public static ArrayList<NewArrival> newArrivals = new ArrayList<>();

    //Variabili per search filter
    public static String titolo = null;
    public static String autore = null;
    public static String editore = null;
    public static String luogo = null;
    public static String codice = null;
    public static String materiale = null;
    public static String anno = null;
    public static String lingua = null;

    //Variabili SIS
    //public static Room room = null;
    //public static Pair<String, Room> beaconCurrentPair = null;
    //public static Map<String,ArrayList<Room> > cacheRooms = null;

    public static Boolean flagScan = false;
    public static Boolean flagAdvancedSearch = false;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        valueSpinner = (getResources().getStringArray(R.array.newArrivalsFilterSpinner))[0];
        typeSearchArguments = (getResources().getStringArray(R.array.new_arrivals_entries))[0];
        typeSearchSeat = (getResources().getStringArray(R.array.new_arrivals_entries))[0];
        typeSearchDisciplinary = (getResources().getStringArray(R.array.new_arrivals_entries))[0];

        //setto preferenze di default
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);

        MainActivity.paging = sharedPref.getInt(getString(R.string.key_paging), 12);
        MainActivity.cover = sharedPref.getBoolean(getString(R.string.key_cover), true);
        MainActivity.typeSearchArguments = sharedPref.getString(getString(R.string.key_new_arrivals_argument), getResources().getStringArray(R.array.new_arrivals_values)[0]);
        MainActivity.typeSearchSeat = sharedPref.getString(getString(R.string.key_new_arrivals_seat), getResources().getStringArray(R.array.new_arrivals_values)[0]);
        MainActivity.typeSearchDisciplinary = sharedPref.getString(getString(R.string.key_new_arrivals_seat), getResources().getStringArray(R.array.new_arrivals_values)[0]);

        toolbar = (Toolbar) findViewById(R.id.toolbargeneric);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mNavDrawerList = (ListView) findViewById(R.id.navigation_drawer_list);
        mNavDrawerContainer = (LinearLayout) findViewById(R.id.navigation_drawer);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        //getSupportActionBar().setIcon(R.mipmap.ic_launcher);

        //settaggio lista drawer
        navDrawerItems = new ArrayList<>();
        navDrawerItems.add(new DrawerItem(getString(R.string.navigation_drawer_search), R.drawable.ic_search_white_24dp));
        navDrawerItems.add(new DrawerItem(getString(R.string.navigation_drawer_new_arrivals), R.drawable.ic_new_arrivals));
        navDrawerItems.add(new DrawerItem(getString(R.string.navigation_drawer_library), R.drawable.ic_local_library_white_24dp));
        navDrawerItems.add(new DrawerItem(getString(R.string.navigation_drawer_news), R.drawable.ic_calendar_white));
        navDrawerItems.add(new DrawerItem(getString(R.string.navigation_drawer_scan), R.drawable.ic_scan));
        navDrawerItems.add(new DrawerItem(getString(R.string.navigation_drawer_setting), R.drawable.ic_setting_white));
        navDrawerItems.add(new DrawerItem(getString(R.string.navigation_drawer_info), R.drawable.ic_info_outline_white_24dp));
        adapter = new AdapterDrawer(this, R.layout.item_list_drawer, navDrawerItems);
        View header = getLayoutInflater().inflate(R.layout.nav_drawer_header, null, false);
        mNavDrawerList.addHeaderView(header);
        mNavDrawerList.setAdapter(adapter);
        mNavDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                displayView(position);
            }
        });


        if (savedInstanceState != null)
        {
            toolbarTitle = savedInstanceState.getCharSequence("TITLE_TOOLBAR");
            mDrawerTitle = savedInstanceState.getCharSequence("TITLE_DRAWER");
            setTitle(toolbarTitle);
            mSelectedNavDrawerItem = savedInstanceState.getInt("NAV_DRAWER_SELECTED_ITEM");
        }
        else
        {
            Intent intent = getIntent();
            if (!Intent.ACTION_SEARCH.equals(intent.getAction()))
            {
                toolbarTitle = mDrawerTitle = "";
                displayView(1);
            }
        }


        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.app_name, R.string.app_name)
        {
            public void onDrawerClosed(View view)
            {
                getSupportActionBar().setTitle(toolbarTitle);
                // calling onPrepareOptionsMenu() to show action bar icons
                invalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView)
            {
                getSupportActionBar().setTitle(mDrawerTitle);
                // calling onPrepareOptionsMenu() to hide action bar icons
                invalidateOptionsMenu();
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        handleIntent(getIntent());
    }

    //metodo chiamato quando si clicca su un elemento delle lista
    private void displayView(int position)
    {
        reset();
        if (position == 1)
        {
            setTitle(R.string.search_title_actionbar);
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.container, SearchFragment.newInstance(), "tag_fragment_search")
                    .commit();
            mNavDrawerList.setItemChecked(1, true);
            mNavDrawerList.setSelection(1);
        }
        else if (position == 2)
        {
            setTitle(R.string.navigation_drawer_new_arrivals);
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.container, SubFiltersFragment.newInstance(), "tag_fragment_new_arrivals")
                    .commit();
            mNavDrawerList.setItemChecked(2, true);
            mNavDrawerList.setSelection(2);

        }
        else if (position == 3)
        {
            setTitle(R.string.navigation_drawer_library);
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.container, LibraryFragment.newInstance(), "tag_fragment_library")
                    .commit();
            mNavDrawerList.setItemChecked(3, true);
            mNavDrawerList.setSelection(3);
        }
        else if (position == 4)
        {
            setTitle(R.string.navigation_drawer_news);
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.container, NewsFragment.newInstance(), "tag_fragment_news")
                    .commit();
            mNavDrawerList.setItemChecked(4, true);
            mNavDrawerList.setSelection(4);
        }
        else if (position == 5)
        {
            setTitle(R.string.navigation_drawer_scan);
            IntentIntegrator scanIntegrator = new IntentIntegrator(this);
            scanIntegrator.initiateScan();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.container, ScanFragment.newInstance(), "tag_fragment_scan")
                    .commit();
            mNavDrawerList.setItemChecked(5, true);
            mNavDrawerList.setSelection(5);
        }
        else if (position == 6)
        {
            startActivity(new Intent(this, SettingsActivity.class));
        }
        else if (position == 7)
        {
            setTitle(R.string.navigation_drawer_info);
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.container, InfoFragment.newInstance(), "tag_fragment_info")
                    .commit();
            mNavDrawerList.setItemChecked(7, true);
            mNavDrawerList.setSelection(7);

        }

        //permette che il contenuto si carichi prima che il drawer si nasconda
        mHandler.postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                mDrawerLayout.closeDrawer(mNavDrawerContainer);
            }
        }, 250);


    }

    @Override
    public void setTitle(CharSequence title)
    {
        toolbarTitle = title;
        getSupportActionBar().setTitle(toolbarTitle);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        outState.putCharSequence("TITLE_TOOLBAR", toolbarTitle);
        outState.putCharSequence("TITLE_DRAWER", mDrawerTitle);
        outState.putInt("NAV_DRAWER_SELECTED_ITEM", mSelectedNavDrawerItem);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState)
    {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig)
    {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onNewIntent(Intent intent)
    {
        handleIntent(intent);
    }

    private void handleIntent(Intent intent)
    {
        if (Intent.ACTION_SEARCH.equals(intent.getAction()))
        {
            String query = intent.getStringExtra(SearchManager.QUERY);
            reset();
            MainActivity.query = new String(query);
            if (intent.getStringExtra(SearchManager.QUERY) != null)
            {
                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.container, ListDocumentsFromAlpehFragment.newInstance(MainActivity.query), "tag_fragment_listDocuments")
                        .commit();
            }
        }
    }

    public static void reset()
    {
        MainActivity.documentsPreviews = null;
        MainActivity.offset = 1;
        MainActivity.serverDown = false;
        MainActivity.offsetBound = false;
        MainActivity.emptySet = false;
        MainActivity.errorFind = false;
        MainActivity.errorDb = false;
        MainActivity.errorGeneric = false;
        MainActivity.errorAlephComunication = false;
        MainActivity.errorValidSearchParameters = false;
        MainActivity.errorConnection = false;
        MainActivity.titolo = null;
        MainActivity.autore = null;
        MainActivity.editore = null;
        MainActivity.luogo = null;
        MainActivity.codice = null;
        MainActivity.materiale = null;
        MainActivity.anno = null;
        MainActivity.lingua = null;
    }

    public static void resetError()
    {
        MainActivity.serverDown = false;
        MainActivity.offsetBound = false;
        MainActivity.emptySet = false;
        MainActivity.errorFind = false;
        MainActivity.errorDb = false;
        MainActivity.errorGeneric = false;
        MainActivity.errorAlephComunication = false;
        MainActivity.errorValidSearchParameters = false;
        MainActivity.errorConnection = false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.main, menu);
        this.searchMenuItem = menu.findItem(R.id.search_item);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.search_item).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        // Toggle nav drawer on selecting action bar app icon/title
        if (mDrawerToggle.onOptionsItemSelected(item))
        {
            return true;
        }
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent)
    {
        IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
        //retrieve scan result
        if (scanningResult != null)
        {
            String scanContent = scanningResult.getContents();
            reset();
            MainActivity.query = scanContent;
            if (scanContent != null)
                flagScan = true;
        }
        else
        {
            Toast toast = Toast.makeText(getApplicationContext(), getString(R.string.no_scan_data), Toast.LENGTH_SHORT);
            toast.show();
        }
    }


    @Override
    public boolean onPrepareOptionsMenu(Menu menu)
    {
        // if nav drawer is opened, hide the action items
        boolean drawerOpen = mDrawerLayout.isDrawerOpen(mNavDrawerContainer);
        ListDocumentsFromAlpehFragment listDocumentsFromAlpehFragment = (ListDocumentsFromAlpehFragment) getSupportFragmentManager().findFragmentByTag("tag_fragment_listDocuments");
        if (listDocumentsFromAlpehFragment != null && listDocumentsFromAlpehFragment.isVisible())
        {
            menu.findItem(R.id.search_item).setVisible(!drawerOpen);
        }
        else
        {
            menu.findItem(R.id.search_item).setVisible(false);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onResume()
    {
        if (flagScan)
        {
            flagScan = false;
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.container, ListDocumentsFromAlpehFragment.newInstance(MainActivity.query))
                    .commitAllowingStateLoss();

        }
        if (flagAdvancedSearch)
        {
            flagAdvancedSearch = false;
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.container, ListDocumentsFromAlpehFragment.newInstance(MainActivity.query))
                    .commit();

        }
        NewsFragment newsFragment = (NewsFragment) getSupportFragmentManager().findFragmentByTag("tag_fragment_news");
        SubFiltersFragment subFiltersFragment = (SubFiltersFragment) getSupportFragmentManager().findFragmentByTag("tag_fragment_new_arrivals");
        InfoFragment infoFragment = (InfoFragment) getSupportFragmentManager().findFragmentByTag("tag_fragment_info");
        LibraryFragment libraryFragment = (LibraryFragment) getSupportFragmentManager().findFragmentByTag("tag_fragment_library");
        ScanFragment scanFragment = (ScanFragment) getSupportFragmentManager().findFragmentByTag("tag_fragment_scan");
        SearchFragment searchFragment = (SearchFragment) getSupportFragmentManager().findFragmentByTag("tag_fragment_search");
        if (searchFragment != null && searchFragment.isVisible())
        {
            mNavDrawerList.setItemChecked(1, true);
            mNavDrawerList.setSelection(1);
        }
        else if (subFiltersFragment != null && subFiltersFragment.isVisible())
        {
            mNavDrawerList.setItemChecked(2, true);
            mNavDrawerList.setSelection(2);

        }
        else if (libraryFragment != null && libraryFragment.isVisible())
        {
            mNavDrawerList.setItemChecked(3, true);
            mNavDrawerList.setSelection(3);
        }
        else if (newsFragment != null && newsFragment.isVisible())
        {
            mNavDrawerList.setItemChecked(4, true);
            mNavDrawerList.setSelection(4);
        }
        else if (scanFragment != null && scanFragment.isVisible())
        {
            mNavDrawerList.setItemChecked(5, true);
            mNavDrawerList.setSelection(5);
        }
        else if (infoFragment != null && infoFragment.isVisible())
        {
            mNavDrawerList.setItemChecked(6, true);
            mNavDrawerList.setSelection(6);
        }
        super.onResume();
    }

    @Override
    public void onPause()
    {
        super.onPause();
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
    }

    public static boolean checkError()
    {
        if (!MainActivity.serverDown && !MainActivity.offsetBound && !MainActivity.errorFind &&
            !MainActivity.emptySet && !MainActivity.errorDb &&
            !MainActivity.errorValidSearchParameters && !MainActivity.errorGeneric &&
            !MainActivity.errorAlephComunication)
            return true;
        else
            return false;
    }


}