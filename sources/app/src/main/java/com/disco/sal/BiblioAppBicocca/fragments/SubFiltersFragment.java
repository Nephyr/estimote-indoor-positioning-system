package com.disco.sal.biblioappbicocca.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.disco.sal.biblioappbicocca.R;
import com.disco.sal.biblioappbicocca.activities.MainActivity;
import com.disco.sal.biblioappbicocca.activities.NewArrivalsActivity;
import com.disco.sal.biblioappbicocca.adapters.AdapterSubFilters;
import com.disco.sal.biblioappbicocca.classes.HttpRequest;
import com.disco.sal.biblioappbicocca.models.SubFilter;

import java.util.ArrayList;

public class SubFiltersFragment extends Fragment
{

    Spinner spinnerFilters;
    public ListView listViewSubFilters;
    public AdapterSubFilters adapterSubFilters;
    String valueSpinner;
    View myInflatedView;

    public static SubFiltersFragment newInstance()
    {
        SubFiltersFragment fragment = new SubFiltersFragment();
        return fragment;
    }

    public SubFiltersFragment()
    {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        myInflatedView = inflater.inflate(R.layout.fragment_sub_filters, container, false);
        listViewSubFilters = (ListView) myInflatedView.findViewById(R.id.listViewSubFilters);
        listViewSubFilters.setEmptyView(myInflatedView.findViewById(R.id.empty));
        spinnerFilters = (Spinner) myInflatedView.findViewById(R.id.spinnerFilters);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(), R.array.newArrivalsFilterSpinner, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerFilters.setAdapter(adapter);
        spinnerFilters.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                MainActivity.indexSpinner = position;
                valueSpinner = parent.getItemAtPosition(position).toString();
                MainActivity.valueSpinner = valueSpinner;
                MainActivity.reset();
                adapterSubFilters = new AdapterSubFilters(getActivity(), new ArrayList<SubFilter>());
                listViewSubFilters.setAdapter(adapterSubFilters);
                loadOrNot();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent)
            {

            }
        });
        MainActivity.valueSpinner = valueSpinner;
        spinnerFilters.setSelection(MainActivity.indexSpinner);
        return myInflatedView;
    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        return super.onOptionsItemSelected(item);
    }

    public class loadSubFilers extends AsyncTask<String, String, String>
    {

        Activity context;
        private ProgressDialog progressDialog = null;

        public loadSubFilers(Activity context)
        {
            this.context = context;
        }

        @Override
        protected void onPostExecute(String result)
        {
            progressDialog.dismiss();
            if (MainActivity.checkError())
            {
                switch (MainActivity.indexSpinner)
                {
                    case 0:
                        setListViewArgument();
                        break;
                    case 1:
                        setListViewDisciplinary();
                        break;
                    case 2:
                        setListViewSeat();
                        break;
                }
            }
            else
            {
                TextView txt;
                View lay = myInflatedView.findViewById(R.id.empty);
                txt = (TextView) lay.findViewById(R.id.emptySetNewsArrive);
                if (MainActivity.serverDown)
                {
                    txt.setText(getString(R.string.connection_error));
                }
                else if (MainActivity.errorGeneric)
                {
                    txt.setText(getString(R.string.generic_error));
                }

            }
        }

        @Override
        protected String doInBackground(String... params)
        {
            if (isOnline())
            {
                switch (MainActivity.indexSpinner)
                {
                    case 0:
                        HttpRequest.getSubFiltersByArgument(MainActivity.typeSearchArguments);
                        break;
                    case 1:
                        HttpRequest.getSubFiltersByDisciplinary(MainActivity.typeSearchDisciplinary);
                        break;
                    case 2:
                        HttpRequest.getSubFiltersBySeat(MainActivity.typeSearchSeat);
                        break;
                }
            }
            else
            {
                MainActivity.serverDown = true;
            }
            return "Finish";

        }

        @Override
        protected void onPreExecute()
        {
            switch (MainActivity.indexSpinner)
            {
                case 0:
                    progressDialog = ProgressDialog.show(context, getString(R.string.download_alert_title), getString(R.string.sub_filters_download_alert_arguments), true);
                    progressDialog.setCancelable(false);
                    break;
                case 1:
                    progressDialog = ProgressDialog.show(context, getString(R.string.download_alert_title), getString(R.string.sub_filters_download_alert_disciplinary_areas), true);
                    progressDialog.setCancelable(false);
                    break;
                case 2:
                    progressDialog = ProgressDialog.show(context, getString(R.string.download_alert_title), getString(R.string.sub_filters_download_alert_seats), true);
                    progressDialog.setCancelable(false);
                    break;
            }

        }

        @Override
        protected void onProgressUpdate(String... values)
        {
        }

        private void setListViewArgument()
        {
            if (MainActivity.typeSearchArguments.equals(getResources().getStringArray(R.array.new_arrivals_values)[2]))
            {
                MainActivity.subFiltersArgumentAll = new ArrayList<>();
                MainActivity.subFiltersArgumentAll.addAll(MainActivity.subFiltersArgumentBicocca);
                MainActivity.subFiltersArgumentAll.addAll(MainActivity.subFiltersArgumentInsubria);
                adapterSubFilters = new AdapterSubFilters(context, MainActivity.subFiltersArgumentAll);
                listViewSubFilters.setAdapter(adapterSubFilters);
                listViewSubFilters.setOnItemClickListener(new NewsItemClickListener());
            }
            else if (MainActivity.typeSearchArguments.equals(getResources().getStringArray(R.array.new_arrivals_values)[0]))
            {
                adapterSubFilters = new AdapterSubFilters(context, MainActivity.subFiltersArgumentBicocca);
                listViewSubFilters.setAdapter(adapterSubFilters);
                listViewSubFilters.setOnItemClickListener(new NewsItemClickListener());
            }
            else if (MainActivity.typeSearchArguments.equals((getResources().getStringArray(R.array.new_arrivals_values)[1])))
            {
                adapterSubFilters = new AdapterSubFilters(context, MainActivity.subFiltersArgumentInsubria);
                listViewSubFilters.setAdapter(adapterSubFilters);
                listViewSubFilters.setOnItemClickListener(new NewsItemClickListener());
            }

        }

        private void setListViewDisciplinary()
        {
            if (MainActivity.typeSearchDisciplinary.equals(getResources().getStringArray(R.array.new_arrivals_values)[2]))
            {
                MainActivity.subFiltersDisciplinaryAreaAll = new ArrayList<>();
                MainActivity.subFiltersDisciplinaryAreaAll.addAll(MainActivity.subFiltersDisciplinaryAreaBicocca);
                MainActivity.subFiltersDisciplinaryAreaAll.addAll(MainActivity.subFiltersDisciplinaryAreaInsubria);
                adapterSubFilters = new AdapterSubFilters(context, MainActivity.subFiltersDisciplinaryAreaAll);
                listViewSubFilters.setAdapter(adapterSubFilters);
                listViewSubFilters.setOnItemClickListener(new NewsItemClickListener());
            }
            else if (MainActivity.typeSearchDisciplinary.equals(getResources().getStringArray(R.array.new_arrivals_values)[0]))
            {
                adapterSubFilters = new AdapterSubFilters(context, MainActivity.subFiltersDisciplinaryAreaBicocca);
                listViewSubFilters.setAdapter(adapterSubFilters);
                listViewSubFilters.setOnItemClickListener(new NewsItemClickListener());
            }
            else if (MainActivity.typeSearchDisciplinary.equals(getResources().getStringArray(R.array.new_arrivals_values)[1]))
            {
                adapterSubFilters = new AdapterSubFilters(context, MainActivity.subFiltersDisciplinaryAreaInsubria);
                listViewSubFilters.setAdapter(adapterSubFilters);
                listViewSubFilters.setOnItemClickListener(new NewsItemClickListener());
            }
        }

        private void setListViewSeat()
        {
            if (MainActivity.typeSearchSeat.equals(getResources().getStringArray(R.array.new_arrivals_values)[2]))
            {
                MainActivity.subFiltersSeatAll = new ArrayList<>();
                MainActivity.subFiltersSeatAll.addAll(MainActivity.subFiltersSeatBicocca);
                MainActivity.subFiltersSeatAll.addAll(MainActivity.subFiltersSeatInsubria);
                adapterSubFilters = new AdapterSubFilters(context, MainActivity.subFiltersSeatAll);
                listViewSubFilters.setAdapter(adapterSubFilters);
                listViewSubFilters.setOnItemClickListener(new NewsItemClickListener());
            }
            else if (MainActivity.typeSearchSeat.equals(getResources().getStringArray(R.array.new_arrivals_values)[0]))
            {
                adapterSubFilters = new AdapterSubFilters(context, MainActivity.subFiltersSeatBicocca);
                listViewSubFilters.setAdapter(adapterSubFilters);
                listViewSubFilters.setOnItemClickListener(new NewsItemClickListener());
            }
            else if (MainActivity.typeSearchSeat.equals(getResources().getStringArray(R.array.new_arrivals_values)[1]))
            {
                adapterSubFilters = new AdapterSubFilters(context, MainActivity.subFiltersSeatInsubria);
                listViewSubFilters.setAdapter(adapterSubFilters);
                listViewSubFilters.setOnItemClickListener(new NewsItemClickListener());
            }
        }

    }


    private class NewsItemClickListener implements ListView.OnItemClickListener
    {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id)
        {
            selectItem(position);
        }
    }

    private void selectItem(int position)
    {

        switch (MainActivity.indexSpinner)
        {
            case 0:
                setIntentArgument(position);
                break;
            case 1:
                setIntentDisciplinary(position);
                break;
            case 2:
                setIntentSeat(position);
                break;
        }

    }

    private void setIntentSeat(int position)
    {
        Intent intent = new Intent(getActivity(), NewArrivalsActivity.class);
        String url = null;
        if (MainActivity.typeSearchSeat.equals(getResources().getStringArray(R.array.new_arrivals_values)[2]))
        {
            url = MainActivity.subFiltersSeatAll.get(position).getUrl();
            intent.putExtra("array", 8);
        }
        else if (MainActivity.typeSearchSeat.equals(getResources().getStringArray(R.array.new_arrivals_values)[1]))
        {
            url = MainActivity.subFiltersSeatInsubria.get(position).getUrl();
            intent.putExtra("array", 7);
        }
        else if (MainActivity.typeSearchSeat.equals(getResources().getStringArray(R.array.new_arrivals_values)[0]))
        {
            url = MainActivity.subFiltersSeatBicocca.get(position).getUrl();
            intent.putExtra("array", 6);
        }

        intent.putExtra("position", position);
        intent.putExtra("url", url);
        intent.putExtra("scelta", MainActivity.typeSearchSeat);
        intent.putExtra("ricarica", -1);

        startActivity(intent);
    }

    private void setIntentDisciplinary(int position)
    {
        Intent intent = new Intent(getActivity(), NewArrivalsActivity.class);
        String url = null;
        if (MainActivity.typeSearchDisciplinary.equals(getResources().getStringArray(R.array.new_arrivals_values)[2]))
        {
            url = MainActivity.subFiltersDisciplinaryAreaAll.get(position).getUrl();
            intent.putExtra("array", 5);
        }
        else if (MainActivity.typeSearchDisciplinary.equals(getResources().getStringArray(R.array.new_arrivals_values)[1]))
        {
            url = MainActivity.subFiltersDisciplinaryAreaInsubria.get(position).getUrl();
            intent.putExtra("array", 4);
        }
        else if (MainActivity.typeSearchDisciplinary.equals(getResources().getStringArray(R.array.new_arrivals_values)[0]))
        {
            url = MainActivity.subFiltersDisciplinaryAreaBicocca.get(position).getUrl();
            intent.putExtra("array", 3);
        }

        intent.putExtra("position", position);
        intent.putExtra("url", url);
        intent.putExtra("scelta", MainActivity.typeSearchDisciplinary);
        intent.putExtra("ricarica", -1);

        startActivity(intent);
    }

    private void setIntentArgument(int position)
    {
        Intent intent = new Intent(getActivity(), NewArrivalsActivity.class);
        String url = null;
        if (MainActivity.typeSearchArguments.equals(getResources().getStringArray(R.array.new_arrivals_values)[2]))
        {
            url = MainActivity.subFiltersArgumentAll.get(position).getUrl();
            intent.putExtra("array", 2);
        }
        else if (MainActivity.typeSearchArguments.equals(getResources().getStringArray(R.array.new_arrivals_values)[1]))
        {
            url = MainActivity.subFiltersArgumentInsubria.get(position).getUrl();
            intent.putExtra("array", 1);
        }
        else if (MainActivity.typeSearchArguments.equals(getResources().getStringArray(R.array.new_arrivals_values)[0]))
        {
            url = MainActivity.subFiltersArgumentBicocca.get(position).getUrl();
            intent.putExtra("array", 0);
        }

        intent.putExtra("position", position);
        intent.putExtra("url", url);
        intent.putExtra("scelta", MainActivity.typeSearchArguments);
        intent.putExtra("ricarica", -1);
        startActivity(intent);
    }

    @Override
    public void onResume()
    {
        if (adapterSubFilters != null)
        {
            loadOrNot();
        }
        super.onResume();
    }

    private void loadOrNot()
    {
        boolean load = true;
        if (MainActivity.indexSpinner == 0)
        {
            if (MainActivity.typeSearchArguments.equalsIgnoreCase(getResources().getStringArray(R.array.new_arrivals_values)[0]))
            {
                if (MainActivity.subFiltersArgumentBicocca != null &&
                    !MainActivity.subFiltersArgumentBicocca.isEmpty())
                {
                    adapterSubFilters = new AdapterSubFilters(getActivity(), MainActivity.subFiltersArgumentBicocca);
                    listViewSubFilters.setAdapter(adapterSubFilters);
                    listViewSubFilters.setOnItemClickListener(new NewsItemClickListener());
                    load = false;
                }

            }
            else if (MainActivity.typeSearchArguments.equalsIgnoreCase(getResources().getStringArray(R.array.new_arrivals_values)[1]))
            {
                if (MainActivity.subFiltersArgumentInsubria != null &&
                    !MainActivity.subFiltersArgumentInsubria.isEmpty())
                {
                    adapterSubFilters = new AdapterSubFilters(getActivity(), MainActivity.subFiltersArgumentInsubria);
                    listViewSubFilters.setAdapter(adapterSubFilters);
                    listViewSubFilters.setOnItemClickListener(new NewsItemClickListener());
                    load = false;
                }

            }
            else if (MainActivity.typeSearchArguments.equalsIgnoreCase(getResources().getStringArray(R.array.new_arrivals_values)[2]))
            {
                if (MainActivity.subFiltersArgumentAll != null &&
                    !MainActivity.subFiltersArgumentAll.isEmpty())
                {
                    adapterSubFilters = new AdapterSubFilters(getActivity(), MainActivity.subFiltersArgumentAll);
                    listViewSubFilters.setAdapter(adapterSubFilters);
                    listViewSubFilters.setOnItemClickListener(new NewsItemClickListener());
                    load = false;
                }
            }
        }
        else if (MainActivity.indexSpinner == 1)
        {
            if (MainActivity.typeSearchDisciplinary.equals((getResources().getStringArray(R.array.new_arrivals_values)[0])))
            {
                if (MainActivity.subFiltersDisciplinaryAreaBicocca != null &&
                    !MainActivity.subFiltersDisciplinaryAreaBicocca.isEmpty())
                {
                    adapterSubFilters = new AdapterSubFilters(getActivity(), MainActivity.subFiltersDisciplinaryAreaBicocca);
                    listViewSubFilters.setAdapter(adapterSubFilters);
                    listViewSubFilters.setOnItemClickListener(new NewsItemClickListener());
                    load = false;
                }
            }
            else if (MainActivity.typeSearchDisciplinary.equals((getResources().getStringArray(R.array.new_arrivals_values)[1])))
            {
                if (MainActivity.subFiltersDisciplinaryAreaInsubria != null &&
                    !MainActivity.subFiltersDisciplinaryAreaInsubria.isEmpty())
                {
                    adapterSubFilters = new AdapterSubFilters(getActivity(), MainActivity.subFiltersDisciplinaryAreaInsubria);
                    listViewSubFilters.setAdapter(adapterSubFilters);
                    listViewSubFilters.setOnItemClickListener(new NewsItemClickListener());
                    load = false;
                }
            }
            else if (MainActivity.typeSearchDisciplinary.equals((getResources().getStringArray(R.array.new_arrivals_values)[2])))
            {
                if (MainActivity.subFiltersDisciplinaryAreaAll != null &&
                    !MainActivity.subFiltersDisciplinaryAreaAll.isEmpty())
                {
                    adapterSubFilters = new AdapterSubFilters(getActivity(), MainActivity.subFiltersDisciplinaryAreaAll);
                    adapterSubFilters.addAll(MainActivity.subFiltersDisciplinaryAreaInsubria);
                    listViewSubFilters.setAdapter(adapterSubFilters);
                    listViewSubFilters.setOnItemClickListener(new NewsItemClickListener());
                    load = false;
                }
            }
        }
        else if (MainActivity.indexSpinner == 2)
        {
            if (MainActivity.typeSearchSeat.equals((getResources().getStringArray(R.array.new_arrivals_values)[0])))
            {
                if (MainActivity.subFiltersSeatBicocca != null &&
                    !MainActivity.subFiltersSeatBicocca.isEmpty())
                {
                    adapterSubFilters = new AdapterSubFilters(getActivity(), MainActivity.subFiltersSeatBicocca);
                    listViewSubFilters.setAdapter(adapterSubFilters);
                    listViewSubFilters.setOnItemClickListener(new NewsItemClickListener());
                    load = false;
                }
            }
            else if (MainActivity.typeSearchSeat.equals((getResources().getStringArray(R.array.new_arrivals_values)[1])))
            {
                if (MainActivity.subFiltersSeatInsubria != null &&
                    !MainActivity.subFiltersSeatInsubria.isEmpty())
                {
                    adapterSubFilters = new AdapterSubFilters(getActivity(), MainActivity.subFiltersSeatInsubria);
                    listViewSubFilters.setAdapter(adapterSubFilters);
                    listViewSubFilters.setOnItemClickListener(new NewsItemClickListener());
                    load = false;
                }
            }
            else if (MainActivity.typeSearchSeat.equals((getResources().getStringArray(R.array.new_arrivals_values)[2])))
            {
                if (MainActivity.subFiltersSeatAll != null &&
                    !MainActivity.subFiltersSeatAll.isEmpty())
                {
                    adapterSubFilters = new AdapterSubFilters(getActivity(), MainActivity.subFiltersSeatAll);
                    listViewSubFilters.setAdapter(adapterSubFilters);
                    listViewSubFilters.setOnItemClickListener(new NewsItemClickListener());
                    load = false;
                }
            }
        }
        if (load)
        {
            loadSubFilers loadSubFilers = new loadSubFilers(getActivity());
            loadSubFilers.execute();
        }

    }

    public boolean isOnline()
    {
        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }
}