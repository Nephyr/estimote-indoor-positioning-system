package com.disco.sal.biblioappbicocca.models;

import java.util.ArrayList;

/**
 * Created by lorenzo on 12/05/15.
 */
public class Document {
    private ArrayList<String> CDD;
    private ArrayList<String> author;
    private ArrayList<ChildParent> child;
    private ArrayList<String> collection;
    private ArrayList<String> contentNote;
    private ArrayList<String> corporateBody;
    private ArrayList<String> edition;
    private ArrayList<String> generalNote;
    private ArrayList<String> holdings;
    private ArrayList<String> isbn;
    private ArrayList<String> issn;
    private ArrayList<ChildParent> parent;
    private ArrayList<String> physicalDescription;
    private ArrayList<String> publication;
    private String thumbnail;
    private String title;
    private String type;
    private String url;
    private ArrayList<Copy> copieses;

    public Document(ArrayList<String> CDD, ArrayList<String> author, ArrayList<ChildParent> child, ArrayList<String> collection, ArrayList<String> contentNote, ArrayList<String> corporateBody, ArrayList<String> edition, ArrayList<String> generalNote, ArrayList<String> holdings, ArrayList<String> isbn, ArrayList<String> issn, ArrayList<ChildParent> parent, ArrayList<String> physicalDescription, ArrayList<String> publication, String thumbnail, String type, String title, String url, ArrayList<Copy> copieses) {
        this.CDD = CDD;
        this.author = author;
        this.child = child;
        this.collection = collection;
        this.contentNote = contentNote;
        this.corporateBody = corporateBody;
        this.edition = edition;
        this.generalNote = generalNote;
        this.holdings = holdings;
        this.isbn = isbn;
        this.issn = issn;
        this.parent = parent;
        this.physicalDescription = physicalDescription;
        this.publication = publication;
        this.thumbnail = thumbnail;
        this.title = title;
        this.type = type;
        this.url = url;
        this.copieses = copieses;
    }

    public Document(ArrayList<String> CDD, ArrayList<String> author, ArrayList<ChildParent> child, ArrayList<String> collection, ArrayList<String> contentNote, ArrayList<String> corporateBody, ArrayList<String> edition, ArrayList<String> generalNote, ArrayList<String> holdings, ArrayList<String> isbn, ArrayList<String> issn, ArrayList<String> physicalDescription, ArrayList<String> publication, String thumbnail, String title, String type, String url, ArrayList<Copy> copieses) {
        this.CDD = CDD;
        this.author = author;
        this.child = child;
        this.collection = collection;
        this.contentNote = contentNote;
        this.corporateBody = corporateBody;
        this.edition = edition;
        this.generalNote = generalNote;
        this.holdings = holdings;
        this.isbn = isbn;
        this.issn = issn;
        this.physicalDescription = physicalDescription;
        this.publication = publication;
        this.thumbnail = thumbnail;
        this.title = title;
        this.type = type;
        this.url = url;
        this.copieses = copieses;
    }

    public ArrayList<String> getCDD() {
        return CDD;
    }

    public void setCDD(ArrayList<String> CDD) {
        this.CDD = CDD;
    }

    public ArrayList<String> getAuthor() {
        return author;
    }

    public void setAuthor(ArrayList<String> author) {
        this.author = author;
    }

    public ArrayList<ChildParent> getChild() {
        return child;
    }

    public void setChild(ArrayList<ChildParent> child) {
        this.child = child;
    }

    public ArrayList<String> getCollection() {
        return collection;
    }

    public void setCollection(ArrayList<String> collection) {
        this.collection = collection;
    }

    public ArrayList<String> getContentNote() {
        return contentNote;
    }

    public void setContentNote(ArrayList<String> contentNote) {
        this.contentNote = contentNote;
    }

    public ArrayList<String> getCorporateBody() {
        return corporateBody;
    }

    public void setCorporateBody(ArrayList<String> corporateBody) {
        this.corporateBody = corporateBody;
    }

    public ArrayList<String> getEdition() {
        return edition;
    }

    public void setEdition(ArrayList<String> edition) {
        this.edition = edition;
    }

    public ArrayList<String> getGeneralNote() {
        return generalNote;
    }

    public void setGeneralNote(ArrayList<String> generalNote) {
        this.generalNote = generalNote;
    }

    public ArrayList<String> getHoldings() {
        return holdings;
    }

    public void setHoldings(ArrayList<String> holdings) {
        this.holdings = holdings;
    }

    public ArrayList<String> getIsbn() {
        return isbn;
    }

    public void setIsbn(ArrayList<String> isbn) {
        this.isbn = isbn;
    }

    public ArrayList<String> getIssn() {
        return issn;
    }

    public void setIssn(ArrayList<String> issn) {
        this.issn = issn;
    }

    public ArrayList<ChildParent> getParent() {
        return parent;
    }

    public void setParent(ArrayList<ChildParent> parent) {
        this.parent = parent;
    }

    public ArrayList<String> getPhysicalDescription() {
        return physicalDescription;
    }

    public void setPhysicalDescription(ArrayList<String> physicalDescription) {
        this.physicalDescription = physicalDescription;
    }

    public ArrayList<String> getPublication() {
        return publication;
    }

    public void setPublication(ArrayList<String> publication) {
        this.publication = publication;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public ArrayList<Copy> getCopieses() {
        return copieses;
    }

    public void setCopieses(ArrayList<Copy> copieses) {
        this.copieses = copieses;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}

