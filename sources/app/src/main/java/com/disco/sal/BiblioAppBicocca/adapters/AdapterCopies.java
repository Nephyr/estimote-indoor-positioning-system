package com.disco.sal.biblioappbicocca.adapters;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.disco.sal.biblioappbicocca.R;
import com.disco.sal.biblioappbicocca.activities.NewRoomNavAct;
import com.disco.sal.biblioappbicocca.activities.RoomActivity;
import com.disco.sal.biblioappbicocca.classes.Room;
import com.disco.sal.biblioappbicocca.classes.SearchRoom;
import com.disco.sal.biblioappbicocca.models.Copy;

import java.util.ArrayList;

/**
 * Created by lorenzo on 15/05/15.
 */
public class AdapterCopies extends ArrayAdapter<Copy>
{
    private final Context context;
    private final ArrayList<Copy> copies;
    private Copy copy;

    public AdapterCopies(Context context, ArrayList<Copy> copies)
    {
        super(context, R.layout.item_copy, copies);
        this.context = context;
        this.copies = copies;
    }

    @SuppressLint("ViewHolder")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.item_copy, parent, false);
        TextView txtCopyNumber = (TextView) rowView.findViewById(R.id.copyNumber);
        final LinearLayout hiddenLayout = (LinearLayout) rowView.findViewById(R.id.hiddenLayout);
        CardView cardLayout = (CardView) rowView.findViewById(R.id.cardLayout);
        final ImageView arrowImage = (ImageView) rowView.findViewById(R.id.arrowImage);
        ImageView enableCopy = (ImageView) rowView.findViewById(R.id.enableCopy);
        TextView txtBarcode = (TextView) rowView.findViewById(R.id.barcode);
        TextView txtLoanStatus = (TextView) rowView.findViewById(R.id.status);
        TextView txtDueDate = (TextView) rowView.findViewById(R.id.due_date);
        TextView txtCollection = (TextView) rowView.findViewById(R.id.collection);
        TextView txtLocation = (TextView) rowView.findViewById(R.id.location);
        Button buttoncopy = (Button) rowView.findViewById(R.id.button);
        Button reachbookcopy = (Button) rowView.findViewById(R.id.button2);
        copy = getItem(position);
        txtCopyNumber.setText(context.getString(R.string.copy) + " " + (position + 1));
        if (copy.getDueDate().equalsIgnoreCase("Disponibile"))
            enableCopy.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_green));
        else
            enableCopy.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_red));
        txtBarcode.setText(Html.fromHtml(
                "<strong>" + context.getString(R.string.barcode) + "</strong> " +
                copy.getBarcode()));
        txtLoanStatus.setText(Html.fromHtml(
                "<strong>" + context.getString(R.string.loan_status) + "</strong> " +
                copy.getLoanStatus()));
        txtDueDate.setText(Html.fromHtml(
                "<strong>" + context.getString(R.string.due_date) + "</strong> " +
                copy.getDueDate()));
        txtCollection.setText(Html.fromHtml(
                "<strong>" + context.getString(R.string.collection) + "</strong> " +
                copy.getCollection()));
        txtLocation.setText(Html.fromHtml(
                "<strong>" + context.getString(R.string.location) + "</strong> " +
                copy.getLocation()));
        hiddenLayout.setVisibility(View.GONE);
        cardLayout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (hiddenLayout.getVisibility() == View.GONE)
                {
                    hiddenLayout.setVisibility(View.VISIBLE);
                    arrowImage.setImageResource(R.drawable.ic_arrow_drop_up_black_24dp);

                }
                else
                {
                    hiddenLayout.setVisibility(View.GONE);
                    arrowImage.setImageResource(R.drawable.ic_arrow_drop_down_black_24dp);
                }
            }
        });
        buttoncopy.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                new Connection_sis().set(position, false);        //AsyncTask Connection_sis carica l'ambiente della biblioteca

            }
        });
        reachbookcopy.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                new Connection_sis().set(position, true);        //AsyncTask Connection_sis carica l'ambiente della biblioteca

            }
        });
        return rowView;
    }

    private class Connection_sis extends AsyncTask<String, String, String>
    {
        String sublibrary, location, fondo;
        private ProgressDialog progressDialog = null;
        private SearchRoom searchRoom = null;
        private Room tmpCurRoom = null;
        private boolean _typeIntent = false;

        @Override
        protected void onPostExecute(String result)
        {
            // Controllo più furbo per l'esistenza o meno delle stanze su SIS!
            if (this.tmpCurRoom == null)
            {
                progressDialog.dismiss();
                AlertDialog.Builder alert = new AlertDialog.Builder(context);
                alert.setTitle(R.string.caution_title);
                alert.setMessage(R.string.room_not_exist);
                alert.setPositiveButton("OK", null);
                alert.setCancelable(true);
                alert.create().show();
                return;
            }

            //Class<?> selectedIntent = (this._typeIntent) ? NewRoomNavAct.class : RoomActivity.class;
            Intent intent;
            if (_typeIntent)
            {
                intent = new Intent(context, NewRoomNavAct.class);
                intent.putExtra("library", this.tmpCurRoom.getParentLibrary().get(0));
                intent.putExtra("room", this.tmpCurRoom.getName());
                intent.putExtra("shelvesbook", this.tmpCurRoom.getNameShelvesbook());
            }
            else
            {
                // Creo intent
                intent = new Intent(context, RoomActivity.class);

                // PASSO L'OGGETTO VIA INTENT SERIALIZZATO!
                // MainActivity.room non più necessario.
                Bundle bundle = new Bundle();
                bundle.putSerializable("searchedroomobj", this.tmpCurRoom);
                intent.putExtras(bundle);

                //intent.putExtra("library", MainActivity.room.getParentLibrary().get(0));
            }

            progressDialog.dismiss();
            context.startActivity(intent);
        }

        @Override
        protected String doInBackground(String... params)
        {
            this.searchRoom = new SearchRoom(sublibrary, location, fondo);  //implementa la visita dell'albero
            this.tmpCurRoom = searchRoom.execute();
            return "Finish";
        }

        public void set(int position, boolean typeIntent)
        {
            this._typeIntent = typeIntent;
            this.sublibrary = copies.get(position).getSubLibrary();
            this.location = copies.get(position).getLocation();
            this.fondo = copies.get(position).getCollection();
            this.execute();
        }

        @Override
        protected void onPreExecute()
        {
            progressDialog = new ProgressDialog(context);
            progressDialog.setTitle(R.string.wait_title);
            progressDialog.setMessage(context.getResources().getString(R.string.location_loading));
            progressDialog.setCancelable(false);
            progressDialog.show();
        }
    }
}
