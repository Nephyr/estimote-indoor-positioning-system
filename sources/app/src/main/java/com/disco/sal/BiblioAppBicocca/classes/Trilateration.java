package com.disco.sal.biblioappbicocca.classes;

/**
 * Created by Marco on 31/01/2016.
 */
public class Trilateration implements Positioning
{
    @Override
    public double[] positioning(double[][] centers, double[] distances)
    {
        double a1, a2, a3, b1, b2, b3, c1, c2, c3, r1, r2, r3;

        a1 = centers[0][0];
        b1 = centers[0][1];
        r1 = distances[0];

        a2 = centers[1][0];
        b2 = centers[1][1];
        r2 = distances[1];

        a3 = centers[2][0];
        b3 = centers[2][1];
        r3 = distances[2];

        double ar1 = 0, br1 = 0, cr1 = 0, ar2 = 0, br2 = 0, cr2 = 0;
        c1 = (a1 * a1) + (b1 * b1) - (r1 * r1);
        c2 = (a2 * a2) + (b2 * b2) - (r2 * r2);
        c3 = (a3 * a3) + (b3 * b3) - (r3 * r3);


        //sceglie la circonferenza piu piccola perche la misura del raggio dovrebbe essere piu precisa
        //calcola i due assi radicali
        if (r1 <= r2 && r1 <= r3)
        {
            ar1 = a1 - a2;
            br1 = b1 - b2;
            cr1 = (c1 - c2) * (0.5);

            ar2 = a1 - a3;
            br2 = b1 - b3;
            cr2 = (c1 - c3) * (0.5);
        }
        else if (r2 <= r1 && r2 <= r3)
        {
            ar1 = a2 - a1;
            br1 = b2 - b1;
            cr1 = (c2 - c1) * (0.5);

            ar2 = a2 - a3;
            br2 = b2 - b3;
            cr2 = (c2 - c3) * (0.5);
        }
        else
        {
            ar1 = a3 - a2;
            br1 = b3 - b2;
            cr1 = (c3 - c2) * (0.5);

            ar2 = a3 - a1;
            br2 = b3 - b1;
            cr2 = (c3 - c1) * (0.5);
        }

        //risolve sistema lineare a due equazioni con cramer
        double x = (cr1 * br2 - cr2 * br1) / (ar1 * br2 - ar2 * br1);
        double y = (ar1 * cr2 - ar2 * cr1) / (ar1 * br2 - ar2 * br1);

            /*if ((ar1*br2-ar2*br1)== 0) {
                System.out.println("Il D del sistema e' 0 e il D delle incognite e' diverso da 0 \n quindi il sistema e' impossibile.");
            }
            else if((ar1*br2-ar2*br1)== 0 && (cr1*br2-cr2*br1)== 0) {
                System.out.println("\n Il D dell'incognita e quello del sistema sono uguali a 0 \n quindi il sistema e' indeterminato.");
            }
            else if((ar1*br2-ar2*br1)== 0 && (ar1*cr2-ar2*cr1)== 0) {
                System.out.println("\n Il D dell'incognita e quello del sistema sono uguali a 0 \n quindi il sistema e' indeterminato.");
            }*/

        if (((ar1 * br2 - ar2 * br1) != 0) && (x < 4 && x > 0 && y < 4 && y > 0))
        {
            //this.updatePosition(x, y);
            double[] xy = new double[2];
            xy[0] = x;
            xy[1] = y;
            return xy;
        }
        else
        {
            return new double[2];
        }
    }
}
