package com.disco.sal.biblioappbicocca.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.disco.sal.biblioappbicocca.R;
import com.disco.sal.biblioappbicocca.models.SubFilter;

import java.util.ArrayList;

/**
 * Created by lorenzo on 05/05/15.
 */
public class AdapterSubFilters extends ArrayAdapter<SubFilter>
{
    private final Context context;
    ArrayList<SubFilter> subFilters;

    public AdapterSubFilters(Context context, ArrayList<SubFilter> subFilters)
    {

        super(context, R.layout.item_sub_filter, subFilters);
        this.context = context;
        this.subFilters = subFilters;

    }

    @SuppressLint("ViewHolder")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.item_sub_filter, parent, false);
        TextView filterName = (TextView) rowView.findViewById(R.id.filterName);
        TextView filterNumebr = (TextView) rowView.findViewById(R.id.filterNumber);
        ImageView imageIcon = (ImageView) rowView.findViewById(R.id.iconSingle);
        SubFilter subFilters = getItem(position);
        imageIcon.setImageDrawable(rowView.getResources().getDrawable(subFilters.getIdImage()));
        filterName.setText(subFilters.getName());
        filterNumebr.setText(subFilters.getNumberOfNew());
        return rowView;
    }
}
