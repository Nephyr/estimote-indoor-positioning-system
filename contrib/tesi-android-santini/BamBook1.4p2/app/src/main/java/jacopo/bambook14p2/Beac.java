package jacopo.bambook14p2;

import android.graphics.Color;

import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.PointsGraphSeries;

import java.io.Serializable;

/**
 * Created by Jacopo on 12/05/2015.
 */
public class Beac {
    String name;
    double[] XYs; //coordinate in metri
    PointsGraphSeries<DataPoint> posBeacon;
    SISManager sisManager;

    public Beac(String name){
        sisManager = new SISManager();
        this.name = name;
        this.XYs = new double[2];
        this.XYs[0] = Double.parseDouble(sisManager.getBeac(name).split(",")[0])/10;
        this.XYs[1] = Double.parseDouble(sisManager.getBeac(name).split(",")[1])/10;
        this.posBeacon = new PointsGraphSeries<DataPoint>(new DataPoint[]{
                new DataPoint(this.XYs[0], this.XYs[1])
        });
    }

    public void drawBeac(){
        posBeacon.setColor(Color.parseColor("#333333"));
        posBeacon.setSize(8);
        Vars.graph.addSeries(posBeacon);
    }

    //ritorna la room contenente la shelf corrente (this) se esiste
    public Room getItsRoom(){
        for(Room r : Vars.rooms){
            if(r.getBeacs().contains(this)){
                return r;
            }
        }
        return null;
    }

    public void colorBeac(){
        Vars.graph.removeSeries(this.posBeacon);
        posBeacon.setColor(Color.parseColor("#00FF00"));
        posBeacon.setSize(8);
        Vars.graph.addSeries(posBeacon);
    }

    public void resetBeac(){
        Vars.graph.removeSeries(this.posBeacon);
        posBeacon.setColor(Color.parseColor("#000000"));
        posBeacon.setSize(8);
        Vars.graph.addSeries(posBeacon);
    }
    public String getName(){
        return this.name;
    }

    public double[] getXYs(){
        return this.XYs;
    }
}
