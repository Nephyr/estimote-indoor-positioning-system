package jacopo.bambook14p2;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import sal.sis.SIS;
import sal.sis.datatypes.Location;
import sal.sis.datatypes.Name;
import sal.sis.exceptions.SISServiceException;
import sal.sis.interfaces.ISpaceInspector;
import sal.sis.interfaces.ISpaceManager;
import sal.sis.services.SISServiceFactory;


public class HomeActivity extends ActionBarActivity {

    Toolbar toolbar;
    TextView infoSIS;
    ImageView syncStatusPOS;
    ImageView syncStatusRIL;
    ImageButton startPOS;
    ImageButton startRIL;
    SISManager sisManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        infoSIS = (TextView) findViewById(R.id.infoSIS);
        toolbar = (Toolbar) findViewById(R.id.myToolbar1);
        syncStatusPOS = (ImageView) findViewById(R.id.imageView2);
        syncStatusRIL = (ImageView) findViewById(R.id.imageView3);
        startPOS = (ImageButton) findViewById(R.id.imageButton);
        startRIL = (ImageButton) findViewById(R.id.imageButton2);
        toolbar.setTitle("Demo ");
        toolbar.setTitleTextColor(Color.parseColor("#000000"));
        setSupportActionBar(toolbar);

        startRIL.setVisibility(View.INVISIBLE);
        startPOS.setVisibility(View.INVISIBLE);
        sisManager = new SISManager();
    }
    public void onClickSetupPOS(View v){
        setupPOS();
    }
    public void onClickSyncPOS(View v){
        syncSIS();
        syncStatusRIL.setImageResource(R.drawable.cross);
        syncStatusPOS.setImageResource(R.drawable.tick);
        startPOS.setVisibility(View.VISIBLE);
        startRIL.setVisibility(View.INVISIBLE);
    }

    public void onClickStartPOS(View v){

        for(Room r : Vars.rooms){
            if(r.getName().equals("ROOM_3")) {
                Vars.selectedRoom = r;
            }
        }
        Intent intent = new Intent(getApplicationContext(), PositioningActivity.class);
        startActivity(intent);
    }

    public void onClickSetupRIL(View v){
        setupRIL();
    }

    public void onClickSyncRIL(View v){
        syncSIS();
        syncStatusPOS.setImageResource(R.drawable.cross);
        syncStatusRIL.setImageResource(R.drawable.tick);
        startPOS.setVisibility(View.INVISIBLE);
        startRIL.setVisibility(View.VISIBLE);
    }

    public void onClickStartRIL(View v){
        Intent intent = new Intent(getApplicationContext(), DetectionActivity.class);
        startActivity(intent);
    }

    public void setupPOS(){
        SIS _sis = SISServiceFactory.build("http://149.132.178.195:8080/SIS2ws-dev/", "5ce3009ce7f8ebc97ba69ec16348d677");
        ISpaceManager spaceManager = _sis.spaceManager();
        ISpaceInspector spaceInspector = _sis.spaceInspector();

        try {

            if(spaceInspector.getSpaceNames().contains("bambook.Grid.ROOM_3"))
                spaceManager.undefSpace("bambook.Grid.ROOM_3");

            if(spaceInspector.getSpaceNames().contains("bambook.Rooms"))
            {
                if(spaceInspector.locationExists("bambook.Rooms", new Name("ROOM_3")))
                {
                    spaceManager.removeLocations("bambook.Rooms", new ArrayList<Location>(){{
                        add(new Name("ROOM_3"));
                    }});
                }
            }

            if(spaceInspector.getSpaceNames().contains("bambook.Beacons"))
                spaceManager.undefSpace("bambook.Beacons");

            sisManager.setNameSpacesRooms();
            sisManager.setNameSpacesBeacons();

            sisManager.createRoom("ROOM_3", 5, 4);

            sisManager.createBeacon("F8:C3:D4:10:1A:75", "ROOM_3", 0, 1.9); //beacon azzurro
            sisManager.createBeacon("EB:C5:9B:39:23:2B", "ROOM_3", 1.9, 3.9); //beacon verde
            sisManager.createBeacon("F2:AB:B7:CB:33:CC", "ROOM_3", 3.1, 3.9); //beacon blu

            sisManager.createBeacon("CF:C3:2F:C4:9F:C8", "ROOM_3", 4.9, 1.9);
            sisManager.createBeacon("CE:0E:9E:E7:D6:95", "ROOM_3", 3.1, 0);
            sisManager.createBeacon("D0:BF:5C:E8:E0:27", "ROOM_3", 1.9, 0);
        } catch (SISServiceException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void setupRIL(){
        SIS _sis = SISServiceFactory.build("http://149.132.178.195:8080/SIS2ws-dev/", "5ce3009ce7f8ebc97ba69ec16348d677");
        ISpaceManager spaceManager = _sis.spaceManager();
        ISpaceInspector spaceInspector = _sis.spaceInspector();

        try {
            if(spaceInspector.getSpaceNames().contains("bambook.Grid.ROOM_1"))
                spaceManager.undefSpace("bambook.Grid.ROOM_1");

            if(spaceInspector.getSpaceNames().contains("bambook.Grid.ROOM_2"))
                spaceManager.undefSpace("bambook.Grid.ROOM_2");

            if(spaceInspector.getSpaceNames().contains("bambook.Rooms"))
            {
                if(spaceInspector.locationExists("bambook.Rooms", new Name("ROOM_1")))
                {
                    spaceManager.removeLocations("bambook.Rooms", new ArrayList<Location>(){{
                        add(new Name("ROOM_1"));
                    }});
                }
                if(spaceInspector.locationExists("bambook.Rooms", new Name("ROOM_2")))
                {
                    spaceManager.removeLocations("bambook.Rooms", new ArrayList<Location>(){{
                        add(new Name("ROOM_2"));
                    }});
                }
            }

            if(spaceInspector.getSpaceNames().contains("bambook.Beacons"))
                spaceManager.undefSpace("bambook.Beacons");

            sisManager.setNameSpacesRooms();
            sisManager.setNameSpacesBeacons();

            sisManager.createRoom("ROOM_1", 5, 4);
            sisManager.createRoom("ROOM_2", 3, 3);

            sisManager.createBeacon("F8:C3:D4:10:1A:75", "ROOM_1", 2.5, 0); //beacon azzurro
            sisManager.createBeacon("EB:C5:9B:39:23:2B", "ROOM_1", 1.4, 3.9); //beacon verde
            sisManager.createBeacon("F2:AB:B7:CB:33:CC", "ROOM_1", 3.5, 3.9); //beacon blu

            sisManager.createBeacon("CF:C3:2F:C4:9F:C8", "ROOM_2", 0, 1);
            sisManager.createBeacon("CE:0E:9E:E7:D6:95", "ROOM_2", 1.5, 2.9);
            sisManager.createBeacon("D0:BF:5C:E8:E0:27", "ROOM_2", 2.9, 1);
        } catch (SISServiceException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void syncSIS(){
        ArrayList<String> roomlist = sisManager.getListRooms();
        Vars.rooms = new ArrayList<>();
        for(String room : roomlist){
            if(room.equals("ROOM_1")||room.equals("ROOM_2")||room.equals("ROOM_3"))
                Vars.rooms.add(new Room(room));
        }

        infoSIS.setText("");
        infoSIS.append("STANZE:");
        for(Room r : Vars.rooms){
            infoSIS.append(" "+r.getName());
        }

        for(Room r : Vars.rooms){
            infoSIS.append("\n\n ---- \n\n");

            infoSIS.append("" + r.getName());
            infoSIS.append("\n\n");

            infoSIS.append("SCAFFALI IN " + r.getName() + ":");
            for(Shelf scaffale : r.getShelves()){
                infoSIS.append("\n" + scaffale.getName() + " -> " + sisManager.getShelf(scaffale.getName()));
            }
            infoSIS.append("\n\n");

            infoSIS.append("BEACON IN " + r.getName() + ":");
            for(Beac beac : r.getBeacs()){
                infoSIS.append("\n"+beac.getName()+" -> "+sisManager.getBeac(beac.getName()));
            }
        }
    }

}
