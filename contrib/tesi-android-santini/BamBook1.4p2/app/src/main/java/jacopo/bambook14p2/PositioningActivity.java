package jacopo.bambook14p2;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.RemoteException;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.estimote.sdk.Beacon;
import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.Region;
import com.estimote.sdk.Utils;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.GridLabelRenderer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PositioningActivity extends ActionBarActivity {

    Toolbar toolbar;
    TextView foundBeaconsText;
    TextView selRoom;
    ListView deviceList;

    //variabili estimote beacons
    static final String TAG = PositioningActivity.class.getSimpleName();
    static final int REQUEST_ENABLE_BT = 1234;
    static final Region ALL_ESTIMOTE_BEACONS_REGION = new Region("rid", null, null, null);
    BeaconManager beaconManager;
    DeviceListAdapter adapter;

    int timer = 0;
    double rssi1=0;
    double rssi2=0;
    double rssi3=0;
    double kFilteringFactor = 0.1;
    ArrayList<double[]> positionsList;
    LocationManager locationManager;
    boolean inAllB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_positioning);

        foundBeaconsText = (TextView) findViewById(R.id.foundBeaconsText);
        toolbar = (Toolbar) findViewById(R.id.myToolbar1);
        toolbar.setTitle("Posizionamento utente");
        toolbar.setTitleTextColor(Color.parseColor("#000000"));

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        selRoom = (TextView) findViewById(R.id.selRoom);
        if(Vars.selectedRoom!=null){
            double maxX=0;
            double maxY=0;
            for(double[] coord : Vars.selectedRoom.getXYs()){
                if(coord[0]>maxX)
                    maxX=coord[0];
                if(coord[1]>maxY)
                    maxY=coord[1];
            }
            selRoom.setText(Vars.selectedRoom.getName()+" ("+maxX+"x"+maxY+")");
            drawGraph();
            Vars.selectedRoom.drawRoom();
        }
        if(Vars.selectedShelves!=null){
            for(Shelf shelf : Vars.selectedShelves){
                shelf.colorShelf();
            }
        }

        final RadioButton allBeacs = (RadioButton) findViewById(R.id.RADIO8pos);
        final RadioButton threeBeacs = (RadioButton) findViewById(R.id.RADIO3beacs);
        final RadioButton AVGthreeBeacs = (RadioButton) findViewById(R.id.RADIOmedia3beac);
        locationManager = new LocationManager();
        positionsList = new ArrayList<>();
        inAllB = false;
        // Configure device list.
        adapter = new DeviceListAdapter(this);
        deviceList = (ListView) findViewById(R.id.device_list);
        deviceList.setAdapter(adapter);

        // Configure BeaconManager.
        beaconManager = new BeaconManager(this);
        beaconManager.setForegroundScanPeriod(250l, 0);
        beaconManager.setBackgroundScanPeriod(250l, 0);

        beaconManager.setRangingListener(new BeaconManager.RangingListener() {
            @Override
            public void onBeaconsDiscovered(Region region, final List<Beacon> beacons) {
                // Note that results are not delivered on UI thread.
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        // Note that beacons reported here are already sorted by estimated
                        // distance between device and beacon.
                        adapter.replaceWith(beacons);

                        setListViewHeightBasedOnItems(deviceList);
                        foundBeaconsText.setText("" + beacons.size());

                        if (beacons.size() >= 3 && Vars.selectedRoom != null) {
                            int numBeacons = beacons.size();
                            double[][] centers= new double[numBeacons][2];
                            TextView time = (TextView) findViewById(R.id.contText);
                            time.setText("" + (timer++));
                            for (Beac b : Vars.selectedRoom.getBeacs()) {
                                if (b.getName().equals(beacons.get(0).getMacAddress())) {
                                    centers[0] = b.getXYs();
                                    rssi1 = (beacons.get(0).getRssi() * kFilteringFactor) + (rssi1 * (1.0 - kFilteringFactor));
                                    TextView t1rssi = (TextView) findViewById(R.id.textView8);
                                    t1rssi.setText(String.format("%.2f", locationManager.calculateAccuracyWithRSSI(rssi1)));

                                    TextView t1sdk = (TextView) findViewById(R.id.textView13);
                                    t1sdk.setText(String.format("%.2f", Utils.computeAccuracy(beacons.get(0))));
                                }
                                if (b.getName().equals(beacons.get(1).getMacAddress())) {
                                    centers[1] = b.getXYs();
                                    rssi2 = (beacons.get(1).getRssi() * kFilteringFactor) + (rssi2 * (1.0 - kFilteringFactor));
                                    TextView t2rssi = (TextView) findViewById(R.id.textView9);
                                    t2rssi.setText(String.format("%.2f", locationManager.calculateAccuracyWithRSSI(rssi2)));

                                    TextView t2sdk = (TextView) findViewById(R.id.textView14);
                                    t2sdk.setText(String.format("%.2f", Utils.computeAccuracy(beacons.get(1))));
                                }
                                if (b.getName().equals(beacons.get(2).getMacAddress())) {
                                    centers[2] = b.getXYs();
                                    rssi3 = (beacons.get(2).getRssi() * kFilteringFactor) + (rssi3 * (1.0 - kFilteringFactor));
                                    TextView t3rssi = (TextView) findViewById(R.id.textView10);
                                    t3rssi.setText(String.format("%.2f", locationManager.calculateAccuracyWithRSSI(rssi3)));

                                    TextView t3sdk = (TextView) findViewById(R.id.textView15);
                                    t3sdk.setText(String.format("%.2f", Utils.computeAccuracy(beacons.get(2))));
                                }
                                for(int i=0;i<beacons.size()-3;i++) {
                                    if (b.getName().equals(beacons.get(3+i).getMacAddress())) {
                                        centers[3+i] = b.getXYs();
                                    }
                                }
                            }

                            //Vars.locationManager.calcPositionFromRSSI(center1, Vars.locationManager.calculateAccuracyWithRSSI(rssi1), center2, Vars.locationManager.calculateAccuracyWithRSSI(rssi2), center3, Vars.locationManager.calculateAccuracyWithRSSI(rssi3));
                            if(allBeacs.isChecked()) {
                                if(!inAllB) {
                                    for (Beac b : Vars.selectedRoom.getBeacs()) {
                                        b.resetBeac();
                                    }
                                }
                                inAllB = true;
                                double[] xy = new double[2];
                                for(int i=0; i<numBeacons-2;i++){
                                    xy = new double[2];
                                    xy = locationManager.calcPosition(centers[i], Utils.computeAccuracy(beacons.get(i)), centers[i+1], Utils.computeAccuracy(beacons.get(i+1)), centers[i+2], Utils.computeAccuracy(beacons.get(i+2)));
                                    positionsList.add(xy);
                                }

                                if (positionsList.size() >= (numBeacons-2)*4) {
                                    double[] totxy = new double[2];
                                    totxy[0]=0;
                                    totxy[1]=0;
                                    for(double[] pos : positionsList){
                                        totxy[0] = totxy[0] + pos[0];
                                        totxy[1] = totxy[1] + pos[1];
                                    }
                                    totxy[0] = totxy[0] / positionsList.size();
                                    totxy[1] = totxy[1] / positionsList.size();
                                    locationManager.updatePosition(totxy[0], totxy[1]);
                                    positionsList = new ArrayList<double[]>();
                                }
                            }
                            else if(threeBeacs.isChecked()){
                                double xy[];
                                xy = locationManager.calcPosition(centers[0], Utils.computeAccuracy(beacons.get(0)), centers[1], Utils.computeAccuracy(beacons.get(1)), centers[2], Utils.computeAccuracy(beacons.get(2)));
                                locationManager.updatePosition(xy[0], xy[1]);
                                for(Beac b : Vars.selectedRoom.getBeacs()){
                                    if(beacons.get(0).getMacAddress().equals(b.getName()) || beacons.get(1).getMacAddress().equals(b.getName()) || beacons.get(2).getMacAddress().equals(b.getName())) {
                                        b.colorBeac();
                                    }
                                    else{
                                        b.resetBeac();
                                    }
                                }
                                inAllB = false;
                            }
                            else if(AVGthreeBeacs.isChecked()){
                                double xy[];
                                xy = locationManager.calcPosition(centers[0], Utils.computeAccuracy(beacons.get(0)), centers[1], Utils.computeAccuracy(beacons.get(1)), centers[2], Utils.computeAccuracy(beacons.get(2)));

                                for(Beac b : Vars.selectedRoom.getBeacs()){
                                    if(beacons.get(0).getMacAddress().equals(b.getName()) || beacons.get(1).getMacAddress().equals(b.getName()) || beacons.get(2).getMacAddress().equals(b.getName())) {
                                        b.colorBeac();
                                    }
                                    else{
                                        b.resetBeac();
                                    }
                                }
                                positionsList.add(xy);
                                if(positionsList.size()>=4){
                                    xy[0] = (positionsList.get(0)[0] + positionsList.get(1)[0] + positionsList.get(2)[0] + positionsList.get(3)[0]) / 4;
                                    xy[1] = (positionsList.get(0)[1] + positionsList.get(1)[1] + positionsList.get(2)[1] + positionsList.get(3)[1]) / 4;
                                    locationManager.updatePosition(xy[0], xy[1]);
                                    positionsList = new ArrayList<double[]>();
                                }
                                inAllB = false;
                            }

                        }
                    }

                });
            }
        });
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return true;
    }

    private void drawGraph(){
        Vars.graph = (GraphView) findViewById(R.id.graph);
        Vars.graph.removeAllSeries();
        Vars.graph.getGridLabelRenderer().setGridStyle(GridLabelRenderer.GridStyle.NONE);
        Vars.graph.getGridLabelRenderer().setHorizontalLabelsVisible(false);
        Vars.graph.getGridLabelRenderer().setVerticalLabelsVisible(false);
        /*Vars.graph.getGridLabelRenderer().setGridColor(Color.GRAY);
        Vars.graph.getGridLabelRenderer().setHorizontalLabelsColor(Color.GRAY);
        Vars.graph.getGridLabelRenderer().setVerticalLabelsColor(Color.GRAY);
        */
        Vars.graph.getViewport().setXAxisBoundsManual(true);
        Vars.graph.getViewport().setYAxisBoundsManual(true);

        //calcolo dimensione grafico in base alla dimensione della stanza selezionata (il grafico deve essere quadrato per le proporzioni grafiche)
        double dimGraph = 0;
        for(double[] xy : Vars.selectedRoom.getXYs()){
            if(xy[0]>dimGraph){
                dimGraph = xy[0];
            }
            if(xy[1]>dimGraph){
                dimGraph = xy[1];
            }
        }
        Vars.graph.getViewport().setMaxX(dimGraph);
        Vars.graph.getViewport().setMaxY(dimGraph);
    }

    public static boolean setListViewHeightBasedOnItems(ListView listView) {

        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter != null) {

            int numberOfItems = listAdapter.getCount();

            // Get total height of all items.
            int totalItemsHeight = 0;

            for (int itemPos = 0; itemPos < numberOfItems; itemPos++) {
                View item = listAdapter.getView(itemPos, null, listView);
                item.measure(0, 0);
                totalItemsHeight += item.getMeasuredHeight();
            }

            // Get total height of all item dividers.
            int totalDividersHeight = listView.getDividerHeight() *
                    (numberOfItems - 1);

            // Set list height.
            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = totalItemsHeight + totalDividersHeight;
            listView.setLayoutParams(params);
            listView.requestLayout();

            return true;

        } else {
            return false;
        }

    }

    //INIZIO METODI ESTIMOTE BEACONS

    @Override
    protected void onDestroy() {
        beaconManager.disconnect();
        super.onDestroy();
    }

    @Override
    protected void onStart() {
        super.onStart();

        // Check if device supports Bluetooth Low Energy.
        if (!beaconManager.hasBluetooth()) {
            Toast.makeText(this, "Device does not have Bluetooth Low Energy", Toast.LENGTH_LONG).show();
            return;
        }

        // If Bluetooth is not enabled, let user enable it.
        if (!beaconManager.isBluetoothEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        } else {
            connectToService();
        }
    }

    @Override
    protected void onStop() {
        try {
            beaconManager.stopRanging(ALL_ESTIMOTE_BEACONS_REGION);
        } catch (RemoteException e) {
            Log.d(TAG, "Error while stopping ranging", e);
        }


        super.onStop();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_ENABLE_BT) {
            if (resultCode == Activity.RESULT_OK) {
                connectToService();
            } else {
                Toast.makeText(this, "Bluetooth not enabled", Toast.LENGTH_LONG).show();
                getActionBar().setSubtitle("Bluetooth not enabled");
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void connectToService() {
        adapter.replaceWith(Collections.<Beacon>emptyList());
        beaconManager.connect(new BeaconManager.ServiceReadyCallback() {
            @Override
            public void onServiceReady() {
                try {
                    beaconManager.startRanging(ALL_ESTIMOTE_BEACONS_REGION);
                } catch (RemoteException e) {
                    Toast.makeText(PositioningActivity.this, "Cannot start ranging, something terrible happened",
                            Toast.LENGTH_LONG).show();
                    Log.e(TAG, "Cannot start ranging", e);
                }
            }
        });
    }

    //FINE METODI ESTIMOTE BEACONS
}
