package jacopo.bambook14p2;

import java.util.ArrayList;
import java.util.List;

import sal.sis.SIS;
import sal.sis.SpatialModels;
import sal.sis.datatypes.EnumerativeContext;
import sal.sis.datatypes.Grid2DSpaceParameters;
import sal.sis.datatypes.Location;
import sal.sis.datatypes.MappingDescription;
import sal.sis.datatypes.Name;
import sal.sis.datatypes.NameSpaceParameters;
import sal.sis.datatypes.Node;
import sal.sis.datatypes.OrientedWeightedGraphParameters;
import sal.sis.datatypes.SpaceInformation;
import sal.sis.datatypes.WeightedEdge;
import sal.sis.exceptions.SISServiceException;
import sal.sis.interfaces.IMappingsInspector;
import sal.sis.interfaces.IMappingsManager;
import sal.sis.interfaces.ISpaceInspector;
import sal.sis.interfaces.ISpaceManager;
import sal.sis.services.SISServiceFactory;

/**
 * Created by Jacopo on 12/05/2015.
 */
public class SISManager {

    SIS _sis;
    ISpaceManager spaceManager;
    ISpaceInspector spaceInspector;
    IMappingsManager mappingsManager;
    IMappingsInspector mappingsInspector;

    public SISManager()
    {
        _sis = SISServiceFactory.build("http://149.132.178.195:8080/SIS2ws-dev/", "5ce3009ce7f8ebc97ba69ec16348d677");
        spaceManager = _sis.spaceManager();
        spaceInspector = _sis.spaceInspector();
        mappingsManager = _sis.mappingsManager();
        mappingsInspector = _sis.mappingsInspector();
    }

    //Crea lo spazio dei nomi delle stanze se non esiste
    public void setNameSpacesRooms()
    {
        try {
            List<String> spaces = spaceInspector.getSpaceNames();
            if(!spaces.contains("bambook.Rooms"))
            {
                NameSpaceParameters params = new NameSpaceParameters(new ArrayList<String>()); //spazio dei nomi delle stanze

                spaceManager.defSpace("bambook.Rooms", SpatialModels.NAME, params);

            }
        } catch (SISServiceException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    //Crea lo spazio dei nomi dei beacon se non esiste
    public void setNameSpacesBeacons()
    {
        try {
            List<String> spaces = spaceInspector.getSpaceNames();
            if(!spaces.contains("bambook.Beacons"))
            {
                NameSpaceParameters params = new NameSpaceParameters(new ArrayList<String>()); //spazio dei nomi dei Beacons

                spaceManager.defSpace("bambook.Beacons", SpatialModels.NAME, params);
            }
        } catch (SISServiceException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    //CREA NOMI DI OGGETTI
    //crea una stanza
    public void createRoom(String name ,double x , double y)
    {
        int x_i = (int) x * 10,y_i = (int) y * 10;
        try {
            List<String> spaces = spaceInspector.getSpaceNames();
            spaces = spaceInspector.getSpaceNames();

            if(!spaces.contains("bambook.Grid."+name))
            {
                ArrayList<String> cell_list = new ArrayList<String>();
                Grid2DSpaceParameters params = new Grid2DSpaceParameters();
                EnumerativeContext grid, name_room;
                params.setMinX(0);
                params.setMinY(0);
                params.setMaxX(x_i);
                params.setMaxY(y_i);

                spaceManager.defSpace("bambook.Grid."+name, SpatialModels.GRID2D, params);

                for(int i = 0; i < x_i; i++)
                {
                    for(int j = 0; j < y_i; j++) {
                        cell_list.add(i +","+ j);
                    }
                }
                Name newroom = new Name(name);
                grid = new EnumerativeContext("bambook.Grid."+name, cell_list);
                try {
                    spaceManager.addLocation("bambook.Rooms", newroom);
                } catch (SISServiceException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }

                name_room = new EnumerativeContext("bambook.Rooms", name);
                try {
                    mappingsManager.map(name_room, grid);
                } catch (SISServiceException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            else
                System.out.println("La room chiamata "+name+" esiste gia'." );
        } catch (SISServiceException e2) {
            // TODO Auto-generated catch block
            e2.printStackTrace();
        }
    }

    //crea un beacon all'interno di una cella di una stanza
    public void createBeacon(String macadress, String room, double x, double y)
    {
        int x_i , y_i;
        x = x*10;
        y = y*10;
        x_i = (int) x;
        y_i = (int) y;
        EnumerativeContext this_beac, this_room, this_roomgrid;

        try {
            if(!spaceInspector.locationExists("bambook.Beacons", new Name(macadress)))
            {
                if(spaceInspector.locationExists("bambook.Rooms", new Name(room)))
                {
                    Name beacon = new Name (macadress);
                    spaceManager.addLocation("bambook.Beacons", beacon);
                    this_beac = new EnumerativeContext("bambook.Beacons", macadress);
                    this_room = new EnumerativeContext("bambook.Rooms",room);
                    this_roomgrid = new EnumerativeContext("bambook.Grid."+room,x_i+","+y_i);
                    mappingsManager.map(this_room, this_beac);
                    mappingsManager.map(this_beac, this_roomgrid);
                }
                else
                    System.out.println("La room chiamata "+room+" non esiste.");
            }
            else
                System.out.println("Il Beacon con MAC ADRESS: "+macadress+" � gi� presente nella lista di beacon");
        } catch (SISServiceException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
//GET CELLE CHE RAPPRESENTANO UN OGGETTO
    //restituisce la lista di celle che rappresenta una stanza
    public ArrayList<String> getRoom(String room)
    {
        try {
            EnumerativeContext ctxs = mappingsInspector.getForwardMapped("bambook.Rooms", room, "bambook.Grid."+room);
            if(ctxs == null)
                return new ArrayList<String>();
            return (ArrayList<String>) ctxs.getLocationNames();
        } catch (SISServiceException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return new ArrayList<String>();
    }

    //restituisce la lista di celle che rappresenta uno shelf
    public ArrayList<String> getShelf(String shelf)
    {
        EnumerativeContext cell,room;
        ArrayList<String> shelfcell = new ArrayList<String>();
        try {
            if(spaceInspector.locationExists("bambook.Shelves", new Name(shelf))){
                room = mappingsInspector.getReverseMapped("bambook.Shelves",shelf,"bambook.Rooms");
                cell = mappingsInspector.getForwardMapped("bambook.Shelves",shelf,"bambook.Grid."+room.getLocationNames().get(0));
                for(int i = 0; i < cell.getLocationNames().size(); i++)
                {
                    shelfcell.add(cell.getLocationNames().get(i));
                }
                return shelfcell;
            }
        } catch (SISServiceException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return shelfcell;
    }

    //restituisce la cella della stanza dove e' posizionato il beacon
    public String getBeac(String beac)
    {
        EnumerativeContext cell,room;
        String beaconcell = "";
        try {
            if(spaceInspector.locationExists("bambook.Beacons", new Name(beac))){
                room = mappingsInspector.getReverseMapped("bambook.Beacons",beac,"bambook.Rooms");
                cell = mappingsInspector.getForwardMapped("bambook.Beacons",beac,"bambook.Grid."+room.getLocationNames().get(0));
                beaconcell = cell.getLocationNames().get(0);

            }
        } catch (SISServiceException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return beaconcell;
    }

//GET LISTA OGGETTI
    //restituisce la lista di tutte le stanze
    public ArrayList<String> getListRooms()
    {
        NameSpaceParameters params;
        try {
            if(spaceInspector.getSpaceNames().contains("bambook.Rooms")){
                SpaceInformation info = spaceInspector.getSpaceInformation("bambook.Rooms");
                params = (NameSpaceParameters) info.getParameters();
                return (ArrayList<String>) params.getNamesLocators();
            }
        } catch (SISServiceException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return new ArrayList<String>();
    }

    //restituisce la lista di tutti gli shelf in una stanza
    public ArrayList<String> getListShelves(String room)
    {
        try {
            EnumerativeContext ctxs = mappingsInspector.getForwardMapped("bambook.Rooms", room, "bambook.Shelves");
            if(ctxs == null)
                return new ArrayList<String>();
            return (ArrayList<String>)ctxs.getLocationNames();
        } catch (SISServiceException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return new ArrayList<String>();
    }

    //restituisce la lista di tutti i beacons in una stanza
    public ArrayList<String> getListBeacs(String room)
    {
        try {
            EnumerativeContext ctxs = mappingsInspector.getForwardMapped("bambook.Rooms", room, "bambook.Beacons");
            if(ctxs == null)
                return new ArrayList<String>();
            return (ArrayList<String>) ctxs.getLocationNames();
        } catch (SISServiceException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return new ArrayList<String>();
    }

}
