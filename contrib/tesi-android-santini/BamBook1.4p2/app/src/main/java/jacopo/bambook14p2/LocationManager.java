package jacopo.bambook14p2;

import android.graphics.Color;

import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.PointsGraphSeries;

import java.util.ArrayList;

/**
 * Created by Jacopo on 14/05/2015.
 */
public class LocationManager {
    PointsGraphSeries<DataPoint> userPosition;
    PointsGraphSeries<DataPoint> userPositionRSSI;

    public double[] calcPosition(double[] center1, double distance1, double[] center2, double distance2, double[] center3, double distance3){
        double a1,a2,a3,b1,b2,b3,c1,c2,c3,r1,r2,r3;

        a1 = center1[0];
        b1 = center1[1];
        r1 = distance1;

        a2 = center2[0];
        b2 = center2[1];
        r2 = distance2;

        a3 = center3[0];
        b3 = center3[1];
        r3 = distance3;

        double ar1=0,br1=0,cr1=0,ar2=0,br2=0,cr2=0;
        c1 = (a1*a1) + (b1*b1) - (r1*r1);
        c2 = (a2*a2) + (b2*b2) - (r2*r2);
        c3 = (a3*a3) + (b3*b3) - (r3*r3);


        //sceglie la circonferenza piu piccola perche la misura del raggio dovrebbe essere piu precisa
        //calcola i due assi radicali
        if(r1<=r2&&r1<=r3)
        {
            ar1 = a1 - a2;
            br1 = b1 - b2;
            cr1 = (c1 - c2)*(0.5);

            ar2 = a1 - a3;
            br2 = b1 - b3;
            cr2 = (c1 - c3)*(0.5);
        }
        else if(r2<=r1&&r2<=r3)
        {
            ar1 = a2 - a1;
            br1 = b2 - b1;
            cr1 = (c2 - c1)*(0.5);

            ar2 = a2 - a3;
            br2 = b2 - b3;
            cr2 = (c2 - c3)*(0.5);
        }
        else
        {
            ar1 = a3 - a2;
            br1 = b3 - b2;
            cr1 = (c3 - c2)*(0.5);

            ar2 = a3 - a1;
            br2 = b3 - b1;
            cr2 = (c3 - c1)*(0.5);
        }

        //risolve sistema lineare a due equazioni con cramer
        double x = (cr1*br2-cr2*br1) / (ar1*br2-ar2*br1);
        double y = (ar1*cr2-ar2*cr1) / (ar1*br2-ar2*br1);

            /*if ((ar1*br2-ar2*br1)== 0) {
                System.out.println("Il D del sistema e' 0 e il D delle incognite e' diverso da 0 \n quindi il sistema e' impossibile.");
            }
            else if((ar1*br2-ar2*br1)== 0 && (cr1*br2-cr2*br1)== 0) {
                System.out.println("\n Il D dell'incognita e quello del sistema sono uguali a 0 \n quindi il sistema e' indeterminato.");
            }
            else if((ar1*br2-ar2*br1)== 0 && (ar1*cr2-ar2*cr1)== 0) {
                System.out.println("\n Il D dell'incognita e quello del sistema sono uguali a 0 \n quindi il sistema e' indeterminato.");
            }*/

        if(((ar1*br2-ar2*br1)!= 0) && (x<4&&x>0&&y<4&&y>0))
        {
            //this.updatePosition(x, y);
            double[] xy = new double[2];
            xy[0] = x;
            xy[1] = y;
            return xy;
        }
        else{
            return new double[2];
        }
    }

    public void updatePosition(double x, double y){
        if(this.userPosition!=null)
            Vars.graph.removeSeries(this.userPosition);

        this.userPosition = new PointsGraphSeries<DataPoint>(new DataPoint[] {
                new DataPoint(x,y),
        });
        this.userPosition.setShape(PointsGraphSeries.Shape.TRIANGLE);
        this.userPosition.setColor(Color.parseColor("#0000FF"));
        this.userPosition.setSize(8);
        Vars.graph.addSeries(this.userPosition);
    }

    public void updatePositionRSSI(double x, double y){
        if(this.userPositionRSSI !=null)
            Vars.graph.removeSeries(this.userPositionRSSI);

        this.userPositionRSSI = new PointsGraphSeries<DataPoint>(new DataPoint[] {
                new DataPoint(x,y),
        });
        this.userPositionRSSI.setShape(PointsGraphSeries.Shape.TRIANGLE);
        this.userPositionRSSI.setColor(Color.parseColor("#FF0000"));
        this.userPositionRSSI.setSize(8);
        Vars.graph.addSeries(this.userPositionRSSI);
    }

    public double calculateAccuracyWithRSSI(double rssi) {
        //formula adapted from David Young's Radius Networks Android iBeacon Code
        if (rssi == 0) {
            return -1.0; // if we cannot determine accuracy, return -1.
        }

        double txPower = -70;
        double ratio = rssi*1.0/txPower;
        if (ratio < 1.0) {
            return Math.pow(ratio, 10);
        }
        else {
            double accuracy =  (0.89976) * Math.pow(ratio, 7.7095) + 0.111;
            return accuracy;
        }
    }

    //prende tutta la lista di beacon rilevati, la analizza e determina a quale stanza appartengono per il maggior numero
    public Room getCurrentRoom(ArrayList<Beac> listBeac) {
        ArrayList<Room> listRooms = new ArrayList<>();
        for (Beac beac : listBeac) {
            Room r = beac.getItsRoom();
            if (!listRooms.contains(r)) {
                listRooms.add(r);
            }
        }
        int[] numBeacARoom = new int[listRooms.size()];
        for (Beac beac : listBeac) {
            Room r = beac.getItsRoom();
            numBeacARoom[listRooms.indexOf(r)]++;
        }
        int max=0;
        int indexOfCurrentRoom=0;
        for (int i = 0; i < numBeacARoom.length; i++) {
            if(numBeacARoom[i]>max){
                max=numBeacARoom[i];
                indexOfCurrentRoom=i;
            }
        }

        return listRooms.get(indexOfCurrentRoom);

    }
}
