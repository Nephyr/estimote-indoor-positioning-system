package jacopo.bambook14p2;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.RemoteException;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.estimote.sdk.Beacon;
import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.Region;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.GridLabelRenderer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DetectionActivity extends ActionBarActivity {

    Toolbar toolbar;
    TextView foundBeaconsText;
    TextView selRoom;
    ListView deviceList;
    //variabili estimote beacons
    static final String TAG = PositioningActivity.class.getSimpleName();
    static final int REQUEST_ENABLE_BT = 1234;
    static final Region ALL_ESTIMOTE_BEACONS_REGION = new Region("rid", null, null, null);
    BeaconManager beaconManager;
    DeviceListAdapter adapter;

    int timer;
    LocationManager locationManager;
    Room currentRoom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detection);

        foundBeaconsText = (TextView) findViewById(R.id.foundBeaconsText);
        toolbar = (Toolbar) findViewById(R.id.myToolbar1);
        toolbar.setTitle("Rilevamento stanza");
        toolbar.setTitleTextColor(Color.parseColor("#000000"));

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        selRoom = (TextView) findViewById(R.id.selRoom);
        timer=0;
        locationManager = new LocationManager();

        // Configure device list.
        adapter = new DeviceListAdapter(this);
        deviceList = (ListView) findViewById(R.id.device_list);
        deviceList.setAdapter(adapter);

        // Configure BeaconManager.
        beaconManager = new BeaconManager(this);
        beaconManager.setForegroundScanPeriod(250l, 0);
        beaconManager.setBackgroundScanPeriod(250l, 0);
        beaconManager.setRangingListener(new BeaconManager.RangingListener() {
            @Override
            public void onBeaconsDiscovered(Region region, final List<Beacon> beacons) {
                // Note that results are not delivered on UI thread.
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        // Note that beacons reported here are already sorted by estimated
                        // distance between device and beacon.
                        adapter.replaceWith(beacons);

                        setListViewHeightBasedOnItems(deviceList);
                        foundBeaconsText.setText("" + beacons.size());
                        TextView time = (TextView) findViewById(R.id.contText);
                        time.setText("" + (timer++));
                        if (beacons.size() >= 1) {
                            ArrayList<Beac> listBeacs = new ArrayList<>();
                            for (Beacon b : beacons) {
                                for (Room r : Vars.rooms) {
                                    for (Beac beac : r.getBeacs()) {
                                        if (b.getMacAddress().equals(beac.getName())) {
                                            listBeacs.add(beac);
                                        }
                                    }
                                }
                            }

                            currentRoom = locationManager.getCurrentRoom(listBeacs);

                            if (!currentRoom.getName().equals(Vars.selectedRoom)) {
                                Vars.selectedRoom = currentRoom;
                                selRoom = (TextView) findViewById(R.id.selRoom);
                                if (Vars.selectedRoom != null) {
                                    double maxX=0;
                                    double maxY=0;
                                    for(double[] coord : Vars.selectedRoom.getXYs()){
                                        if(coord[0]>maxX)
                                            maxX=coord[0];
                                        if(coord[1]>maxY)
                                            maxY=coord[1];
                                    }
                                    selRoom.setText(Vars.selectedRoom.getName()+" ("+maxX+"x"+maxY+")");
                                    drawGraph();
                                    Vars.selectedRoom.drawRoom();
                                }
                                if (Vars.selectedShelves != null) {
                                    for (Shelf shelf : Vars.selectedShelves) {
                                        shelf.colorShelf();
                                    }
                                }
                            }
                        }
                    }

                });
            }
        });
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return true;
    }

    private void drawGraph(){
        Vars.graph = (GraphView) findViewById(R.id.graph);
        Vars.graph.removeAllSeries();
        Vars.graph.getGridLabelRenderer().setGridStyle(GridLabelRenderer.GridStyle.NONE);
        Vars.graph.getGridLabelRenderer().setHorizontalLabelsVisible(false);
        Vars.graph.getGridLabelRenderer().setVerticalLabelsVisible(false);
        /*Vars.graph.getGridLabelRenderer().setGridColor(Color.GRAY);
        Vars.graph.getGridLabelRenderer().setHorizontalLabelsColor(Color.GRAY);
        Vars.graph.getGridLabelRenderer().setVerticalLabelsColor(Color.GRAY);
        */
        Vars.graph.getViewport().setXAxisBoundsManual(true);
        Vars.graph.getViewport().setYAxisBoundsManual(true);

        //calcolo dimensione grafico in base alla dimensione della stanza selezionata (il grafico deve essere quadrato per le proporzioni grafiche)
        double dimGraph = 0;
        for(double[] xy : Vars.selectedRoom.getXYs()){
            if(xy[0]>dimGraph){
                dimGraph = xy[0];
            }
            if(xy[1]>dimGraph){
                dimGraph = xy[1];
            }
        }
        Vars.graph.getViewport().setMaxX(dimGraph);
        Vars.graph.getViewport().setMaxY(dimGraph);
    }

    public static boolean setListViewHeightBasedOnItems(ListView listView) {

        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter != null) {

            int numberOfItems = listAdapter.getCount();

            // Get total height of all items.
            int totalItemsHeight = 0;

            for (int itemPos = 0; itemPos < numberOfItems; itemPos++) {
                View item = listAdapter.getView(itemPos, null, listView);
                item.measure(0, 0);
                totalItemsHeight += item.getMeasuredHeight();
            }

            // Get total height of all item dividers.
            int totalDividersHeight = listView.getDividerHeight() *
                    (numberOfItems - 1);

            // Set list height.
            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = totalItemsHeight + totalDividersHeight;
            listView.setLayoutParams(params);
            listView.requestLayout();

            return true;

        } else {
            return false;
        }
    }


    //INIZIO METODI ESTIMOTE BEACONS

    @Override
    protected void onDestroy() {
        beaconManager.disconnect();
        super.onDestroy();
    }

    @Override
    protected void onStart() {
        super.onStart();

        // Check if device supports Bluetooth Low Energy.
        if (!beaconManager.hasBluetooth()) {
            Toast.makeText(this, "Device does not have Bluetooth Low Energy", Toast.LENGTH_LONG).show();
            return;
        }

        // If Bluetooth is not enabled, let user enable it.
        if (!beaconManager.isBluetoothEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        } else {
            connectToService();
        }
    }

    @Override
    protected void onStop() {
        try {
            beaconManager.stopRanging(ALL_ESTIMOTE_BEACONS_REGION);
        } catch (RemoteException e) {
            Log.d(TAG, "Error while stopping ranging", e);
        }


        super.onStop();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_ENABLE_BT) {
            if (resultCode == Activity.RESULT_OK) {
                connectToService();
            } else {
                Toast.makeText(this, "Bluetooth not enabled", Toast.LENGTH_LONG).show();
                getActionBar().setSubtitle("Bluetooth not enabled");
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void connectToService() {
        adapter.replaceWith(Collections.<Beacon>emptyList());
        beaconManager.connect(new BeaconManager.ServiceReadyCallback() {
            @Override
            public void onServiceReady() {
                try {
                    beaconManager.startRanging(ALL_ESTIMOTE_BEACONS_REGION);
                } catch (RemoteException e) {
                    Toast.makeText(DetectionActivity.this, "Cannot start ranging, something terrible happened",
                            Toast.LENGTH_LONG).show();
                    Log.e(TAG, "Cannot start ranging", e);
                }
            }
        });
    }

    //FINE METODI ESTIMOTE BEACONS
}
