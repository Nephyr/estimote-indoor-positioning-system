package jacopo.bambook14p2;

import android.graphics.Color;

import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Jacopo on 12/05/2015.
 */
public class Shelf {
    String name;
    ArrayList<double[]> XYs; //lista spigloli in metri
    SISManager sisManager;

    public Shelf(String name){
        sisManager = new SISManager();
        this.name = name;
        this.setXYsFromCells(sisManager.getShelf(name));
    }

    public void drawShelf(){
        double x1,y1,x2,y2;
        int size = this.XYs.size();
        LineGraphSeries<DataPoint> shelfWall;
        for(int i=0;i<size-1;i++){
            x1 = (this.XYs.get(i))[0];
            y1 = (this.XYs.get(i))[1];
            x2 = (this.XYs.get(i+1))[0];
            y2 = (this.XYs.get(i+1))[1];
            shelfWall = new LineGraphSeries<DataPoint>(new DataPoint[]{
                    new DataPoint(x1, y1),
                    new DataPoint(x2, y2),
            });
            shelfWall.setColor(Color.parseColor("#333333"));
            Vars.graph.addSeries(shelfWall);
        }
        x1 = (this.XYs.get(0))[0];
        y1 = (this.XYs.get(0))[1];
        x2 = (this.XYs.get(size-1))[0];
        y2 = (this.XYs.get(size-1))[1];
        shelfWall = new LineGraphSeries<DataPoint>(new DataPoint[]{
                new DataPoint(x1, y1),
                new DataPoint(x2, y2),
        });
        shelfWall.setColor(Color.parseColor("#333333"));
        Vars.graph.addSeries(shelfWall);
    }

    public void colorShelf(){
        int size = XYs.size();
        double x1,x2,y1,y2;
        LineGraphSeries<DataPoint> shelfWall;
        for (int i = 0; i < size - 1; i++) {
            x1 = (XYs.get(i))[0];
            y1 = (XYs.get(i))[1];
            x2 = (XYs.get(i + 1))[0];
            y2 = (XYs.get(i + 1))[1];
            shelfWall = new LineGraphSeries<DataPoint>(new DataPoint[]{
                    new DataPoint(x1, y1),
                    new DataPoint(x2, y2),
            });
            shelfWall.setColor(Color.GREEN);
            shelfWall.setThickness(7);
            Vars.graph.addSeries(shelfWall);
        }
        x1 = (XYs.get(0))[0];
        y1 = (XYs.get(0))[1];
        x2 = (XYs.get(size - 1))[0];
        y2 = (XYs.get(size - 1))[1];
        shelfWall = new LineGraphSeries<DataPoint>(new DataPoint[]{
                new DataPoint(x1, y1),
                new DataPoint(x2, y2),
        });
        shelfWall.setColor(Color.GREEN);
        shelfWall.setThickness(7);
        Vars.graph.addSeries(shelfWall);
    }

    public void setXYsFromCells(ArrayList<String> cellslist){
        ArrayList<double[]> list = new ArrayList<>();
        double[] tmp;
        for(int i=0;i<cellslist.size();i++)
        {
            tmp = new double[2];
            tmp[0] = Double.parseDouble(cellslist.get(i).split(",")[0]);
            tmp[1] = Double.parseDouble(cellslist.get(i).split(",")[1]);
            list.add(tmp);
        }

        double x1=list.get(0)[0]/10;
        double y1=list.get(0)[1]/10;
        double x2=0,y2=0;

        for(int i=0;i<list.size();i++)
        {
            if((list.get(i)[0]/10)<x1)
                x1=(list.get(i)[0]/10);
            if((list.get(i)[1]/10)<y1)
                y1=(list.get(i)[1]/10);

            if(((list.get(i)[0]/10)+0.1)>x2)
                x2=(list.get(i)[0]/10)+0.1;
            if(((list.get(i)[1]/10)+0.1)>y2)
                y2=(list.get(i)[1]/10)+0.1;
        }

        ArrayList<double[]> XYs = new ArrayList<>();
        XYs.add(new double[]{x1,y1});
        XYs.add(new double[]{x2,y1});
        XYs.add(new double[]{x2, y2});
        XYs.add(new double[]{x1, y2});

        this.XYs = XYs;
    }

    //ritorna la room contenente la shelf corrente (this) se esiste
    public Room getItsRoom(){
        for(Room r : Vars.rooms){
            if(r.getShelves().contains(this)){
                return r;
            }
        }
        return null;
    }

    public String getName(){
        return this.name;
    }

    public ArrayList<double[]> getXYs(){
        return this.XYs;
    }
}
