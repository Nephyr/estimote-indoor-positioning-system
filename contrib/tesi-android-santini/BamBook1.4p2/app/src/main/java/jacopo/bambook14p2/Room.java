package jacopo.bambook14p2;

import android.graphics.Color;

import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Jacopo on 12/05/2015.
 */
public class Room {
    String name;
    ArrayList<double[]> XYs; //lista spigloli in metri
    ArrayList<Shelf> shelves;
    ArrayList<Beac> beacs;
    SISManager sisManager;

    public Room(String name){
        sisManager = new SISManager();
        this.name = name;
        this.setXYsFromCells(sisManager.getRoom(name));
        this.shelves = new ArrayList<Shelf>();
        this.beacs = new ArrayList<Beac>();

        //per ogni scaffale, lo creo e lo aggiungo alla lista degli scaffali di questa stanza
        for(String shelf : sisManager.getListShelves(name)) {
            Shelf s = new Shelf(shelf);
            this.shelves.add(s);
        }
        //per ogni beacon, lo creo (da solo si setta la posizione) e lo aggiungo alla lista dei beacon di questa stanza
        for(String beac : sisManager.getListBeacs(name)){
            Beac b = new Beac(beac);
            this.beacs.add(b);
        }
    }

    public void drawRoom(){

        //disegno stanza
        double x1,y1,x2,y2;
        int size = this.XYs.size();
        LineGraphSeries<DataPoint> roomWall;
        for(int i=0;i<size-1;i++){
            x1 = (this.XYs.get(i))[0];
            y1 = (this.XYs.get(i))[1];
            x2 = (this.XYs.get(i+1))[0];
            y2 = (this.XYs.get(i+1))[1];
            roomWall = new LineGraphSeries<DataPoint>(new DataPoint[]{
                    new DataPoint(x1, y1),
                    new DataPoint(x2, y2),
            });
            roomWall.setColor(Color.parseColor("#333333"));
            Vars.graph.addSeries(roomWall);
        }
        x1 = (this.XYs.get(0))[0];
        y1 = (this.XYs.get(0))[1];
        x2 = (this.XYs.get(size-1))[0];
        y2 = (this.XYs.get(size-1))[1];
        roomWall = new LineGraphSeries<DataPoint>(new DataPoint[]{
                new DataPoint(x1, y1),
                new DataPoint(x2, y2),
        });
        roomWall.setColor(Color.parseColor("#333333"));
        Vars.graph.addSeries(roomWall);

        //disegno scaffali
        for(Shelf shelf : this.shelves){
            shelf.drawShelf();
        }

        //disegno beacons
        for(Beac beac : this.beacs){
            beac.drawBeac();
        }
    }

    public void setXYsFromCells(ArrayList<String> cellslist){
        ArrayList<double[]> list = new ArrayList<>();
        double[] tmp;
        for(int i=0;i<cellslist.size();i++)
        {
            tmp = new double[2];
            tmp[0] = Double.parseDouble(cellslist.get(i).split(",")[0]);
            tmp[1] = Double.parseDouble(cellslist.get(i).split(",")[1]);
            list.add(tmp);
        }

        double x1=list.get(0)[0]/10;
        double y1=list.get(0)[1]/10;
        double x2=0,y2=0;

        for(int i=0;i<list.size();i++)
        {
            if((list.get(i)[0]/10)<x1)
                x1=(list.get(i)[0]/10);
            if((list.get(i)[1]/10)<y1)
                y1=(list.get(i)[1]/10);

            if(((list.get(i)[0]/10)+0.1)>x2)
                x2=(list.get(i)[0]/10)+0.1;
            if(((list.get(i)[1]/10)+0.1)>y2)
                y2=(list.get(i)[1]/10)+0.1;
        }

        ArrayList<double[]> XYs = new ArrayList<>();
        XYs.add(new double[]{x1,y1});
        XYs.add(new double[]{x2,y1});
        XYs.add(new double[]{x2,y2});
        XYs.add(new double[]{x1,y2});

        this.XYs = XYs;
    }

    //ritorna la shelf dato il nome se presente nella room corrente
    public Shelf getShelfFromName(String shelf){
        for(Shelf s : this.shelves){
            if(s.getName().equals(shelf)){
                return s;
            }
        }
        return null;
    }

    public String getName(){
        return this.name;
    }

    public ArrayList<double[]> getXYs(){
        return this.XYs;
    }

    public ArrayList<Shelf> getShelves(){
        return this.shelves;
    }

    public ArrayList<Beac> getBeacs(){
        return this.beacs;
    }

}
